import axios from 'axios'
export const createHttpWithHeaders = function(headers){
  return axios.create({
    baseURL: '/',
    headers: headers
  })
}