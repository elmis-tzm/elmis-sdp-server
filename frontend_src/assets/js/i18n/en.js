export const en = {
    appName: 'eLMIS Facility Edition',
    login: 'Sign In',
    username: 'Username',
    password: 'Password',
    node: 'Node'
}
