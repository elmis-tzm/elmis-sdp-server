export const convertToFacilityApprovedProduct = function(nodeProduct){
    return {
        productId: nodeProduct.programProduct.product.id,
        productCode: nodeProduct.programProduct.product.code,
        programproductid: nodeProduct.programProduct.id,
        productName: nodeProduct.programProduct.product.primaryname,
        categoryId: null,
        programCode: nodeProduct.programProduct.program.code,
        generalStrength: nodeProduct.programProduct.product.strength,
        exipryDate: null,
        quantity: null,
        packSize: nodeProduct.programProduct.product.packsize,
        dosesPerDispensingUnit: nodeProduct.programProduct.product.dosesperdispensingunit,
        dispensingUnit: nodeProduct.programProduct.product.dispensingunit,
        remark: "",
        quantityRequested: null,
        maxMonthOfStock: null,
        minMonthOfStock: null,
        nodeProduct: nodeProduct
    }
};