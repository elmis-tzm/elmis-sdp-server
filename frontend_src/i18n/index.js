import Vue from 'vue'
import VueI18n from 'vue-i18n'
import store from '../store'

import {en} from '../assets/js/i18n/en'
import {fr} from '../assets/js/i18n/fr'
import {ja} from '../assets/js/i18n/ja'

Vue.use(VueI18n)

var i18n = new VueI18n({
    locale: store.state.persistedState.lang ? store.state.persistedState.lang : 'en',
    messages: {
        en: {
            message: en
        },
        fr: {
            message: fr
        },
        ja: {
            message: ja
        }
    }
})

export default i18n
