var context = {}
export default {
    data(){
        return {
            sortKey: 'productCode',
            pageNumber: 1,
            totalNumberOfPages: 0,
            pageSize: 15,
            reverse: false,
            columns: [
                {'key': 'productCode', 'value': 'Product Code'},
                {'key': 'productName', 'value': 'Product Name'},
                {'key': 'generalStrength', 'value': 'Strength'},
                {'key': 'quantity', 'value': 'Quantity'},
                {'key': 'remark', 'value': 'Remark'}],
            columns2: [{'key': 'productCode', 'value': 'Product Code'},
                {'key': 'productName', 'value': 'Product Name'},
                {'key': 'generalStrength', 'value': 'General Strength'}],
            baseProductList: [],
            filteredList: [],
            search: ''
        }
    },
    beforeMount(){
        context = this
    },
    filters: {
        capitalize: function (columnObj) {
            return columnObj['value']
        }
    },
    computed:{
        filteredProductList: {
            get: function () {
                this.filteredList = this.baseProductList.filter(function (fap) {
                    var searchKey = context.search.toLowerCase()
                    var productCode = fap.productCode.toLowerCase()
                    var productName = fap.productName.toLowerCase()
                    return (fap.programCode === context.selectedProgram) && ((productCode.indexOf(searchKey) !== -1) || (productName.indexOf(searchKey) !== -1))
                })
                var pages = this.filteredList.length / this.pageSize
                this.totalNumberOfPages = Number.isInteger(pages) ? pages : (parseInt(pages) + 1)
                var type = this.reverse ? 'desc' : 'asc'
                var orderBy = type.toUpperCase()
                var column = this.sortKey['key']
                if (!column) {
                    column = 'productCode'
                }
                if (['ASC', 'DESC'].indexOf(orderBy) === -1) {
                    console.error(type, 'Not valid, please use ASC or DESC')
                    return
                }
                if (!this.reverse) {
                    this.filteredList.sort((a, b) => { return (a[column] > b[column]) ? 1 : ((b[column] > a[column]) ? -1 : 0) })
                } else {
                    this.filteredList.sort((a, b) => { return (a[column] > b[column]) ? -1 : ((b[column] > a[column]) ? 1 : 0) })
                }

                var pageSize = parseInt(context.pageSize)
                var pageNumber = parseInt(context.pageNumber) > parseInt(context.totalNumberOfPages) ? parseInt(context.totalNumberOfPages) : parseInt(context.pageNumber)
                if (pageNumber > context.totalNumberOfPages) {
                    pageNumber = context.totalNumberOfPages
                }
                var startingIndex = (pageNumber - 1) * pageSize
                var endingIndex = startingIndex + pageSize
                var paginatedFilteredList = this.filteredList.slice(startingIndex, endingIndex)
                return paginatedFilteredList
            }
        }
    },
    methods:{
        sortBy: function (newSortKey, event) {
            event.preventDefault()
            console.log(newSortKey)
            context.reverse = (context.sortKey === newSortKey) ? !context.reverse : false
            context.sortKey = newSortKey
        },
        getSortIcon: function(column){
            if(column == context.sortKey){
                if(context.reverse){
                    return 'fa fa-sort-desc'
                }else{
                    return 'fa fa-sort-asc'
                }
            }else{
                return 'fa fa-sort'
            }
        },
        setPage: function (pageNumber) {
            context.pageNumber = pageNumber
        }
    }
}
