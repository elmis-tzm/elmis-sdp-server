import {getHttpAuthenticated} from '../assets/js/http_with_token'
var context = {}
export default{
    beforeMount(){
        context = this
    },
    methods:{
        loadProductSources: function(){
            getHttpAuthenticated().get("/product-sources").then(function(response) {
                context.productSources = response.data
            }).catch(function(e) {
                context.http_errors.push(e)
            })
        }
    }
}