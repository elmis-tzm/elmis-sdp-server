var context={}
export default{
    beforeMount(){
        context = this
    },
    methods:{
        closeDialog: function(){
            context.showSuccessAlert = false
            context.showErrorAlert = false
        }
    }
}