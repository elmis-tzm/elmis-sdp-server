import {getHttpAuthenticatedWithLoadingFlag} from '../assets/js/http_with_token'
import store from '../store'
var context = {}
export default{
    beforeMount(){
        context = this
    },
    methods : {
        loadFacilityApprovedProducts: function(){
            getHttpAuthenticatedWithLoadingFlag().get("/facility-approved-products/" + store.state.persistedState.facility.code).then(function(response) {
                var facilityApprovedProducts = response.data

                for(var i=0; i<facilityApprovedProducts.length; i++){

                    var fap = facilityApprovedProducts[i]
                    fap.id = fap.productId
                    fap.code = fap.productCode
                    fap.primaryName = fap.productName

                    context.baseProductList.push(fap)
                }

            }).catch(function(e) {
                console.log(e)
            })
        }
    }
}
