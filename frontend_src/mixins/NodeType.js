import {getHttpAuthenticated} from '../assets/js/http_with_token'
var context = {}
export default{
    beforeMount(){
        context = this
    },
    methods:{
        loadNodeTypes: function(){
            getHttpAuthenticated().get("/rest-api/node/get-all-nodetypes").then(function(response) {
                context.nodeTypeList = response.data
            }).catch(function(e) {
                context.http_errors.push(e)
            })
        }
    }
}