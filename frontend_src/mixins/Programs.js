import {getHttpAuthenticated} from '../assets/js/http_with_token';
var context = {}
export default{
    beforeMount(){
        context = this
    },
    computed:{
        myPrograms(){
            var sortColumn = context.sortProgramsBy
            var filteredPrograms = context.programs.filter(function(pr){
                for(var i=0;i<context.nodePrograms.length;i++){
                    if(pr.id === context.nodePrograms[i].programId){
                        return true
                    }
                }
                return false
            })

            filteredPrograms.sort((a, b) => {
                return (a[sortColumn] > b[sortColumn]) ? 1 : ((b[sortColumn] > a[sortColumn]) ? -1 : 0)
            })

            return filteredPrograms
        }
    },
    methods:{
        loadNodePrograms: function(){
            getHttpAuthenticated().get("/get-node-programs-by-node/"+this.$store.state.persistedState.node.id)
                .then(function(response) {
                    context.nodePrograms = response.data
                }).catch(function(e) {
                context.http_errors.push(e)
            })
        },
        loadPrograms: function(){
            getHttpAuthenticated().get("/programs").then(function(response) {
                context.programs = response.data;
                context.programs.forEach(value => {
                    value.label=value.name;
                    value.value=value.id;
                })
            }).catch(function(e) {
                context.http_errors.push(e)
            })
        }
    }
}