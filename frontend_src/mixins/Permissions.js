import store from '../store'
export default({
    methods:{
        hasPermission: function(rightName){
            return store.state.persistedState.userNodeRoleRights.includes(rightName)
        }
    }
})