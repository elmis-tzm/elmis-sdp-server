var context = {}
export default{
    beforeMount(){
        context = this
    },
    methods:{
        sortBy: function (newSortKey, event) {
            event.preventDefault()
            console.log(newSortKey)
            context.reverse = (context.sortKey === newSortKey) ? !context.reverse : false
            context.sortKey = newSortKey
        },
        getSortIcon: function(column){
            if(column == context.sortKey){
                if(context.reverse){
                    return 'fa fa-sort-desc'
                }else{
                    return 'fa fa-sort-asc'
                }
            }else{
                return 'fa fa-sort'
            }
        },
        setPage: function (pageNumber) {
            context.pageNumber = pageNumber
        }
    }
}
