var context = {}
export default{
    beforeMount(){
        context = this
    },
    methods: {
        facilityCode() {
            return context.$store.state.persistedState.facility.code
        }
    }
}