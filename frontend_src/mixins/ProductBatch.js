import {getHttpAuthenticated} from '../assets/js/http_with_token'
var context = {}
export default {
    beforeMount(){
        context = this
    },
    computed:{
        productBatchList(){
            var batch = {}
            var productBatchList = []
            for(var i=0;i<context.batchList.length;i++){
                batch = context.batchList[i]
                var batchLineItem = {
                    id: batch.id,
                    name: batch.name,
                    batchNumber: batch.name,
                    expiryDate: new Date(batch.expiryDate),
                    quantity: '',
                    remark: '',
                    hasErrors: false
                }

                if(context.transactionProductsToEdit){

                    var editingTxnProduct = context.transactionProductsToEdit.find(function (txnProd) {
                        return txnProd.batchNumber === batchLineItem.batchNumber
                    })

                    if (editingTxnProduct) {
                        batchLineItem.quantity = editingTxnProduct.quantity
                        batchLineItem.remark = editingTxnProduct.remark
                        if(editingTxnProduct.adjustmentType)
                        {
                            batchLineItem.adjustmentType = editingTxnProduct.adjustmentType
                        }
                    }
                }

                if(context.selectedProduct.nodeProduct){
                    var nppbs = context.selectedProduct.nodeProduct.nodeProgramProductBatches
                    var nppbMatch = nppbs.find ((nppb) => {
                        return parseInt(nppb.programProductBatchId) === parseInt(batchLineItem.id)
                    })

                    if(nppbMatch){
                        batchLineItem.nppbId = nppbMatch.id
                        batchLineItem.programProductBatchId = nppbMatch.programProductBatchId
                    }

                    var nPProductBatchBalances = context.selectedProduct.balances.nodeProgramProductBatchBalances
                    var nPProductBatchBalanceMatch = nPProductBatchBalances.find(function (nPProductBatchBalance) {
                        return parseInt(nPProductBatchBalance.nodeProgramProductBatch.id) === parseInt(batchLineItem.nppbId)
                    })

                    if(nPProductBatchBalanceMatch){
                        batchLineItem.balance = parseInt(nPProductBatchBalanceMatch.balance)
                        batchLineItem.usableBalance = parseInt(nPProductBatchBalanceMatch.usableBalance)
                    }
                }

                if(batchLineItem.expiryDate > new Date().getTime())
                {
                    productBatchList.push(batchLineItem)
                }
            }
            return productBatchList
        }
    },
    methods:{
        getProductBatchesByProductCode(productCode){
            getHttpAuthenticated().get("/rest-api/batch/" + productCode).then(function(response) {
                context.batchList = response.data
            }).catch(function(e) {
                console.log(e)
            })
        }
    }
}