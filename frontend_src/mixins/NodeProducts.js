import {getHttpAuthenticated} from '../assets/js/http_with_token'
import {convertToFacilityApprovedProduct} from '../assets/js/products_helper'
var context = {}
export default{
    beforeMount(){
        context = this
    },
    methods : {
        loadNodeProducts: function(nodeId, programCode){
            if(programCode){
                getHttpAuthenticated().get("/facility-approved-node-products?nodeId=" + nodeId
                    + "&programCode="+programCode).then(function(response) {

                    var nodeProducts = response.data
                    context.baseProductList.length = 0
                    for(var i=0;i<nodeProducts.length;i++){
                        context.baseProductList.push(convertToFacilityApprovedProduct(nodeProducts[i]))
                    }

                }).catch(function(e) {
                    console.log(e)
                })
            }

        }
    }
}