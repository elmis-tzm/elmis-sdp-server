import {getHttpAuthenticated} from '../assets/js/http_with_token'
//    import VeeValidate from 'vee-validate'
var context = {}
export default{
    data(){
        return {
            programs: [],
            nodePrograms: []
        }
    },
    beforeMount(){
        context = this
        context.loadPrograms(null);
    },
    methods: {
        loadNodePrograms: function () {
            getHttpAuthenticated().get("/rest-api/node/get-all-nodetypes").then(function (response) {
                context.nodeTypeList = response.data
            }).catch(function (e) {
                context.http_errors.push(e)
            })
        },
        isNullOrUndefined:  function (value) {
            return value === null || value === undefined;
        }
        ,getIndexOfProgram : function(program,nodePrograms){
            if(context.nodePrograms!==null && nodePrograms.length>0){
                for(var i=0; i<nodePrograms.length;i++){
                    var p=nodePrograms[i];
                    if(program.id===p.programId){
                        return p;
                    }
                }
            }
            return null;
        }
        ,
        loadPrograms: function (node) {

            if(!context.isNullOrUndefined(node)) {
                context.nodePrograms=node.nodePrograms;
            }
            getHttpAuthenticated().get("/programs").then(function (response) {
                context.programs = response.data
                for (var i=0; context.programs.length; i++) {
                    var program = context.programs[i];
                    if (context.getIndexOfProgram(program, context.nodePrograms) === null) {
                        var nodeProgram = {
                            nodeId: "",
                            dispensingPoint: false,
                            selectedNow: false,
                            selectedBefore:false,
                            programId: program.id,
                            programCode: program.code
                        }
                        context.nodePrograms.push(nodeProgram)
                    }
                }

            }).catch(function (e) {
                context.http_errors.push(e)
            })
        }


    }
}