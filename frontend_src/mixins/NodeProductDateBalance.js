import {getHttpAuthenticated} from '../assets/js/http_with_token'
var context = {}
export default{
    beforeMount(){
        context = this
    },
    methods : {
        loadNodeProductDateBalances: function(date, nodeId, programId){
            getHttpAuthenticated()
                .get("/rest-api/node/node/balances-on-date/" + date + "/" + nodeId + "/" + programId)
                .then(function(response) {
                    context.productBalances = response.data
                }).catch(function(e) {
                console.log(e)
            })
        }
    }
}
