import Vue from 'vue'
import Vuex from 'vuex'
import arvPersistedStateModule from './dispensationModule'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

const persistedStateModule = {
	state: {
		lang: '',
		authToken: '',
		facility: {},
		user: {},
		node: {},
		userNodeRoleRights: [],
		sessionExpired: false,
		transactionProducts: [],
		loading: false,
        showBatchPanel: true,
        transactionDate: new Date(),
        sideNavCollapsed: false,
        systemSettings:[]
	},
	mutations: {
		setLang(state, value){
			state.lang = value
		},
		setAuthToken(state, value){
			state.authToken = value
		},
		setFacility(state, value){
			state.facility = value
		},
		setUser(state, value){
			state.user = value
		},
		setNode(state, value){
			state.node = value
		},
		setUserNodeRoleRights(state, value){
			state.userNodeRoleRights = value
		},
		setSessionExpired(state, value){
			state.sessionExpired = value
		},
		setTransactionProducts(state, value){
			state.transactionProducts = value
		},
		setLoading(state, value){
			state.loading = value
		},
        setShowBatchPanel(state, value){
            state.showBatchPanel = value
        },
        setTransactionDate(state, value){

        },
        setSideNavCollapsed(state,value){
            state.sideNavCollapsed = value
        },
        setSystemSettings(state,value){
            state.systemSettings = value
        }
	},
    actions:{
        setLang({commit}, value){
            commit('setLang', value)
        },
        setAuthToken({commit}, value){
            commit('setAuthToken', value)
        },
        setFacility({commit}, value){
            commit('setFacility', value)
        },
        setUser({commit}, value){
            commit('setUser', value)
        },
        setNode({commit}, value){
            commit('setNode', value)
        },
        setUserNodeRoleRights({commit}, value){
            commit('setUserNodeRoleRights', value)
        },
        setSessionExpired({commit}, value){
            commit('setSessionExpired', value)
        },
        setTransactionProducts({commit}, value){
            commit('setTransactionProducts', value)
        },
	    setLoading({commit}, value){
	        commit('setLoading', value)
        },
        setShowBatchPanel({commit}, value){
            commit('setShowBatchPanel', value)
        },
        setSideNavCollapsed({commit}, value){
            commit('setSideNavCollapsed', value)
        },
        setSystemSettings({commit}, value){
            commit('setSystemSettings', value)
        }
    }
}

const store = new Vuex.Store({
	modules: {
        persistedState: persistedStateModule,
        arvPersistedState: arvPersistedStateModule
	},
	plugins: [createPersistedState({
		storage: window.sessionStorage
	})],
    mutations: {
        reset(state, value){
            state.persistedState = persistedStateModule
            state.arvPersistedState = arvPersistedStateModule
        }
    },
    actions: {
        reset({commit}, value) {
            commit('reset', value)
        }
    }
})

export default store 
