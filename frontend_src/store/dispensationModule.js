export default {
    state:{
        currentClient: {},
        numberOfDays: 0,
        regimenProducts: [],
        productBatches: []
    },
    mutations:{
        setCurrentClient(state, value){
            state.currentClient = value
        },
        setNumberOfDays(state, value){
            state.numberOfDays = value
        },
        setRegimenProducts(state, value){
            state.regimenProducts = value
        },
        setProductBatches(state, newProductBatches){

            state.productBatches = newProductBatches

        }
    },
    actions:{
        setCurrentClient({commit}, value){
            commit('setCurrentClient', value)
        },
        setNumberOfDays({commit}, value){
            commit('setNumberOfDays', value)
        },
        setRegimenProducts({commit}, value){
            commit('setRegimenProducts', value)
        },
        setProductBatches({commit}, value){
            commit('setProductBatches', value)
        }
    },
    getters:{
        productBatches: state=>{
            return state.productBatches
        },
        currentClient : state=>{
            return state.currentClient
        }
    }
}

