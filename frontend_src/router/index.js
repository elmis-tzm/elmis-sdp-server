import Vue from 'vue'
import store from '../store'
import Router from 'vue-router'
import Login from '../components/Login'
import Dashboard from '../components/dashboard/index'
import ReceiveProducts from '../components/transactions/ReceiveProducts'
import IssueProducts from '../components/transactions/IssueProducts'
import HivTest from '../components/transactions/HivTest'
import StockControlCard from '../components/stock/StockControlCard'
import PhysicalCount from '../components/stock/PhysicalCount'
import Dispense from '../components/transactions/Dispense'
import DispenseEM from '../components/transactions/DispenseEM'
import Adjust from '../components/stock/Adjust'
import ProgramNode from '../components/admin/ProgramNode'
import OfflineRnr from '../components/report/OfflineRnr'
import Users from '../components/admin/login-users/Users'
import SystemSettings from '../components/admin/system-settings/SystemSettings.vue'
import BulkConsumption from '../components/transactions/BulkConsumption'
import admin from '../components/admin/index.vue'
import TransactionReversal from '../components/transactions/reversal.vue'
import ProductStatus from '../components/stock/ProductStatus.vue'
import DAR from '../components/report/DAR.vue'

Vue.use(Router)

 var router = new Router({
     mode: 'history',
    routes: [
        {
            path: '/',
            name: 'login',
            component: Login
        },
        {
            path: '/dashboard',
            name: 'dashboard',
            component: Dashboard
        },
        {
            path: '/receive_products',
            name: 'receive_products',
            component: ReceiveProducts,
            meta: { requiresAuth: true, requiresRight: 'RECEIVE PRODUCTS' }
        },
        {
            path: '/issue_products',
            name: 'issue_products',
            component: IssueProducts,
            meta: { requiresAuth: true, requiresRight: 'ISSUE PRODUCTS' }
        },
        {
            path: '/hiv_tests',
            name: 'hiv_tests',
            component: HivTest,
            meta: { requiresAuth: true, requiresRight: 'HIV TEST' }
        },
        {
            path: '/stock_control_card',
            name: 'stock_control_card',
            component: StockControlCard,
            meta: { requiresAuth: true, requiresRight: 'VIEW STOCK CONTROL CARD' }
        },
        {
            path: '/physical_count',
            name: 'physical_count',
            component: PhysicalCount,
            meta: { requiresAuth: true, requiresRight: 'PHYSICAL COUNT' }
        },
        {
            path: '/adjust',
            name: 'adjust',
            component: Adjust,
            meta: { requiresAuth: true, requiresRight: 'ADJUSTMENT' }
        },
        {
            path: '/dispense',
            name: 'dispense',
            component: Dispense,
            meta: { requiresAuth: true, requiresRight: 'ARV DISPENSATION' }
        },
        {
            path: '/emdispense',
            name: 'emdispense',
            component: DispenseEM,
            meta: { requiresAuth: true, requiresRight: 'ARV DISPENSATION' }
        },
        {
            path: '/bulk_consumption',
            name: 'bulk_consumption',
            component: BulkConsumption,
            meta: { requiresAuth: true, requiresRight: 'ARV DISPENSATION' }
        },
        {
            path: '/program_node',
            name: 'program_node',
            component: ProgramNode,
            meta: { requiresAuth: true, requiresRight: 'ARV DISPENSATION' }
        },
        {
            path: '/offline-RnR',
            name: 'offlineRnr',
            component: OfflineRnr,
            meta: { requiresAuth: true, requiresRight: 'VIEW HUB R&R' }
        }
        ,
        {
            path: '/users',
            name: 'manage_users',
            component: Users,
            meta: { requiresAuth: true, requiresRight: 'ARV DISPENSATION' }
        },
        {
            path: '/settings',
            name: 'system_settings',
            component: SystemSettings,
            meta: { requiresAuth: true, requiresRight: 'ARV DISPENSATION' }
        }
        ,
        {
            path: '/users2',
            name: 'users',
            component: admin,
            meta: { requiresAuth: true, requiresRight: 'ARV DISPENSATION', userSelected: true, activeTab : 0}
        },
        {
            path: '/roles',
            name: 'roles',
            component: admin,
            meta: { requiresAuth: true, requiresRight: 'ARV DISPENSATION', roleSelected: true, activeTab: 1},
            props: { default: true, sidebar: false }
        },
        {
            path: '/transaction-reversal',
            name: 'transaction_reversal',
            component: TransactionReversal,
            meta: { requiresAuth: true, requiresRight: 'REVERSE TRANSACTIONS'}
        },
        {
            path: '/product-status',
            name: 'product_status',
            component: ProductStatus,
            meta: { requiresAuth: true, requiresRight: 'ARV DISPENSATION'}
        },
        {
            path: '/dar',
            name: 'dar',
            component: DAR,
            meta: { requiresAuth: true, requiresRight: 'VIEW ARV DAR'}
        },
    ]
})

var hasPermission = function(path){
    var rightName = path.meta.requiresRight
    return store.state.persistedState.userNodeRoleRights.includes(rightName)
}
router.beforeEach((to, from, next) => {

    if(from.name && from.path !== to.path){ //TODO: what better way of knowing that we are browsing a different page
        store.dispatch('setTransactionProducts',[])
    }

    const authUser = store.state.persistedState.authToken
    if (to.matched.some(record => record.meta.requiresAuth)) {
        // this route requires auth, check if logged in
        // if not, redirect to login page.
        if (!authUser) {
            sessionStorage.clear()
            next('/')
        } else {
            if(from.name !== "login"){
                if(hasPermission(to)){
                    next()
                } else{
                    sessionStorage.clear()
                    next('/')
                } 
            }else{
                next()
            }              
        }
    } else {
        next() // make sure to always call next()!
    }
})

export default router
