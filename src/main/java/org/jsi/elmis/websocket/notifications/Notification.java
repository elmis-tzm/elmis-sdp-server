/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.websocket.notifications;


import org.jsi.elmis.model.BaseModel;

import java.util.List;

public class Notification extends BaseModel {

    public static final String NOT_YET_SENT = "NOT_YET_SENT";
    public static final String SENT = "SENT";
    public static final String SEEN = "SEEN";
    public static final String ADDRESSED = "ADDRESSED";
    public static final String APPROVAL="APPROVAL";
    public static final String ISSUE="ISSUE";
    public static final String RECEIPT="RECEIPT";

    public Notification(Integer id, String addresseeCode, Object content, String status, String remark) {
        super(id);
        this.addresseeCode = addresseeCode;
        this.content = content;
        this.status = status;
        this.remark = remark;
        this.expectedAction = remark;
    }

    public Notification(){
        super();
    }

    private Object content;

    private String status;

    private String remark;

    private String addresseeCode;

    private String expectedAction; //TODO: currently being copied from remark. Should change. (1)

    List<ElmisNotificationUser> elmisNotificationUsers;


    public Object getContent() {
        return content;
    }

    public void setContent(Object content) {
        this.content = content;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
        this.expectedAction = remark == null ? null : remark.trim(); //TODO: see (1)
    }

    public String getAddresseeCode() {
        return addresseeCode;
    }

    public void setAddresseeCode(String addresseeCode) {
        this.addresseeCode = addresseeCode;
    }

}