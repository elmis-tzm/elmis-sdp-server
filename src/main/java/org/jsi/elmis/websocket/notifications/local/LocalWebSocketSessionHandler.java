/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.websocket.notifications.local;

import org.jsi.elmis.model.StockTransfer;
import org.jsi.elmis.websocket.notifications.StompNotificationSessionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.stereotype.Component;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by Mekbib on 6/24/2017.
 */

public class LocalWebSocketSessionHandler extends StompSessionHandlerAdapter {
    private StompSession stompSession = null;
    private String notifyURL;
    private String myFacilityCode;
    private StompHeaders sendNotificationHeaders = new StompHeaders();
    private StompSession middleWareNotificationSession;

    @Override
    public void afterConnected(StompSession session, StompHeaders connectedHeaders) {
        this.stompSession = session;

        stompSession.subscribe("/topic/client-subscription", new StompFrameHandler() {
            @Override
            public Type getPayloadType(StompHeaders headers) {
                return Object.class;
            }

            @Override
            public void handleFrame(StompHeaders headers, Object payload) {
                StompHeaders subscriptionStompHeaders = new StompHeaders();
                subscriptionStompHeaders.set(StompHeaders.DESTINATION, "/topic/subscription");
                subscriptionStompHeaders.set("subscribedFacilityCode", myFacilityCode);
                middleWareNotificationSession.send(subscriptionStompHeaders, "Greetings from facility " + myFacilityCode + "!");
            }
        });
        System.out.println("Connected to Local Notification websocket");
    }

    public void sendNotification(Object notificationPayLoad){
        System.out.println("Are we connected " + areWeConnected());
        System.out.println("Payload to be sent " + notificationPayLoad);
        sendNotificationHeaders.set(StompHeaders.DESTINATION, notifyURL);
        stompSession.send(sendNotificationHeaders, notificationPayLoad);

    }

    public Boolean areWeConnected(){
        return stompSession != null ? stompSession.isConnected() : false;
    }

    public void setNotifyURL(String notifyURL){
        this.notifyURL = notifyURL;
    }

    public void setMiddleWareNotificationWebSocketSessionHandler(StompSession stompSession){
        this.middleWareNotificationSession = stompSession;
    }

    public void setMyFacilityCode(String myFacilityCode){
        this.myFacilityCode = myFacilityCode;
    }
}
