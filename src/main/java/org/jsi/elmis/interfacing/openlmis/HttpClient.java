/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.interfacing.openlmis;

/**
 * PoC
 * Created by: Elias Muluneh
 * Date: 6/30/13
 * Time: 5:32 PM
 */

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.*;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.util.EntityUtils;
import us.monoid.json.JSONException;

import java.io.*;

import static org.apache.http.entity.mime.HttpMultipartMode.BROWSER_COMPATIBLE;

public class HttpClient {

    public static final String GET = "GET";
    public static final String POST = "POST";
    public static final String PUT = "PUT";

    public static String HOST = "localhost";
    public static int PORT = 8080;
    public static String PROTOCOL = "http";


    private DefaultHttpClient httpClient;
    private BasicHttpContext httpContext;

    public ResponseEntity uploadFileAndJSON(String url, String json, InputStream inputStream, String fileName, String username, String password) throws JSONException, Exception {

        HttpHost targetHost = new HttpHost(HOST, PORT, PROTOCOL);
        AuthScope authScope = new AuthScope(HOST, PORT);
        HttpEntity entity;
        String sCurrentLine;
        String sCompleteResponse = "";
        UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(username, password);
        httpClient.getCredentialsProvider().setCredentials(authScope, credentials);
        AuthCache authCache = new BasicAuthCache();
        authCache.put(targetHost, new BasicScheme());
        httpContext.setAttribute(ClientContext.AUTH_CACHE, authCache);
        MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create();
        entityBuilder.setMode(BROWSER_COMPATIBLE);
        ContentBody cbFile = new InputStreamBody(inputStream, "multipart/form-data");
        entityBuilder.addPart(fileName, cbFile);
        HttpPost httpPost = new HttpPost(url);
        httpPost.setEntity(entityBuilder.build());
        httpPost.getRequestLine();
        HttpResponse response = null;
        response = this.httpClient.execute(httpPost);
        BufferedReader rd;

        entity = response.getEntity();

        rd = new BufferedReader(new InputStreamReader(entity.getContent()));

        ResponseEntity responseEntity = new ResponseEntity();
        responseEntity.setStatus(response.getStatusLine().getStatusCode());

        while ((sCurrentLine = rd.readLine()) != null) {
            sCompleteResponse = sCompleteResponse + (sCurrentLine);
        }
        responseEntity.setResponse(sCompleteResponse);

        EntityUtils.consume(entity);

        return responseEntity;

    }

    public ResponseEntity SendJSON(String json, String url, String commMethod, String username, String password) {
//    if (StringUtils.isEmpty(username) && StringUtils.isEmpty(password)) return null;

        HttpHost targetHost = new HttpHost(HOST, PORT, PROTOCOL);
        AuthScope authScope = new AuthScope(HOST, PORT);

        UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(username, password);
        httpClient.getCredentialsProvider().setCredentials(authScope, credentials);

        AuthCache authCache = new BasicAuthCache();
        authCache.put(targetHost, new BasicScheme());

        httpContext.setAttribute(ClientContext.AUTH_CACHE, authCache);

        try {
            return handleRequest(commMethod, json, url);
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ResponseEntity handleRequest(String commMethod, String json, String url) throws IOException {
        HttpResponse response;
        HttpEntity entity;
        BufferedReader rd;
        String sCurrentLine;
        String sCompleteResponse = "";

        HttpRequestBase httpRequest = getHttpRequest(commMethod, url);

        prepareRequestHeaderAndEntity(commMethod, json, httpRequest);

        response = httpClient.execute(httpRequest, httpContext);
        entity = response.getEntity();

        rd = new BufferedReader(new InputStreamReader(entity.getContent()));

        ResponseEntity responseEntity = new ResponseEntity();
        responseEntity.setStatus(response.getStatusLine().getStatusCode());

        while ((sCurrentLine = rd.readLine()) != null) {
            sCompleteResponse = sCompleteResponse + (sCurrentLine);
        }
        responseEntity.setResponse(sCompleteResponse);

        EntityUtils.consume(entity);

        return responseEntity;
    }

    private HttpRequestBase getHttpRequest(String commMethod, String url) {
        switch (commMethod) {
            case GET:
                return new HttpGet(url);
            case POST:
                return new HttpPost(url);
            case PUT:
                return new HttpPut(url);
        }
        return null;
    }

    private void prepareRequestHeaderAndEntity(String commMethod, String json, HttpRequestBase httpRequest)
            throws UnsupportedEncodingException {

        if (commMethod.equals(GET)) return;

        httpRequest.setHeader(new BasicHeader("Content-Type", "application/json;charset=UTF-8"));
        ((HttpEntityEnclosingRequestBase) httpRequest).setEntity(new StringEntity(json, "UTF-8"));
    }

    public void createContext() {
        this.httpClient = new DefaultHttpClient();
        CookieStore cookieStore = new BasicCookieStore();
        httpContext = new BasicHttpContext();
        httpContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);
    }
}