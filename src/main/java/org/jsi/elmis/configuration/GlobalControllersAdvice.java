/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.configuration;


import java.util.HashMap;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;


@ControllerAdvice(annotations = Controller.class)
class GlobalControllersAdvice {

	/**
	 * Handle exceptions thrown by handlers.
	 */
	@ExceptionHandler(value = { MethodArgumentNotValidException.class})
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ResponseEntity<HashMap<String, String>> exception(Exception exception) {

		HashMap<String, String> msg = new HashMap<String, String>();

		if (exception instanceof MethodArgumentNotValidException) {
			for (FieldError error : ((MethodArgumentNotValidException) exception).getBindingResult().getFieldErrors()) {
				msg.put(error.getField(), error.getDefaultMessage());
			}
		} else {
			msg.put("error", "Please fill required field");
		}

		return new ResponseEntity<HashMap<String, String>>(msg, HttpStatus.BAD_REQUEST);

	}
	

}
