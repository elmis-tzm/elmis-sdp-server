/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.rest.client;

import java.lang.reflect.Type;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.Date;

import com.sun.jersey.api.client.filter.GZIPContentEncodingFilter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.api.json.JSONConfiguration;
import org.jsi.elmis.service.interfaces.PropertiesService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.net.ssl.*;
/**
 * Created by Mekbib on 12/28/2016.
 */
public class ClientCommon {

    protected final Gson jSONMarshaller = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSS").create();

    protected static WebResource webResource;

    protected static ClientResponse response;
    @Autowired
    PropertiesService propertiesService;

    protected ClientCommon() {
    }


    protected Client getWSClient() {
        ignoreSSLClient();
        ClientConfig clientConfig = new DefaultClientConfig();
        clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
        Client wsClient = Client.create(clientConfig);
        wsClient.setConnectTimeout(5000);
        wsClient.setReadTimeout(5000);
        wsClient.addFilter(new GZIPContentEncodingFilter(false));
        return wsClient;
    }


    protected Gson getGson() {
        return new GsonBuilder().registerTypeAdapter(Date.class,
                new JsonDeserializer<Date>() {

                    @Override
                    public Date deserialize(JsonElement json, Type typeOfT,
                                            JsonDeserializationContext context)
                            throws JsonParseException {
                        return new Date(json.getAsJsonPrimitive().getAsLong());
                    }
                }).create();
    }


    public void  ignoreSSLClient(){
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager(){
            public X509Certificate[] getAcceptedIssuers(){return null;}
            public void checkClientTrusted(X509Certificate[] certs, String authType){}
            public void checkServerTrusted(X509Certificate[] certs, String authType){}
        }};

        // Ignore differences between given hostname and certificate hostname
        HostnameVerifier hv = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) { return true; }
        };

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(hv);
        } catch (Exception e) {e.printStackTrace();}
    }

    protected Client getAuthenticatedClient() {
        String middleWareUser = propertiesService.getProperty("elmis.central.approver.name", false).getValue();
        String middleWarePassword = propertiesService.getProperty("elmis.central.password", false).getValue();

        HTTPBasicAuthFilter authFilter = new HTTPBasicAuthFilter(middleWareUser, middleWarePassword);
        Client authenticatedClient = getWSClient();
        authenticatedClient.addFilter(authFilter);
        return authenticatedClient;
    }
}