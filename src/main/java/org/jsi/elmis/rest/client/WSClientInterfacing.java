/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.rest.client;

import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import org.apache.log4j.Logger;
import org.jsi.elmis.dao.CommonDAO;
import org.jsi.elmis.model.ProcessingPeriod;
import org.jsi.elmis.model.Requisition;
import org.jsi.elmis.model.adapters.RequisitionAdapter;
import org.jsi.elmis.rest.client.model.Rnr;
import org.jsi.elmis.rest.request.RequisitionSearchCriteria;
import org.jsi.elmis.rest.request.RequisitionSearchRequest;
import org.jsi.elmis.rnr.RnR;
import org.jsi.elmis.rnr.RnRLineItem;
import org.jsi.elmis.service.interfaces.MiscellaneousService;
import org.jsi.elmis.service.interfaces.OfflineRnRService;
import org.jsi.elmis.service.interfaces.PropertiesService;
import org.jsi.elmis.service.interfaces.RnRService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class WSClientInterfacing extends ClientCommon {

    @Autowired
    OfflineRnRService offlineRnRService;
    @Autowired
    RnRService rnRService;
    @Autowired
    MiscellaneousService miscService;
    @Autowired
    CommonDAO commonDAO;
    @Autowired
    RequisitionAdapter requisitionAdapter;
    private final Logger log = Logger.getLogger(getClass());

    public Requisition initiateRnr(Long facilityId,
                                   Long programId,
                                   Long periodId,
                                   String sourceApplication,
                                   Boolean emergency) {


        //TODO: download previous missing requisitions according to given criteria
        rnRService.downloadAndSaveRnrs(facilityId.intValue(), periodId.intValue(), programId.intValue(), emergency);

        Rnr rnr;
        Map<String, Object> responseMap;
        String sourceApp = propertiesService.getProperty("installation.type", false).getValue().equalsIgnoreCase("DM") ? "eLMIS_DM" : "eLMIS_FE";
        String centralELMISURL = propertiesService.getProperty("elmis.central.url", false).getValue();
        try {
            StringBuilder initiateRnrURL = new StringBuilder();
            initiateRnrURL
                    .append(centralELMISURL).append("/rest-api/requisitions/initiate?")
                    .append("facilityId=" + facilityId + "&")
                    .append("programId=" + programId + "&")
                    .append("periodId=" + periodId + "&")
                    .append("sourceApplication=" + sourceApp + "&")
                    .append("emergency=" + emergency);
            log.debug("posting to : " + initiateRnrURL.toString());
            webResource = getAuthenticatedClient().resource(initiateRnrURL.toString());
            responseMap = webResource.accept("application/json").type("application/json").post(Map.class);

            if (responseMap.containsKey("error")) {
                log.debug(responseMap.get("error").toString());
                return null;
            }
        } catch (Exception ex) {
            return null;
        }

        String responsePayload = getGson().toJson(responseMap.get("requisitionId"));
        log.debug("response " + responsePayload);
        rnr = getGson().fromJson(responsePayload, Rnr.class);
        List<RnRLineItem> allLineItems = new ArrayList<>();

        allLineItems.addAll(rnr.getFullSupplyLineItems());
        allLineItems.addAll(rnr.getNonFullSupplyLineItems());

        rnr.setAllLineItems(allLineItems);
        return rnRService.saveRnr(rnr);
    }

    public List<Rnr> downloadRnrs(RequisitionSearchRequest requisitionSearchRequest) {
        List<Rnr> rnrs;
        Map<String, Object> responseMap;
        String centralELMISURL = propertiesService.getProperty("elmis.central.url", false).getValue();
        try {
            StringBuilder downloadRnrURL = new StringBuilder();
            downloadRnrURL
                    .append(centralELMISURL).append("/rest-api/requisitions/search");

            log.debug("posting to : " + downloadRnrURL.toString());
            webResource = getAuthenticatedClient().resource(downloadRnrURL.toString());
            responseMap = webResource.accept("application/json").type("application/json").post(Map.class, requisitionSearchRequest);

            if (responseMap.containsKey("error")) {
                log.debug(responseMap.get("error").toString());
                return null;
            }
        } catch (Exception ex) {
            return null;
        }

        String responsePayload = getGson().toJson(responseMap.get("requisitions"));
        log.debug("response " + responsePayload);

        rnrs = getGson().fromJson(responsePayload, new TypeToken<ArrayList<Rnr>>() {
        }.getType());

        for (Rnr rnr : rnrs) {
            List<RnRLineItem> allLineItems = new ArrayList<>();

            allLineItems.addAll(rnr.getFullSupplyLineItems());
            allLineItems.addAll(rnr.getNonFullSupplyLineItems());

            rnr.setAllLineItems(allLineItems);
        }

        return rnrs;
    }

    public Map<String, List> downloadLogesticPeriods(RequisitionSearchCriteria searchCriteria) {
        List<Rnr> rnrs;
        List<ProcessingPeriod> processingPeriods;
        Map<String, Object> responseMap;
        Map<String, List> responseObject;
        String centralELMISURL = propertiesService.getProperty("elmis.central.url", false).getValue();
        try {
            StringBuilder downloadRnrURL = new StringBuilder();
            downloadRnrURL
                    .append(centralELMISURL).append("/rest-api/logistics/periods");

            log.debug("posting to : " + downloadRnrURL.toString());
            webResource = getAuthenticatedClient().resource(downloadRnrURL.toString());
            responseMap = webResource.accept("application/json").type("application/json").post(Map.class, searchCriteria);

            if (responseMap.containsKey("error")) {
                log.debug(responseMap.get("error").toString());
                return null;
            }
        } catch (Exception ex) {
            return null;
        }

        String responsePayload = getGson().toJson(responseMap.get("requisitions"));
        String responsePeriodPayload = getGson().toJson(responseMap.get("periods"));
        log.debug("response " + responsePayload);

        rnrs = getGson().fromJson(responsePayload, new TypeToken<ArrayList<Rnr>>() {
        }.getType());
        processingPeriods = getGson().fromJson(responsePeriodPayload, new TypeToken<ArrayList<ProcessingPeriod>>() {
        }.getType());
        addRnrLineItemss(rnrs);
        responseObject = new HashMap<>();
        responseObject.put("rnrs", rnrs);
        responseObject.put("periods", processingPeriods);
        return responseObject;
    }

    public void addRnrLineItemss(List<Rnr> rnrs) {
        if (rnrs != null && !rnrs.isEmpty()) {
            for (Rnr rnr : rnrs) {
                List<RnRLineItem> allLineItems = new ArrayList<>();
                allLineItems.addAll(rnr.getFullSupplyLineItems());
                allLineItems.addAll(rnr.getNonFullSupplyLineItems());
                rnr.setAllLineItems(allLineItems);
            }
        }
    }

}
