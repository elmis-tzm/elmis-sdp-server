/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2013 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License along with this program.  If not, see http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

package org.jsi.elmis.rnr;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;



/**
 * RegimenLineItem represents a regimenLineItem belonging to a Rnr.
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegimenLineItem{

  public static final String ON_TREATMENT = "patientsOnTreatment";
  public static final String INITIATED_TREATMENT = "patientsToInitiateTreatment";
  public static final String STOPPED_TREATMENT = "patientsStoppedTreatment";
  public static final String TYPE_NUMERIC = "regimen.reporting.dataType.numeric";
  public static final String REMARKS = "remarks";

  private String code;
  private String name;
  private Integer patientsOnTreatment = 0;
  private Integer patientsToInitiateTreatment = 0;
  private Integer patientsStoppedTreatment = 0;

  private Integer patientsOnTreatmentAdult = 0;
  private Integer patientsToInitiateTreatmentAdult = 0;
  private Integer patientsStoppedTreatmentAdult = 0;

  private Integer patientsOnTreatmentChildren = 0;
  private Integer patientsToInitiateTreatmentChildren = 0;
  private Integer patientsStoppedTreatmentChildren = 0;

  private String remarks;
  private Integer regimenDisplayOrder;

  private Boolean skipped = true;

}
