/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.controllers;

import org.jsi.elmis.exceptions.InsufficientNodeProductException;
import org.jsi.elmis.exceptions.UnavailableNodeProductException;
import org.jsi.elmis.model.*;
import org.jsi.elmis.rest.client.WSClientMiddleWare;
import org.jsi.elmis.service.interfaces.TransactionService;
import org.jsi.elmis.service.interfaces.UserManagementService;
import org.jsi.elmis.websocket.notifications.Notification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.core.MediaType;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * Created by Mekbib on 7/13/2017.
 */
@Controller
public class MiddleWareController {

    @Autowired
    WSClientMiddleWare wsClientMiddleWare;

    @Autowired
    TransactionService txnService;

    @RequestMapping(value = "/rest-api/store/facility-stock-request", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON)
    public @ResponseBody
    Integer requestStockTransfer(@RequestBody StockTransfer stockTransfer) {
        return wsClientMiddleWare.requestStockTransfer(stockTransfer);
    }

    @RequestMapping(value = "/rest-api/store/facility-stock-request", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON)
    public @ResponseBody Integer updateStockTransfer(@RequestBody StockTransfer stockTransfer) {
        return wsClientMiddleWare.updateStockTransfer(stockTransfer);
    }

    @RequestMapping(value = "/rest-api/store/middleware/notifications/{userUUID}/{status}", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON)
    public @ResponseBody List<Notification> getStockTransferNotifications(@PathVariable("userUUID") String userUUID,
                                                                          @PathVariable("status") String status) {
        return wsClientMiddleWare.getStockTransferNotifications(userUUID, status);
    }

    @RequestMapping(value = "/rest-api/store/middleware/notification", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON)
    public @ResponseBody Integer updateNotification(@RequestBody Notification notification) {
        return wsClientMiddleWare.updateNotification(notification);
    }

    @RequestMapping(value = "/rest-api/store/stock-transfer/reverse-txn/{stockTransferId}/{userId}", method = RequestMethod.GET)
    public @ResponseBody Integer reverseStockTransferTransaction(@PathVariable("stockTransferId") Integer stockTransferId,
                                                                 @PathVariable("userId") Integer userId)
            throws InsufficientNodeProductException, UnavailableNodeProductException {
        return txnService.reverseStockTransferTransaction(stockTransferId, userId);
    }

    @RequestMapping(value = "/rest-api/store/middleware/facility-stock-transfer", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public @ResponseBody List<StockTransferAugmented> getFacilityStockTransfers() {
        return wsClientMiddleWare.getFacilityStockTransfers();
    }

    @RequestMapping(value = "/rest-api/store/middleware/stock-transfer-augmented/{stockTransferId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public @ResponseBody StockTransferAugmented getStockTransfersAugmented(@PathVariable("stockTransferId") Integer stockTransferId) {
        return wsClientMiddleWare.getStockTransferAugmented(stockTransferId);
    }

    @RequestMapping(value = "/rest-api/store/middleware/stock-transfers-augmented", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public @ResponseBody List<StockTransferAugmented> getStockTransfersAugmentedBySearchParams(
            @RequestParam(value = "requester-code", required = false) String  requesterCode,
            @RequestParam(value = "supplier-code", required = false) String supplerCode,
            @RequestParam(value = "status", required = false) String status,
            @RequestParam(value = "initiation-from", required = false) Date initiationFrom,
            @RequestParam(value = "initiation-to", required = false) Date initiationTo,
            @RequestParam(value = "last-update-from", required = false) Date lastUpdateFrom,
            @RequestParam(value = "last-update-to", required = false) Date lastUpdateTo
    ) {
        return wsClientMiddleWare.getStockTransfers(requesterCode, supplerCode, initiationFrom, initiationTo, status, lastUpdateFrom, lastUpdateTo );
    }

    @RequestMapping(value = "/rest-api/store/middleware/interaction-policy", method = POST, consumes = MediaType.APPLICATION_JSON)
    public @ResponseBody int createInteractionPolicies(@RequestBody List<FacilityInteractionPolicy> interactionPolicies){
        return wsClientMiddleWare.createInteractionPolicies(interactionPolicies);
    }

    @RequestMapping(value = "/rest-api/store/middleware/interaction-policy", method = DELETE, consumes = MediaType.APPLICATION_JSON)
    public @ResponseBody int deleteInteractionPolicty(@RequestBody List<FacilityInteractionPolicy> interactionPolicies){
        return wsClientMiddleWare.deleteInterationPolicies(interactionPolicies);
    }

    @RequestMapping(value = "/rest-api/store/middleware/interaction-policy/facility/{from-facility-code}/{action-type}", method = GET, produces = MediaType.APPLICATION_JSON)
    public @ResponseBody List<Facility> selectFacilitiesInteractingWithFacility(@PathVariable("from-facility-code") String facilityCode, @PathVariable("action-type") String actionType){
        return wsClientMiddleWare.getFacilitiesInteractingWithFacility(facilityCode, actionType);
    }

    @RequestMapping(value = "/rest-api/store/middleware/facilities", method = GET, produces = MediaType.APPLICATION_JSON)
    public @ResponseBody List<Facility> getMiddlewareFacilities(){
        return wsClientMiddleWare.getAllFacilities();
    }
}
