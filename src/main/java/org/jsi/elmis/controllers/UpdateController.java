/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.controllers;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.jsi.elmis.model.UpdateHistory;
import org.jsi.elmis.service.interfaces.PropertiesService;
import org.jsi.elmis.service.interfaces.UpdateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.async.DeferredResult;

/**
 * @author Mesay S. Taye
 * 
 */

@Controller
public class UpdateController {
	
	@Autowired
	UpdateService updateService;
	
	@Autowired
	PropertiesService propertiesService;
	
    @Value("${client.update.file.path}")
    private String clientUpdateFilePath;
    
 
    @RequestMapping(value = "/rest-api/current-version", method = RequestMethod.GET)
    @ResponseBody public UpdateHistory getCurrentVersion(HttpServletResponse response){
            return updateService.selectCurrentVersion();
    }
    
    @RequestMapping(value = "/rest-api/latest-avaiable-update", method = RequestMethod.GET)
    @ResponseBody public DeferredResult<Map<String, String>> latestAvailabeUpdate(HttpServletResponse response){
    	DeferredResult<Map<String, String>> latestAvailableUpdate = new DeferredResult<>(null, new HashMap<>());
    	try {
			Thread.sleep(Long.valueOf(propertiesService.getProperty("updater.checkupdate.interval", false).getValue()));	
			Map<String, String> latestUpdateDetails = new HashMap<>();
			latestUpdateDetails.put("version", updateService.getLatestAvailableUpdate());
			latestUpdateDetails.put("change_log", updateService.getChangelog());//TODO : inject change-log.txt or something like that
			latestAvailableUpdate.setResult(latestUpdateDetails);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    	
		return latestAvailableUpdate;
    }
    
    @RequestMapping(value = "/rest-api/sdp-client", method = RequestMethod.GET)
    public void getFile(HttpServletResponse response) throws IOException {
            InputStream is = new FileInputStream(clientUpdateFilePath);
            IOUtils.copy(is, response.getOutputStream());
            response.flushBuffer();
    }
    
    @RequestMapping(value = "/rest-api/download-update", method = RequestMethod.GET)
    public void downloadUpdate(HttpServletResponse response) throws IOException {
    	updateService.downloadUpdates();
    }
    
    @RequestMapping(value = "/rest-api/execute-update", method = RequestMethod.GET)
    public void executeUpdate(HttpServletResponse response) throws IOException {
    	updateService.executeUpdates();
    }

}
