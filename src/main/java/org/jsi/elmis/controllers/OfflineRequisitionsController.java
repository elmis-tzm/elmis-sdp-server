/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jsi.elmis.common.constants.ELMISConstants;
import org.jsi.elmis.model.OfflineRnRStatus;
import org.jsi.elmis.model.ProcessingPeriod;
import org.jsi.elmis.model.Requisition;
import org.jsi.elmis.model.adapters.RequisitionAdapter;
import org.jsi.elmis.rest.client.WSClientInterfacing;
import org.jsi.elmis.rest.client.model.Rnr;
import org.jsi.elmis.rest.request.RequisitionSearchRequest;
import org.jsi.elmis.service.factory.RnRServiceConfiguration;
import org.jsi.elmis.service.interfaces.OfflineRnRService;
import org.jsi.elmis.service.interfaces.RnRIntializerService;
import org.jsi.elmis.service.interfaces.RnRService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class OfflineRequisitionsController {

    @Autowired
    private OfflineRnRService rnrService;
    @Autowired
    private RnRService mainRnRService;
    @Autowired
    WSClientInterfacing wsClientInterfacing;
    RnRIntializerService intializerService;
    @Autowired
    RnRServiceConfiguration serviceConfiguration;

    @RequestMapping(value = "/rest-api/offline-rnr/sdp-requisitions", method = RequestMethod.POST)
    public @ResponseBody
    Integer saveRnR(@RequestBody Requisition requisition) {
        return rnrService.saveRnR(requisition);
    }

    @RequestMapping(value = "/rest-api/offline-rnr/sync", method = RequestMethod.POST)
    public @ResponseBody
    Requisition syncRnR(@RequestBody Requisition requisition) {
        intializerService=serviceConfiguration.getIntializerService();
       return intializerService.syncronize(requisition);
    }

    @RequestMapping(value = "/rest-api/offline-rnr/sdp-requisitions/update", method = RequestMethod.POST)
    public @ResponseBody
    Integer updateRnR(@RequestBody Requisition requisition) {
        return rnrService.updateRnR(requisition);
    }

    @RequestMapping(value = "/rest-api/offline-rnr/rnr/monthly/{periodid}", method = RequestMethod.GET)
    public @ResponseBody
    List<Requisition> rnrsForPeriod(
            @PathVariable(value = "periodid") Integer periodId) {
        return rnrService.getOfflineRequisitionsByPeriodId(periodId);
    }

    @RequestMapping(value = "/rest-api/offline-rnr/delete/{id}", method = RequestMethod.GET)
    public @ResponseBody
    Integer deleteRnR(@PathVariable(value = "id") Integer id) {
        return rnrService.deleteRnR(id);
    }

    @RequestMapping(value = "/rest-api/offline-rnr/status/{requisitionId}", method = RequestMethod.GET)
    public @ResponseBody
    OfflineRnRStatus getOfflineRnRStatus(@PathVariable(value = "requisitionId") Integer requisitionId) {
        return rnrService.getOfflineRnRStatusByRequisitionId(requisitionId);
    }

    @RequestMapping(value = "/rest-api/offline-rnr/rnr/monthly/{program}/{facility}/{emergency}", method = RequestMethod.GET)
    public @ResponseBody
    Map<String, List> rnrsForFacility(
            @PathVariable(value = "program") Long programId,
            @PathVariable(value = "facility") Long facilityId,
            @PathVariable(value = "emergency") Boolean emergency) {
//        this is synced from the databae
        Map<String, List> rnrsPeriodsMap = rnrService.getOfflineRequisitionsByFacilityProgram(programId.intValue(), facilityId.intValue(), emergency);

        return rnrsPeriodsMap;
    }
}
