/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.controllers;

import java.util.List;

import javax.ws.rs.core.MediaType;

import org.jsi.elmis.model.SMS;
import org.jsi.elmis.service.interfaces.MessagingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Mesay S. Taye
 * 
 */

@Controller
public class MessagingController {
	
	@Autowired
	MessagingService messagingService;
 
    @RequestMapping(value = "/rest-api/messaging/sms", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON)
    public
    @ResponseBody
    Boolean sendSMSs(@RequestBody List<SMS> SMSs) {
        return messagingService.sendSMSs(SMSs);
    }
}
