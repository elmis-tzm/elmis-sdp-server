package org.jsi.elmis.controllers;/*
 * This program was produced for the U.S. Agency for International Development. It was prepared by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed under MPL v2 or later.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the Mozilla Public License as published by the Mozilla Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 *
 * You should have received a copy of the Mozilla Public License along with this program. If not, see http://www.mozilla.org/MPL/
 */

import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MediaType;

import org.jsi.elmis.backup.BackupFactory;
import org.jsi.elmis.backup.IDatabaseBackup;
import org.jsi.elmis.model.ApprovalHierarchyDiagramNode;
import org.jsi.elmis.model.StockRequestPath;
import org.jsi.elmis.model.StockRequisitionApprovalStatus;
import org.jsi.elmis.model.dto.StockRequistionDTO;
import org.jsi.elmis.service.interfaces.StockRequestPathService;
import org.jsi.elmis.service.interfaces.StockRequisitionApprovalStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.async.DeferredResult;

@Controller
public class StockRequisitionApprovalController {
    @Autowired
    private StockRequestPathService requestPathService;
    @Autowired
    private StockRequisitionApprovalStatusService requisitionApprovalStatusService;

    IDatabaseBackup databaseBackup;

    public StockRequisitionApprovalController() {

    }
    @Autowired
    public StockRequisitionApprovalController(BackupFactory backupFactory) {
        this.databaseBackup = backupFactory.createDatabaseBackupInstance();
    }
    @RequestMapping(value = "/rest-api/approval-heirarchi/requestPath", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON)
    public
    @ResponseBody
    Integer defineRequestPath(@RequestBody List<StockRequestPath> requestPaths, HttpServletRequest request) {
        return requestPathService.defineStockRequestPaths(requestPaths);
    }

    @RequestMapping(value = "/rest-api/approval-heirarchi/request-paths", method = RequestMethod.GET)
    public
    @ResponseBody
    List<StockRequestPath> loadRequestPathList() {
        List<StockRequestPath> requestPathList = null;
        requestPathList = requestPathService.loadRequestPathList();
        return requestPathList;
    }
    @RequestMapping(value = "/rest-api/approval-heirarchi/stock-request-approval", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON)
    public
    @ResponseBody
    Integer maintainUserApprovalStatus(@RequestBody StockRequisitionApprovalStatus requisitionApprovalStatus, HttpServletRequest request) {

        requisitionApprovalStatusService.changeRequestStatus(requisitionApprovalStatus);

        return 1;
    }


/*        @RequestMapping(value = "/rest-api/approval-heirarchi/pending-request", method = RequestMethod.GET)
    public
    @ResponseBody
    DeferredResult<List<StockRequistionDTO>> loadUserPendingRequests(HttpServletRequest request,@RequestParam Integer user) {
        final DeferredResult<List<StockRequistionDTO>> deferredResult = new DeferredResult<>(null, Collections.emptyList());
        new Thread(()->{
            try {
                Thread.sleep(5000);//TODO: simulate delay until properly implemented
                deferredResult.setResult(requisitionApprovalStatusService.loadRequestsForUserApproval(user));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
        return deferredResult;
    }


    @RequestMapping(value = "/rest-api/approval-heirarchi/approved-request", method = RequestMethod.GET)
    public
    @ResponseBody
    DeferredResult<List<StockRequistionDTO>> loadApprovedRequistionList(HttpServletRequest request,@RequestParam Integer node) {
        final DeferredResult<List<StockRequistionDTO>> deferredResult = new DeferredResult<>(null, Collections.emptyList());
        new Thread(()->{
            try {
                Thread.sleep(5000);//TODO: simulate delay until properly implemented
                deferredResult.setResult(requisitionApprovalStatusService.loadApprovedRequistionList(node));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
        return deferredResult;
    }*/

    protected Long loggedInUserId(HttpServletRequest request) {
        return (Long) request.getSession().getAttribute("USER_ID");
    }


    @RequestMapping(value = "/rest-api/approval-heirarchi/pending-request-notification", method = RequestMethod.GET)
    public
    @ResponseBody
    DeferredResult<List<StockRequistionDTO>> loadUserPendingRequestMessages(@RequestParam Integer user) {
        final DeferredResult<List<StockRequistionDTO>> defferedMessageNotificationList = new DeferredResult<>(null, Collections.emptyList());
        try {
            Thread.sleep(5000);
            defferedMessageNotificationList.setResult(requisitionApprovalStatusService.loadRequestsForUserApproval(user));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return defferedMessageNotificationList;
    }
    @RequestMapping(value = "/rest-api/approval-heirarchi/approved-request-notification", method = RequestMethod.GET)
    public
    @ResponseBody
    DeferredResult<List<StockRequistionDTO>> loadNodeApprovedRequestMessages(@RequestParam Integer node) {
        final DeferredResult<List<StockRequistionDTO>> defferedMessageNotificationList = new DeferredResult<>(null, Collections.emptyList());
        try {
            Thread.sleep(5000);
            defferedMessageNotificationList.setResult(requisitionApprovalStatusService.loadApprovedRequistionList(node));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return defferedMessageNotificationList;
    }

    @RequestMapping(value = "/rest-api/approval-heirarchi/diagram-node", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON)
    public
    @ResponseBody
    Integer saveHierarchyDiagramNodes(@RequestBody List<ApprovalHierarchyDiagramNode> hierarchyDiagramNodes) {

        requestPathService.saveHierarchyDiagramNodes(hierarchyDiagramNodes);
        return 0;
    }


    @RequestMapping(value = "/rest-api/test-backup", method = RequestMethod.GET)
    public
    @ResponseBody
    String testDatabaseBackup() {
String backupString="";
        try {
            backupString=databaseBackup.getDataBaseBackup();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return backupString;
    }

}
