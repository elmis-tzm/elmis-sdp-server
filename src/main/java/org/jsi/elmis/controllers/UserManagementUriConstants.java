/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.controllers;

public class UserManagementUriConstants {
	private static final String LOCATION = "/users/";
	
	public static final String AVAILABLE_USER_MGT_WS = LOCATION+"available";
	public static final String CREATE_USER = LOCATION+"createuser";
	public static final String UPDATE_USER = LOCATION+"updateuser";
	public static final String DELETE_USER = LOCATION+"deleteuser";
	public static final String SELECT_BY_USER_ID = LOCATION+"user/id/{id}";
	public static final String SELECT_BY_USER_EMAIL = LOCATION+"user/email/{email:.+}";
	public static final String VERIFY_USER = LOCATION+"verifyuser";
	public static final String USERS = LOCATION+"users";
	
	public static final String SAVE_ROLE = LOCATION+"createrole";
	public static final String ROLES = LOCATION+"roles";
	public static final String UPDATE_ROLE = LOCATION+"updaterole";
	public static final String DELETE_ROLE = LOCATION+"deleterole/{roleId}";
	
	public static final String ASSIGN_USER_NODE_ROLE = LOCATION+"assignusernoderole";
	public static final String UPATE_USER_NODE_ROLE = LOCATION+"updateusernoderole";
	
	public static final String CREATE_ROLE_RIGHT = LOCATION+"createroleright";
	public static final String SAVE_ROLE_RIGHTS = LOCATION+"saverolerights";
	public static final String GET_ROLE_RIGHTS = LOCATION+"rolerights";
	public static final String UPDATE_ROLE_RIGHT = LOCATION+"updateroleright";
	
	public static final String RIGHTS = LOCATION+"rights";

	public static final String LOGIN = LOCATION +"authenticate";

	public static final String GET_ALL_USER_NODE_ROLE_RIGHTS = LOCATION + "getusernoderolerights";
	public static final String REVOKE_RIGHTS_FROM_ROLE = LOCATION + "revokerightsfromrole";
	public static final String REVOKE_ROLE_FROM_USER = LOCATION + "revokerolesfromuser";
	public static final String REVOKE_NODE_FROM_USER = LOCATION + "revokenodesfromuser";

	public static final String GET_USER_ROLE_BY_NODE = LOCATION + "getuserrolesbyuserandnode";

	public static final String GET_ROLE_RIGHTS_BY_ROLE_ID = LOCATION + "getrolerightbyrole";

	public static final String AUTHENTICATE_USER_ON_CENTRAL = LOCATION + "interfacing/authenticate";
	
	public String getLocation(){
		return LOCATION;
	}
	
	public String getAvailableWebServices(){
		StringBuffer availableWS = new StringBuffer();
		availableWS.append(CREATE_USER);
		
		return "";
	}
}
