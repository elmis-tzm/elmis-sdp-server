/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.model.adapters;

import org.jsi.elmis.model.ProcessingPeriod;
import org.jsi.elmis.model.Requisition;
import org.jsi.elmis.model.RequisitionLineItem;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class RequisitionUtil {

    public static List<Requisition> mapRequistionIntiation(List<ProcessingPeriod> processingPeriodList, List<Requisition> centralRequisitionList, List<Requisition> offlineRequisitionList,Boolean dmInstallation) {
        List<Requisition> requisitionList = null;
        requisitionList = mergeCentralRequistionsWithOffline(centralRequisitionList, offlineRequisitionList,dmInstallation);
        if(processingPeriodList!=null && !processingPeriodList.isEmpty()) {
            ProcessingPeriod activiePeriod = processingPeriodList.get(0);
            for (ProcessingPeriod processingPeriod : processingPeriodList) {
                if (!isRequisitonExistForPeriod(requisitionList, processingPeriod)) {
                    Requisition requisition = createEmptyRequistionForPeriod(processingPeriod);
                    requisitionList.add(requisition);
                }

            }
        }
        requisitionList = sort(requisitionList);
        if (requisitionList != null && !requisitionList.isEmpty()) {
            requisitionList.get(0).setReadyForEdit(true);
        }
        return requisitionList;
    }

    private static Requisition createEmptyRequistionForPeriod(ProcessingPeriod processingPeriod) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date periodDate=null;
        try {
             periodDate = sdf.parse(processingPeriod.getStringStartDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Requisition requisition = new Requisition();
        requisition.setPeriodId(processingPeriod.getId());
        requisition.setPeriodName(processingPeriod.getName());
        requisition.setPeriodStart(periodDate);
        requisition.setStatus("Not Started");
        return requisition;
    }

    private static boolean isRequisitonExistForPeriod(List<Requisition> requisitions, ProcessingPeriod period) {
        for (Requisition requisition : requisitions) {
            if (requisition.getPeriodId().equals(period.getId())) {
                return true;
            }
        }
        return false;
    }

    private static List<Requisition> mergeCentralRequistionsWithOffline(List<Requisition> centralRequisitions, List<Requisition> offlineRequisitions,Boolean dmInstallation) {
        List<Requisition> mergedRequisitionList = new ArrayList<>();
        if(offlineRequisitions!=null && !offlineRequisitions.isEmpty()) {
            for (Requisition offRequisition : offlineRequisitions) {
                if (!isRequistionExist(offRequisition, centralRequisitions)) {
                    mergedRequisitionList.add(offRequisition);
                }
            }
        }
        if(centralRequisitions!=null && !centralRequisitions.isEmpty()) {
            for (Requisition requisition : centralRequisitions) {
                Requisition offlineRequisition = getOfflineRequistionMappedToCentral(requisition, offlineRequisitions);
                if (offlineRequisition == null) {
                    mergedRequisitionList.add(requisition);
                } else { if(dmInstallation) {
                    mergedRequisitionList.add(syncRequisitionBalance(requisition, offlineRequisition));
                }else{
                    mergedRequisitionList.add(syncRequisitionAmcValue(requisition, offlineRequisition));
                }
                }
            }
        }
        return mergedRequisitionList;
    }

    private static boolean isRequistionExist(Requisition requisition, List<Requisition> requisitions) {
        if(requisitions!=null && !requisitions.isEmpty()) {
            for (Requisition r : requisitions) {
                if (r.getPeriodId().equals(requisition.getPeriodId())) {
                    return true;
                }
            }
        }
        return false;
    }

    private static Requisition getOfflineRequistionMappedToCentral(Requisition requisition, List<Requisition> requisitions) {
        if(requisitions!=null && !requisitions.isEmpty()) {
            for (Requisition r : requisitions) {
                if (r.getPeriodId().equals(requisition.getPeriodId())) {
                    return r;
                }
            }
        }
        return null;
    }

    private static Requisition syncRequisitionBalance(Requisition centralRequisition, Requisition offlineRequisition) {
        if (centralRequisition.getRequisitionLineItems() != null && offlineRequisition.getRequisitionLineItems() != null) {
            for (RequisitionLineItem lineItem : centralRequisition.getRequisitionLineItems()) {
                for (RequisitionLineItem requisitionLineItem : offlineRequisition.getRequisitionLineItems()) {
                    if (requisitionLineItem.getId().equals(lineItem.getId())) {
                        requisitionLineItem.setBeginningBalance(lineItem.getBeginningBalance());
                        continue;
                    }
                }
            }
        }
        return offlineRequisition;
    }
    private static Requisition syncRequisitionAmcValue(Requisition centralRequisition, Requisition offlineRequisition) {
        if (centralRequisition.getRequisitionLineItems() != null && offlineRequisition.getRequisitionLineItems() != null) {
            for (RequisitionLineItem lineItem : centralRequisition.getRequisitionLineItems()) {
                for (RequisitionLineItem requisitionLineItem : offlineRequisition.getRequisitionLineItems()) {
                    if (requisitionLineItem.getId().equals(lineItem.getId())) {
                        requisitionLineItem.setAmc(lineItem.getAmc());
                        continue;
                    }
                }
            }
        }
        return offlineRequisition;
    }
    private static List<Requisition> sort(List<Requisition> requisitionList) {
if(requisitionList!=null && !requisitionList.isEmpty()){
            requisitionList.sort(new Comparator<Requisition>() {
                @Override
                public int compare(Requisition r1, Requisition r2) {

                    return r1.getPeriodStart().compareTo(r2.getPeriodStart());
                }
            });
        }
        return requisitionList;
    }
}
