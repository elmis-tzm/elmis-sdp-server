/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.model;

public class NodeProgram {
    private Integer id;

    private Integer nodeId;

    private Integer programId;
private  String programCode;
    private Boolean dispensingPoint;
    private Boolean selectedBefore;
    private  Boolean selectedNow;

    public String getProgramCode() {
        return programCode;
    }

    public void setProgramCode(String programCode) {
        this.programCode = programCode;
    }

    public Boolean getSelectedBefore() {
        return selectedBefore;
    }

    public void setSelectedBefore(Boolean selectedBefore) {
        this.selectedBefore = selectedBefore;
    }

    public Boolean getSelectedNow() {
        return selectedNow;
    }

    public void setSelectedNow(Boolean selectedNow) {
        this.selectedNow = selectedNow;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNodeId() {
        return nodeId;
    }

    public void setNodeId(Integer nodeId) {
        this.nodeId = nodeId;
    }

    public Integer getProgramId() {
        return programId;
    }

    public void setProgramId(Integer programId) {
        this.programId = programId;
    }

    public Boolean getDispensingPoint() {
        return dispensingPoint;
    }

    public void setDispensingPoint(Boolean dispensingPoint) {
        this.dispensingPoint = dispensingPoint;
    }
}