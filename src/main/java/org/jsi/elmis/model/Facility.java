/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.model;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Mesay S. Taye
 *
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Facility {
    private Integer id;

    private String code;

    private String name;

    private String description;

    private String gln;
    @JsonProperty(value="mainPhone")
    @SerializedName("mainPhone")
    private String mainphone;

    private String fax;

    private String address1;

    private String address2;
    @JsonProperty(value="geographicZoneId")
    @SerializedName("geographicZoneId")
    private Integer geographiczoneid;
    @JsonProperty(value="typeId")
    @SerializedName("typeId")
    private Integer typeid;
    @JsonProperty(value="catchmentPopulation")
    @SerializedName("catchmentPopulation")
    private Integer catchmentpopulation;

    private BigDecimal latitude;

    private BigDecimal longitude;

    private BigDecimal altitude;
    @JsonProperty(value="operatedById")
    @SerializedName("operatedById")
    private Integer operatedbyid;
    @JsonProperty(value="coldStorageGrossCapacity")
    @SerializedName("coldStorageGrossCapacity")
    private BigDecimal coldstoragegrosscapacity;
    @JsonProperty(value="coldStorageNetCapacity")
    @SerializedName("coldStorageNetCapacity")
    private BigDecimal coldstoragenetcapacity;
    @JsonProperty(value="suppliesOthers")
    @SerializedName("suppliesOthers")
    private Boolean suppliesothers;

    private Boolean sdp;

    private Boolean online;

    private Boolean satellite;
    @JsonProperty(value="parentFacilityId")
    @SerializedName("parentFacilityId")
    private Integer parentfacilityid;
    @JsonProperty(value="hasElectricity")
    @SerializedName("hasElectricity")
    private Boolean haselectricity;
    @JsonProperty(value="hasElectronicScc")
    @SerializedName("hasElectronicScc")
    private Boolean haselectronicscc;
    @JsonProperty(value="hasElectronicDar")
    @SerializedName("hasElectronicDar")
    private Boolean haselectronicdar;

    private Boolean active;
    @JsonProperty(value="goLiveDate")
    @SerializedName("goLiveDate")
    private Date golivedate;
    @JsonProperty(value="goDownDate")
    @SerializedName("goDownDate")
    private Date godowndate;

    private String comment;
    @JsonProperty(value="dataReportable")
    @SerializedName("dataReportable")
    private Boolean datareportable;
    @JsonProperty(value="virtualFacility")
    @SerializedName("virtualFacility")
    private Boolean virtualfacility;

    private Integer createdby;

    private Date createddate;

    private Integer modifiedby;

    private Date modifieddate;

}