/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.model;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.AssertTrue;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.jsi.elmis.rest.result.UserNodeRoleRightResult;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author Mesay S. Taye
 *
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_EMPTY)
public class User {
    private Integer id;
    @SerializedName(value = "userName")
    @NotEmpty(message = "User name required", groups = {Edit.class, New.class})
    private String username;
    
    @SerializedName(value = "password")
    @NotEmpty(message = "Password is required", groups = {New.class})
    private String password;
    
    @NotEmpty(message = "confirm password is required",  groups = {New.class})
	private String confirmPassword;
    
    @SerializedName(value = "firstName")
    @NotEmpty(message = "first name is required", groups = {Edit.class, New.class})
    private String firstname;
    
    @SerializedName(value = "lastName")
    @NotEmpty(message = "last name is required", groups = {Edit.class, New.class})
    private String lastname;
    
    @SerializedName(value = "employeeId")
    private String employeeid;
    
    @SerializedName(value = "jobTitle")
    private String jobtitle;
    
    @SerializedName(value = "primaryNotificationMethod")
    private String primarynotificationmethod;
    
    @SerializedName(value = "officePhone")
    private String officephone;
    
    @SerializedName(value = "cellPhone")
    private String cellphone;
    
    @SerializedName(value = "email")
    @Email(message = "Email is invalid", groups = {Edit.class, New.class})
    @NotEmpty(message = "email is required", groups = {Edit.class, New.class})
    private String email;
    
    @SerializedName(value = "supervisorId")
    private Integer supervisorid;
    
    @SerializedName(value = "facilityId")
    private Integer facilityid;
    
    @SerializedName(value = "active")
    private Boolean active;
    
    @SerializedName(value = "vendorId")
    private Integer vendorid;
    
    @SerializedName(value = "createdBy")
    private Integer createdby;
    
    @SerializedName(value = "createdDate")
    private Date createddate;
    
    @SerializedName(value = "modifiedBy")
    private Integer modifiedby;
    
    @SerializedName(value = "modifiedDate")
    private Date modifieddate;
    
    @SerializedName(value = "verified")
    private Boolean verified;

    private Boolean deleted;
    @SerializedName(value = "uuid")
    private String uuid;
    
    private List<UserNode> userNodes;
    
    @AssertTrue(message = "password and confirm password dosen't match",  groups = {Edit.class, New.class})
	private boolean ispasswordConfirm() {
		if (password == null) {
			return confirmPassword == null;
		} else {
			return password.equals(confirmPassword);
		}
	}
    
    public interface New {
    }
 
    public interface Edit {
    }

	@Override
	public String toString() {
		return "User [username=" + username + ", firstname=" + firstname
				+ ", lastname=" + lastname + ", email=" + email + "]";
	}
}