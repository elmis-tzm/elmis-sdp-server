/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.model;

import java.math.BigDecimal;
import java.util.Date;

import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Mesay S. Taye
 *
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Product {
	
    private Integer id;

    private String code;

    @SerializedName("alternateItemCode")
    private String alternateitemcode;

    private String manufacturer;

    @SerializedName("manufacturerCode")
    private String manufacturercode;

    @SerializedName("manufacturerBarCode")
    private String manufacturerbarcode;

    @SerializedName("mohBarCode")
    private String mohbarcode;

    private String gtin;

    private String type;

    @SerializedName("displayOrder")
    private Integer displayorder;

    @SerializedName("primaryName")
    private String primaryname;

    @SerializedName("fullName")
    private String fullname;

    @SerializedName("genericName")
    private String genericname;

    @SerializedName("alternateName")
    private String alternatename;

    private String description;

    private String strength;

    @SerializedName("formId")
    private Integer formid;

    @SerializedName("dosageUnitId")
    private Integer dosageunitid;

    @SerializedName("categoryId")
    private Integer categoryid;

    private Integer productgroupid;

    @SerializedName("dispensingUnit")
    private String dispensingunit;

    @SerializedName("dosesPerDispensingUnit") 
    private Short dosesperdispensingunit;

    @SerializedName("packSize") 
    private Short packsize;

    @SerializedName("alternatePackSize") 
    private Short alternatepacksize;

    @SerializedName("storeRefrigerated") 
    private Boolean storerefrigerated;

    @SerializedName("storeRoomTemperature") 
    private Boolean storeroomtemperature;

    private Boolean hazardous;

    private Boolean flammable;

    @SerializedName("controlledSubstance") 
    private Boolean controlledsubstance;

    @SerializedName("lightSensitive") 
    private Boolean lightsensitive;

    @SerializedName("approvedByWHO") 
    private Boolean approvedbywho;

    @SerializedName("contraceptiveCYP") 
    private BigDecimal contraceptivecyp;
    
    @SerializedName("packLength") 
    private BigDecimal packlength;

    @SerializedName("packWidth") 
    private BigDecimal packwidth;

    @SerializedName("packHeight") 
    private BigDecimal packheight;

    @SerializedName("packWeight") 
    private BigDecimal packweight;

    @SerializedName("packsPerCarton") 
    private Short packspercarton;

    @SerializedName("cartonLength") 
    private BigDecimal cartonlength;

    @SerializedName("cartonWidth") 
    private BigDecimal cartonwidth;

    @SerializedName("cartonHeight") 
    private BigDecimal cartonheight;

    @SerializedName("cartonsPerPallet") 
    private Short cartonsperpallet;

    @SerializedName("expectedShelfLife") 
    private Short expectedshelflife;

    @SerializedName("specialStorageInstructions") 
    private String specialstorageinstructions;

    @SerializedName("specialTransportInstructions") 
    private String specialtransportinstructions;
    
    private Boolean active;

    @SerializedName("fullSupply")  
    private Boolean fullsupply;

    private Boolean tracer;

    @SerializedName("roundToZero")  
    private Boolean roundtozero;

    private Boolean archived;

    private Short packroundingthreshold;

    private Integer createdby;

    private Date createddate;

    private Integer modifiedby;

    private Date modifieddate;

    private Integer qtyonhand;
    
    private Boolean locallyactive;

}