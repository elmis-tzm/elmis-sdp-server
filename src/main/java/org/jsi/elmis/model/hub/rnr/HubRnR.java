/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.model.hub.rnr;

import java.util.Date;

import org.jsi.elmis.model.Facility;
import org.jsi.elmis.model.ProcessingPeriod;

import com.google.gson.Gson;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class HubRnR {
	
	public static final String WEEKLY = "WEEKLY";
	public static final String FACILITY_MONTHLY = "FACILITY_MONTHLY";
	public static final String AGGREGATED = "AGGREGATED";
	
	public static final String PENDING = "PENDING";
	public static final String SUBMITTED = "SUBMMITTTED";
	
	private Integer id;
	
	private String facilityCode;
	
	private Facility facility;
	
	private ProcessingPeriod period;
	
	private String programCode;
	
	private String type;
	
	private String rnrJSON;
	
	private String status;
	
	private HubRnR parentRnR;
	
	private Date startDate;
	
	private Date endDate;
	
	private Report report;
	
	public HubRnR(String rnr,String type , String status){
		Gson gson = new Gson();
    	Report report = gson.fromJson(rnr, Report.class);
    	this.report = report;
    	this.facilityCode = report.getAgentCode();
    	this.programCode = report.getProgramCode();
    	this.type = type;
    	this.rnrJSON = rnr;
    	this.status = status;
	}
	
	public HubRnR(String rnr , String status){
		Gson gson = new Gson();
    	Report report = gson.fromJson(rnr, Report.class);
    	this.report = report;
    	this.facilityCode = report.getAgentCode();
    	this.programCode = report.getProgramCode();
    	this.rnrJSON = rnr;
    	this.status = status;
	}
	
	public HubRnR(Report report,String type , String status){
		this.report = report;
    	this.facilityCode = report.getAgentCode();
    	this.programCode = report.getProgramCode();
    	this.type = type;
    	Gson gson = new Gson();
    	this.rnrJSON = gson.toJson(report);
    	this.status = status;
	}

}
