/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.model.dto;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jsi.elmis.common.util.ElmisISOJsonDateSerializer;

import java.util.Date;

/**
 * Created by Mekbib on 9/15/2016.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SmartCareDispensationDTO {
    private String patientGuid;
    private String mslDrugId;
    private Integer providerId;
    private Integer userId;
    private String artNumber;
    private String hmisCode;
    @JsonSerialize(using = ElmisISOJsonDateSerializer.class)
    private Date nextVisitDate;
    @JsonSerialize(using = ElmisISOJsonDateSerializer.class)
    private Date transactionTime;
    @JsonSerialize(using = ElmisISOJsonDateSerializer.class)
    private Date dispensationDate;
    private String frequency;
    private Double quantityDispensed;
    private String unitOfMeasurement;
    private String doseStrength;
    private Double unitQuantityPerDose;
    private Integer sequence;
    private Integer transactionId;
    public String getArtNumber(){
    	
    	if(artNumber.contains("-"))
    	{
    		return artNumber;
    	}
        StringBuilder artNumberSB = new StringBuilder(artNumber);

        //1234-1234-1234-5
        //0, 9, 14
        artNumberSB.insert(4, '-');
        artNumberSB.insert(9, '-');
        artNumberSB.insert(14, '-');

        return artNumberSB.toString();
    }
    
    
    
}
