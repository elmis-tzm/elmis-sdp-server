/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.model.dto.transaction.details;

import java.util.List;
import java.util.Map;

import org.jsi.elmis.dao.ARVDispensingDAO;
import org.jsi.elmis.dao.AdjustmentDAO;
import org.jsi.elmis.dao.HIVTestingDAO;
import org.jsi.elmis.dao.ProductSourceDAO;
import org.jsi.elmis.dao.StoreDAO;
import org.jsi.elmis.dao.TransactionDAO;
import org.jsi.elmis.model.NodeTransaction;
import org.jsi.elmis.model.Transaction;
import org.jsi.elmis.model.TransactionType;
import org.jsi.elmis.service.interfaces.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TransactionDetailsFactory {
	
	@Autowired
	ARVDispensingDAO arvDispDAO; 
	@Autowired
	TransactionDAO txnDAO;
	@Autowired
	TransactionService txnService;
	@Autowired
	ProductSourceDAO sourceDAO;
	@Autowired
	AdjustmentDAO adjDAO;
	@Autowired
	HIVTestingDAO htDAO;
	@Autowired
	StoreDAO storeDAO;
	
	
	public TransactionDetails createInstance(String transactionType , Transaction txn){
		switch (transactionType) {
			case TransactionType.ARV_DISPENSING:
				ARVDispensingDetails arvDispDetials = new ARVDispensingDetails(txn , arvDispDAO.selectClientForTransacition(txn.getId())); 
				return arvDispDetials;
			case TransactionType.ISSUING:
				List<NodeTransaction> nodeTxns = txnDAO.selectNodeTxnsByTxnId(txn.getId());
				Map<String, NodeTransaction> nodeTxnsMap = txnService.getNodeTxnWithDirections(nodeTxns);
				IssuingDetails issuingDetails = new IssuingDetails(txn , nodeTxnsMap.get(NodeTransaction.SUPPLIER).getNode(),nodeTxnsMap.get(NodeTransaction.RECIPIENT).getNode());
				return issuingDetails;
			case TransactionType.ISSUE_OUT:
				IssueToFacilityDetails facilityIssueDetails =  new IssueToFacilityDetails(storeDAO.getIssueOutFacility(txn.getId()) , txn);
				return facilityIssueDetails;
			case TransactionType.RECEIVING:
				ReceivingDetails receivingDetails =  new ReceivingDetails(txn , sourceDAO.getSourceForReciept(txn.getId()));
				return receivingDetails;
			case TransactionType.POSITIVE_ADJUSTMENT:
				AdjustmentDetails positiveAdjustmentDetails = new AdjustmentDetails(txn , adjDAO.findAdjTypeForTxn(txn.getId()));
				return positiveAdjustmentDetails;
			case TransactionType.NEGATIVE_ADJUSTMENT:
				AdjustmentDetails negativeAdjustmentDetails = new AdjustmentDetails(txn ,  adjDAO.findAdjTypeForTxn(txn.getId()));
				return negativeAdjustmentDetails;
			case TransactionType.SCREENING_TEST:
				HIVTestingDetails screeningTestDetails =  new HIVTestingDetails(txn ,  htDAO.getHIVTestInfoForTxn(txn.getId()));
				return screeningTestDetails;
			case TransactionType.CONFIRMATORY_TEST:
				HIVTestingDetails conTestDetails = new HIVTestingDetails(txn , htDAO.getHIVTestInfoForTxn(txn.getId()));
				return conTestDetails;
			default:
				return new TransactionDetails(txn);
		}
			
	}
}
