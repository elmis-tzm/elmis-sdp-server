/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.jsi.elmis.dao.mappers.*;
import org.jsi.elmis.model.*;
import org.springframework.stereotype.Component;

/**
 * @author Mesay S. Taye
 *
 */
@Component
public class NodeProductDAO extends DAO{

/*	public int saveNodeProduct(NodeProduct nodeProduct){
		SqlSession session = getSqlMapper().openSession();
		NodeProductMapper mapper = session.getMapper(NodeProductMapper.class);
		int result = 0;
		
		try {
			result = mapper.insert(nodeProduct);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
	}*/
	
	public NodeProduct saveNodeProduct(NodeProduct nodeProduct){
		NodeProduct existingNodeProd = null;
       if (nodeProduct.getProgramProductId() != null && nodeProduct.getNodeId() != null){
    	   existingNodeProd = selectNodeProductByNodeandProduct(nodeProduct.getNodeId(), nodeProduct.getProgramProductId());
    	   if (existingNodeProd != null) nodeProduct.setId(existingNodeProd.getId());
		} else if (nodeProduct.getId() != null){
			existingNodeProd = selectNodeProductByPrimaryKey(nodeProduct.getId());
		}

		if(existingNodeProd != null){
			updateNodeProduct(nodeProduct);
		} else {
			insertNodeProduct(nodeProduct);
		}
		return nodeProduct;
	}

	public void createIfNotExists(NodeProduct npp, TransactionProduct txnProduct){
		if (npp.getId() == null) {
            insertNodeProduct(npp);
        }

            if(txnProduct.getBatchQuantities() != null || !txnProduct.getBatchQuantities().isEmpty()) {

                for (TransactionProgramProductBatch tppb :
                        txnProduct.getBatchQuantities()) {
                    NodeProgramProductBatch nppb = selectByNPPIdandPBId(npp.getId(), tppb.getProgramProductBatchId());
                    if (nppb == null) {
                        nppb = new NodeProgramProductBatch(null, npp.getId(), tppb.getProgramProductBatchId(), tppb.getQuantity());
                        insertNodeProductBatch(nppb);
                    }
                }
            }

	}

    public NodeProgramProductBatch selectByNPPIdandPBId(Integer nppId, Integer pbId){

        SqlSession session = getSqlMapper().openSession();
        NodeProgramProductBatchMapper mapper = session.getMapper(NodeProgramProductBatchMapper.class);
        NodeProgramProductBatch nppb = null;

        try {
            nppb = mapper.selectByNodeProgramProductandProductBatch(nppId, pbId);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.close();
        }
        return nppb;

    }
	
	public NodeProduct selectNodeProductByNodeandProduct(Integer nodeId, Integer ppId){
		
		SqlSession session = getSqlMapper().openSession();
		NodeProductMapper mapper = session.getMapper(NodeProductMapper.class);
		NodeProduct nodeProduct = null;
		
		try {
			nodeProduct = mapper.selectByProductIdandNodeID(nodeId, ppId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return nodeProduct;
		
	}
	
	public int insertNodeProduct(NodeProduct nodeProduct){
		
		SqlSession session = getSqlMapper().openSession();
		NodeProductMapper mapper = session.getMapper(NodeProductMapper.class);
		int result = 0;
		
		try {
			result = mapper.insert(nodeProduct);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
		
	}
	
	public int updateNodeProduct(NodeProduct nodeProduct){
		
		SqlSession session = getSqlMapper().openSession();
		NodeProductMapper mapper = session.getMapper(NodeProductMapper.class);
		int result = 0;
		
		try {
			result = mapper.updateByPrimaryKeySelective(nodeProduct);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
		
	}
	
	public int updateFacilityProductPhysicalCount(Integer periodId, String productCode){
		
		SqlSession session = getSqlMapper().openSession();
		NodeProductMapper mapper = session.getMapper(NodeProductMapper.class);
		int result = 0;
		
		try {
			result = mapper.updateFacilityPhysicalCountForProduct(periodId, productCode);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
	}
	public NodeProduct selectNodeProductByPrimaryKey(Integer nodeProductId){
		
		SqlSession session = getSqlMapper().openSession();
		NodeProductMapper mapper = session.getMapper(NodeProductMapper.class);
		NodeProduct nodeProduct = null;
		
		try {
			nodeProduct = mapper.selectByPrimaryKey(nodeProductId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return nodeProduct;
		
	}
	
	public Integer _selectTxnCountForNodeProductAfterDate(Integer nodeId, Integer ppId , Date txnDate){
		
		SqlSession session = getSqlMapper().openSession();
		NodeProductMapper mapper = session.getMapper(NodeProductMapper.class);
		Integer txnCount = null;
		
		try {
			txnCount = mapper.selectTxnCountForNodeProductAfterDate(nodeId, ppId ,txnDate);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return txnCount;
		
	}
	
	public Integer selectActionCountAfterDate(Integer nodeId, Integer ppId , Date txnDate){
		
		SqlSession session = getSqlMapper().openSession();
		NodeProductMapper mapper = session.getMapper(NodeProductMapper.class);
		Integer txnCount = null;
		
		try {
			txnCount = mapper.selectActionCountAfterDate(nodeId, ppId ,txnDate);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return txnCount;
		
	}
	
	public List<NodeProduct> selectNodeProducts(Integer nodeId){
		
		SqlSession session = getSqlMapper().openSession();
		NodeProductMapper mapper = session.getMapper(NodeProductMapper.class);
		List<NodeProduct> nodeProducts = null;
		
		try {
			nodeProducts = mapper.selectByNodeId(nodeId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return nodeProducts;
		
	}
	
	public List<NodeProduct> selectNodeProductsByProgram(Integer nodeId, Integer programId){
		
		SqlSession session = getSqlMapper().openSession();
		NodeProductMapper mapper = session.getMapper(NodeProductMapper.class);
		List<NodeProduct> nodeProducts = null;
		
		try {
			nodeProducts = mapper.selectByNodeIdAndProgramId(nodeId, programId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return nodeProducts;
		
	}
	
	public List<NodeProduct> selectNodeProductsByProductId(Integer ppId){
		
		SqlSession session = getSqlMapper().openSession();
		NodeProductMapper mapper = session.getMapper(NodeProductMapper.class);
		List<NodeProduct> nodeProducts = null;
		
		try {
			nodeProducts = mapper.selectByProgramProductId(ppId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return nodeProducts;
		
	}
	
	
	public List<NodeProduct> selectNodeProducts(String programCode, Boolean isExternal, Boolean isVirtual, Boolean includeConsumptionPoints){
		
		SqlSession session = getSqlMapper().openSession();
		NodeProductMapper mapper = session.getMapper(NodeProductMapper.class);
		List<NodeProduct> nodeProducts = null;
		
		try {
			nodeProducts = mapper.selectNodeProductsInProgram(programCode, isExternal, isVirtual, includeConsumptionPoints);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return nodeProducts;
		
	}

    public List<NodeProduct> selectNodeProducts(String programCode, Boolean isExternal, Boolean isVirtual){

        SqlSession session = getSqlMapper().openSession();
        NodeProductMapper mapper = session.getMapper(NodeProductMapper.class);
        List<NodeProduct> nodeProducts = null;

        try {
            nodeProducts = mapper.selectNodeProductsInProgram(programCode, isExternal, isVirtual, true);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.close();
        }
        return nodeProducts;

    }
	
	public List<NodeProduct> selectNodeProducts(Integer nodeId , String programCode , String facilityCode){
		
		SqlSession session = getSqlMapper().openSession();
		NodeProductMapper mapper = session.getMapper(NodeProductMapper.class);
		List<NodeProduct> nodeProducts = null;
		
		try {
			nodeProducts = mapper.selectByNodeIdAndProgram(nodeId, programCode, facilityCode);//TODO: remove hardcoded fcility code

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return nodeProducts;
	}
	
	public BigDecimal getBeginningBalanceFromTransactionHistory(Integer nodeId , Integer productId){
		
		SqlSession session = getSqlMapper().openSession();
		NodeProductMapper mapper = session.getMapper(NodeProductMapper.class);
		BigDecimal beginningBalance = null;
		
		try {
			beginningBalance = mapper.selectBeginningQtyFromTransactionHistory(nodeId, productId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return beginningBalance;
	}

	public Boolean nodeIsDispensingPointForProgram(Integer nodeId, Integer programId) {
		SqlSession session = getSqlMapper().openSession();
		NodeProgramMapper mapper = session.getMapper(NodeProgramMapper.class);
		NodeProgram nodeProgram = null;
		
		try {
			nodeProgram = mapper.selectNodeProgram(nodeId, programId , true);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return nodeProgram != null;
	}
	
	public Boolean nodeIsDispensingPointForProduct(Integer nodeId, Integer productId) {
		return null;
	}
	
	public PhysicalCount selectPhysicalCount(Integer physicalCountId) {
		SqlSession session = getSqlMapper().openSession();
		PhysicalCountMapper mapper = session.getMapper(PhysicalCountMapper.class);
		PhysicalCount pCount = null;
		
		try {
			pCount = mapper.selectByPrimaryKey(physicalCountId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return pCount;
	}

	
	public BigDecimal getFacilityStockOnHand(Integer productId) {
		SqlSession session = getSqlMapper().openSession();
		NodeMapper mapper = session.getMapper(NodeMapper.class);
		BigDecimal stockOnHand = null;
		
		try {
			stockOnHand = mapper.selectFacilityStockOnHand(productId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return stockOnHand;
	}
	
	public List<Map<String , Object>> getFacilityStockOnHand(String programCode) {
		SqlSession session = getSqlMapper().openSession();
		NodeProductMapper mapper = session.getMapper(NodeProductMapper.class);
		List<Map<String , Object>> facilityStock = null;
		
		try {
			facilityStock = mapper.selectFacilityStock(programCode);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return facilityStock;
	}

    public NodeProgramProductDailyStatus getNPPDAilyProgProdStatus(Date date, NodeProduct nodeProgProd) {
		SqlSession session = getSqlMapper().openSession();
		NodeProgramProductDailyStatusMapper nppMapper = session.getMapper(NodeProgramProductDailyStatusMapper.class);
		NodeProgramProductBatchDailyStatusMapper nppBmapper = session.getMapper(NodeProgramProductBatchDailyStatusMapper.class);


		NodeProgramProductDailyStatus nppDailyStatus = null;
		try{

			nppDailyStatus = nppMapper.selectNPPDAilyProgProdStatus(date, nodeProgProd.getId());

			if (nppDailyStatus == null) return null;

			for (NodeProgramProductBatch nppb: nodeProgProd.getNodeProgramProductBatches()) {
				NodeProgramProductBatchDailyStatus nppBatchDailyStatus = nppBmapper.selectNPPBDailyStatus(date, nppb.getId());
				if (nppBatchDailyStatus != null) nppDailyStatus.getNodeProgramProductBatchDailyStatuses().add(nppBatchDailyStatus);
			}

		} catch (Exception ex ) {
			ex.printStackTrace();
		} finally{
			session.close();
		}
		return nppDailyStatus;
    }

    public NodeProgramProductBatchDailyStatus getNPPBatchDAilyProgProdStatus(Date date, NodeProgramProductBatch nodeProgProdBatch) {
        SqlSession session = getSqlMapper().openSession();
        NodeProgramProductBatchDailyStatusMapper nppBmapper = session.getMapper(NodeProgramProductBatchDailyStatusMapper.class);


        NodeProgramProductBatchDailyStatus nppBatchDailyStatus = null;
        try{

            nppBatchDailyStatus = nppBmapper.selectNPPBDailyStatus(date, nodeProgProdBatch.getId());



        } catch (Exception ex ) {
            ex.printStackTrace();
        } finally{
            session.close();
        }
        return nppBatchDailyStatus;
    }



    public NodeProgramProductDailyStatus getPreviousDailyStatus(Integer nodeProgProdId, Date date) {
		SqlSession session = getSqlMapper().openSession();
		NodeProgramProductDailyStatusMapper mapper = session.getMapper(NodeProgramProductDailyStatusMapper.class);

		try{

			return mapper.selectPreviousNPPDAilyProgProdStatus(date, nodeProgProdId);

		} catch (Exception ex ) {
			ex.printStackTrace();
		} finally{
			session.close();
		}
		return null;
	}

	public void updateAllDailyBeginningBalancesAfterDate(NodeProduct nodeProgProd, Date date, BigDecimal signedTxnQty, TransactionProduct txnProd, Boolean txnIsPositive) {
		SqlSession session = getSqlMapper().openSession();
		NodeProgramProductDailyStatusMapper mapper = session.getMapper(NodeProgramProductDailyStatusMapper.class);
		NodeProgramProductBatchDailyStatusMapper nppbDSMapper = session.getMapper(NodeProgramProductBatchDailyStatusMapper.class);

		try{

			mapper.updateAllDailyBeginningBalancesAfterDate(date, nodeProgProd.getId(), signedTxnQty);

            for (int i = 0; i < nodeProgProd.getNodeProgramProductBatches().size(); i++) {
                nppbDSMapper.updateAllDailyBatchBeginningBalancesAfterDate(date,  nodeProgProd.getNodeProgramProductBatches().get(i).getId(), txnProd.getBatchQuantities().get(i).getSignedQuantity(txnIsPositive));
            }

		} catch (Exception ex ) {
			ex.printStackTrace();
		} finally{
			session.commit();
			session.close();
		}
	}

	public void updateAllDailyFinalBalancesSinceDate(NodeProduct nodeProgProd, Date date, BigDecimal signedTxnQty, TransactionProduct txnProd, Boolean txnIsPositive) {
		SqlSession session = getSqlMapper().openSession();
		NodeProgramProductDailyStatusMapper mapper = session.getMapper(NodeProgramProductDailyStatusMapper.class);
        NodeProgramProductBatchDailyStatusMapper nppbDSMapper = session.getMapper(NodeProgramProductBatchDailyStatusMapper.class);

		try{
			mapper.updateAllDailyFinalBalancesSinceDate(date, nodeProgProd.getId(), signedTxnQty);

            for (int i = 0; i < nodeProgProd.getNodeProgramProductBatches().size(); i++) {

                nppbDSMapper.updateAllDailyBatchFinalBalancesSinceDate(date, nodeProgProd.getNodeProgramProductBatches().get(i).getId(), txnProd.getBatchQuantities().get(i).getSignedQuantity(txnIsPositive));

            }

		} catch (Exception ex ) {
			ex.printStackTrace();
		} finally{
			session.commit();
			session.close();
		}
	}

	public void updateAllDailyMinBalancesSinceDate(NodeProduct nodeProgProd, Date date, BigDecimal signedTxnQty, TransactionProduct txnProd, Boolean txnIsPositive) {
		SqlSession session = getSqlMapper().openSession();
		NodeProgramProductDailyStatusMapper mapper = session.getMapper(NodeProgramProductDailyStatusMapper.class);
        NodeProgramProductBatchDailyStatusMapper nppbDSMapper = session.getMapper(NodeProgramProductBatchDailyStatusMapper.class);

		try{

			mapper.updateAllDailyMinBalancesSinceDate(date, nodeProgProd.getId(), signedTxnQty);

            for (int i = 0; i < nodeProgProd.getNodeProgramProductBatches().size(); i++) {

                nppbDSMapper.updateAllDailyBatchMinBalancesSinceDate(date, nodeProgProd.getNodeProgramProductBatches().get(i).getId(), txnProd.getBatchQuantities().get(i).getSignedQuantity(txnIsPositive));

            }

		} catch (Exception ex ) {
			ex.printStackTrace();
		} finally{
			session.commit();
			session.close();
		}
	}

    public void saveNodeProdDailyStatus(NodeProgramProductDailyStatus nppDailyStatus) {
        SqlSession session = getSqlMapper().openSession();
        NodeProgramProductDailyStatusMapper mapper = session.getMapper(NodeProgramProductDailyStatusMapper.class);
        NodeProgramProductBatchDailyStatusMapper nppbStatusMapper = session.getMapper(NodeProgramProductBatchDailyStatusMapper.class);

        try{
            mapper.insert(nppDailyStatus);

            for (NodeProgramProductBatchDailyStatus nppbDailyStatus:
                 nppDailyStatus.getNodeProgramProductBatchDailyStatuses()) {
                nppbStatusMapper.insert(nppbDailyStatus);
            }

        } catch (Exception ex ) {
            ex.printStackTrace();
        } finally{
            session.commit();
            session.close();
        }
    }

    public void insertNodeProdBatchDailyStatus(NodeProgramProductBatchDailyStatus nppbDailyStatus) {
        SqlSession session = getSqlMapper().openSession();
        NodeProgramProductBatchDailyStatusMapper nppbStatusMapper = session.getMapper(NodeProgramProductBatchDailyStatusMapper.class);

        try{
            nppbStatusMapper.insert(nppbDailyStatus);

        } catch (Exception ex ) {
            ex.printStackTrace();
        } finally{
            session.commit();
            session.close();
        }
    }

    public void updateNodeProdBatchDailyStatus(NodeProgramProductBatchDailyStatus nppbDailyStatus) {
        SqlSession session = getSqlMapper().openSession();
        NodeProgramProductBatchDailyStatusMapper nppbStatusMapper = session.getMapper(NodeProgramProductBatchDailyStatusMapper.class);

        try{
            nppbStatusMapper.updateByPrimaryKey(nppbDailyStatus);

        } catch (Exception ex ) {
            ex.printStackTrace();
        } finally{
            session.commit();
            session.close();
        }
    }


    public void updateNodeProdDailyStatus(NodeProgramProductDailyStatus nppDailyStatus) {
        SqlSession session = getSqlMapper().openSession();
        NodeProgramProductDailyStatusMapper mapper = session.getMapper(NodeProgramProductDailyStatusMapper.class);

        try{
            mapper.updateByPrimaryKey(nppDailyStatus);

        } catch (Exception ex ) {
            ex.printStackTrace();
        } finally{
            session.commit();
            session.close();
        }
    }

    public NodeProgramPRoductDailyTransactionSummary getNPPDailyTxnTotal(Date date, Integer nodeProgProdId, Integer txnTypeId) {
        SqlSession session = getSqlMapper().openSession();
        NodeProgramPRoductDailyTransactionSummaryMapper mapper = session.getMapper(NodeProgramPRoductDailyTransactionSummaryMapper.class);

        try{
            return mapper.selectByNodeProgProdIdAndDate(date, nodeProgProdId, txnTypeId);

        } catch (Exception ex ) {
            ex.printStackTrace();
        } finally{
            session.commit();
            session.close();
        }
        return null;
    }

    public void insertNPPDailyTxnSummary(NodeProgramPRoductDailyTransactionSummary nppDailyTxnSummary) {
        SqlSession session = getSqlMapper().openSession();
        NodeProgramPRoductDailyTransactionSummaryMapper mapper = session.getMapper(NodeProgramPRoductDailyTransactionSummaryMapper.class);

        try{

            mapper.insert(nppDailyTxnSummary);

        } catch (Exception ex ) {
            ex.printStackTrace();
        } finally{
            session.commit();
            session.close();
        }
    }

    public void updatePPDailyTxnSummary(NodeProgramPRoductDailyTransactionSummary nppDailyTxnSummary) {
        SqlSession session = getSqlMapper().openSession();
        NodeProgramPRoductDailyTransactionSummaryMapper mapper = session.getMapper(NodeProgramPRoductDailyTransactionSummaryMapper.class);

        try{

            mapper.updateByPrimaryKey(nppDailyTxnSummary);

        } catch (Exception ex ) {
            ex.printStackTrace();
        } finally{
            session.commit();
            session.close();
        }
    }

    public NodeProgramProductDailyAdjustmentSummary getNPPDailyAdjSummary(Date date, LossAdjustmentType laType, Integer nodeProgProdId) {
        SqlSession session = getSqlMapper().openSession();
        NodeProgramProductDailyAdjustmentSummaryMapper mapper = session.getMapper(NodeProgramProductDailyAdjustmentSummaryMapper.class);

        try{

            return mapper.selectNodeProgramProductDailyAdjustmentSummary(date, nodeProgProdId, laType.getName());

        } catch (Exception ex ) {
            ex.printStackTrace();
        } finally{
            session.close();
        }
        return null;
    }

    public void insertNPPDailyAdjustmentSummary(NodeProgramProductDailyAdjustmentSummary nppDailyAdjummary) {
        SqlSession session = getSqlMapper().openSession();
        NodeProgramProductDailyAdjustmentSummaryMapper mapper = session.getMapper(NodeProgramProductDailyAdjustmentSummaryMapper.class);

        try{

            mapper.insert(nppDailyAdjummary);

        } catch (Exception ex ) {
            ex.printStackTrace();
        } finally{
            session.commit();
            session.close();
        }
    }

    public void updatePPDailyAdjustmentSummary(NodeProgramProductDailyAdjustmentSummary nppDailyAdjummary) {
        SqlSession session = getSqlMapper().openSession();
        NodeProgramProductDailyAdjustmentSummaryMapper mapper = session.getMapper(NodeProgramProductDailyAdjustmentSummaryMapper.class);

        try{

            mapper.updateByPrimaryKey(nppDailyAdjummary);

        } catch (Exception ex ) {
            ex.printStackTrace();
        } finally{
            session.commit();
            session.close();
        }
    }

    public BigDecimal getBalance(Date date, Integer nppId) {
            SqlSession session = getSqlMapper().openSession();
            NodeProgramProductDailyStatusMapper mapper = session.getMapper(NodeProgramProductDailyStatusMapper.class);
            
            BigDecimal bal = null;

            try{

                bal = mapper.selectNodeProdBalanceOnDate(date, nppId);

            } catch (Exception ex ) {
                ex.printStackTrace();
            } finally{
                session.close();
            }
            return (bal == null)?BigDecimal.ZERO:bal;
    }

    public BigDecimal getUsableBalance(Date date, Integer nppId) {
        SqlSession session = getSqlMapper().openSession();
        NodeProgramProductDailyStatusMapper mapper = session.getMapper(NodeProgramProductDailyStatusMapper.class);

        try{

            return mapper.selectNodeProdUsableBalanceOnDate(date, nppId);

        } catch (Exception ex ) {
            ex.printStackTrace();
        } finally{
            session.close();
        }
        return null;
    }

	public Integer saveDailyStockStatusSubmission(DailyStockStatusSubmission dailyStockStatusSubmission) {
		SqlSession session = getSqlMapper().openSession();
        DailyStockStatusSubmissionMapper mapper = session.getMapper(DailyStockStatusSubmissionMapper.class);
        try{
            mapper.insert(dailyStockStatusSubmission);
        } catch (Exception ex ) {
        } finally{
        	session.commit();
            session.close();
        }
        return 0;
	}

	public List<DailyStockStatusSubmission> getFailedDailyStockStatusSubmissions() {
		 List<DailyStockStatusSubmission> failedDailyStockStatusSubmissions = new ArrayList<>();
		SqlSession session = getSqlMapper().openSession();
        DailyStockStatusSubmissionMapper mapper = session.getMapper(DailyStockStatusSubmissionMapper.class);
        try{
        	failedDailyStockStatusSubmissions = mapper.selectFailedSubmissions();
        } catch (Exception ex ) {
        } finally{
            session.close();
        }
        return failedDailyStockStatusSubmissions;
	}


	public int insertNodeProductBatch(NodeProgramProductBatch nppb){

		SqlSession session = getSqlMapper().openSession();
		NodeProgramProductBatchMapper mapper = session.getMapper(NodeProgramProductBatchMapper.class);
		int result = 0;

		try {
			result = mapper.insert(nppb);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;

	}

    public NodeProgramProductBatchDailyStatus getPreviousDailyNPPBatchStatus(Integer nodeProgProdBatchId, Date date) {
        SqlSession session = getSqlMapper().openSession();
        NodeProgramProductBatchDailyStatusMapper nppBmapper = session.getMapper(NodeProgramProductBatchDailyStatusMapper.class);
        NodeProgramProductBatchDailyStatus nppBatchDailyStatus = null;
        try{

            nppBatchDailyStatus = nppBmapper.selectLastNPPBatchDailyStatus(date, nodeProgProdBatchId);

        } catch (Exception ex ) {
            ex.printStackTrace();
        } finally{
            session.close();
        }
        return nppBatchDailyStatus;
    }

    public List<NodeProgramProductBatchBalance> getBatchBalancesOnDate(Date date, Integer nppId) {
        SqlSession session = getSqlMapper().openSession();
        NodeProgramProductDailyStatusMapper nppBDSmapper = session.getMapper(NodeProgramProductDailyStatusMapper.class);
        NodeProgramProductBatchMapper nppbMapper = session.getMapper(NodeProgramProductBatchMapper.class);
        List<NodeProgramProductBatchBalance> nppBatchBalances = new ArrayList<>();

        try{
            List<NodeProgramProductBatch> nppBatches = nppbMapper.selectByNodeProductId(nppId);

            for (NodeProgramProductBatch nppb: nppBatches) {

                     BigDecimal bal =    nppBDSmapper.selectNodeProdBatchBalanceOnDate(date, nppb.getId());
                     BigDecimal uBal =   nppBDSmapper.selectNodeProdBatchUsableBalanceOnDate(date, nppb.getId());

                     NodeProgramProductBatchBalance nppbBalance = new NodeProgramProductBatchBalance(new NodeProgramProductBatch(nppb.getId(),nppb.getNodeProgramProductId(),nppb.getProgramProductBatchId(),null), bal, (uBal == null)?bal:uBal);
                     //( id,  nodeProgramProductId,  programProductBatchId,  quantity)
                nppBatchBalances.add(nppbBalance);

            }

        } catch (Exception ex ) {
            ex.printStackTrace();
        } finally{
            session.close();
        }
        return nppBatchBalances;
    }
}