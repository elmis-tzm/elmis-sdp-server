/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.ibatis.session.SqlSession;
import org.jsi.elmis.common.constants.ELMISConstants;
import org.jsi.elmis.dao.mappers.FacilityNodeMapper;
import org.jsi.elmis.dao.mappers.NodeMapper;
import org.jsi.elmis.dao.mappers.NodeProductMapper;
import org.jsi.elmis.dao.mappers.NodeProgramMapper;
import org.jsi.elmis.dao.mappers.NodeTypeMapper;
import org.jsi.elmis.model.*;
import org.jsi.elmis.model.dto.FacilityNodeProgramDTO;
import org.springframework.stereotype.Component;


/**
 * @author Mesay S. Taye
 *
 */
@Component
public class NodeDAO extends DAO {

	public int saveNode(Node node){
		SqlSession session = getSqlMapper().openSession();
		NodeMapper mapper = session.getMapper(NodeMapper.class);
		int result = 0;
		
		try {
			mapper.insert(node);
			result = node.getId();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
	}
	
	public int updateNode(Node node){
		SqlSession session = getSqlMapper().openSession();
		NodeMapper mapper = session.getMapper(NodeMapper.class);
		int result = 0;
		
		try {
			result = mapper.updateByPrimaryKeySelective(node);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
	}
	
	public int deleteNode(Integer nodeId){
		SqlSession session = getSqlMapper().openSession();
		NodeMapper mapper = session.getMapper(NodeMapper.class);
		int result = 0;
		
		try {
			result = mapper.deleteByPrimaryKey(nodeId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
	}
	
	public int insertNodeProduct(NodeProduct nodeProduct){
		
		SqlSession session = getSqlMapper().openSession();
		NodeProductMapper mapper = session.getMapper(NodeProductMapper.class);
		int result = 0;
		
		try {
			result = mapper.insert(nodeProduct);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
		
	}
	
	public int updateNodeProduct(NodeProduct nodeProduct){
		
		SqlSession session = getSqlMapper().openSession();
		NodeProductMapper mapper = session.getMapper(NodeProductMapper.class);
		int result = 0;
		
		try {
			result = mapper.updateByPrimaryKeySelective(nodeProduct);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
		
	}
	
	public NodeProduct selectNodeProductByNodeandProduct(Integer nodeId, Integer productId){
		
		SqlSession session = getSqlMapper().openSession();
		NodeProductMapper mapper = session.getMapper(NodeProductMapper.class);
		NodeProduct nodeProduct = null;
		
		try {
			nodeProduct = mapper.selectByProductIdandNodeID(nodeId, productId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return nodeProduct;
		
	}

	public Node selectNodeByProductSourceId(Integer sourceId){

		SqlSession session = getSqlMapper().openSession();
		NodeMapper mapper = session.getMapper(NodeMapper.class);
		Node node = null;

		try {
			node = mapper.selectNodeForProductSource(sourceId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return node;

	}

	public NodeProduct selectStockControlCardTransactions(Integer nodeId, Integer productId){
		
		SqlSession session = getSqlMapper().openSession();
		NodeProductMapper mapper = session.getMapper(NodeProductMapper.class);
		NodeProduct nodeProduct = null;
		
		try {
			nodeProduct = mapper.selectByProductIdandNodeID(nodeId, productId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return nodeProduct;	
	}
	
	public List<Node> getNodesByType(String nodeType){
		SqlSession session = getSqlMapper().openSession();
		NodeMapper mapper = session.getMapper(NodeMapper.class);
		List<Node> nodes = null;
		
		try {
			nodes = mapper.selectByType(nodeType);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return nodes;	
	}

	public List<Node> getAllNodes() {
		SqlSession session = getSqlMapper().openSession();
		NodeMapper mapper = session.getMapper(NodeMapper.class);
		List<Node> nodes = null;
		
		try {
			nodes = mapper.getAllNodes();

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return nodes;
	}
	
	public Boolean saveNodeProgram(NodeProgram nodeProgram){
		SqlSession session = getSqlMapper().openSession();
		NodeProgramMapper mapper = session.getMapper(NodeProgramMapper.class);
		Boolean result = false;

		try {
			result = mapper.insert(nodeProgram) != 0;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		} finally {
			session.commit();
			session.close();
		}

		return result;
	}
	
	public List<Node> getNodesByProgram(Integer programId) {
		SqlSession session = getSqlMapper().openSession();
		NodeMapper mapper = session.getMapper(NodeMapper.class);
		List<Node> nodes = null;
		
		try {
			nodes = mapper.getNodesByProgram(programId);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return nodes;
	}
	
	public List<NodeProgram> selectNodeProgramsByNode(Integer nodeId) {
		SqlSession session = getSqlMapper().openSession();
		NodeProgramMapper mapper = session.getMapper(NodeProgramMapper.class);
		List<NodeProgram> nodePrograms = null;
		
		try {
			nodePrograms = mapper.selectNodeProgramsByNodeId(nodeId);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return nodePrograms;
	}
	
	public int deleteNodeProgram(Integer nodeProgramId){
		SqlSession session = getSqlMapper().openSession();
		NodeProgramMapper mapper = session.getMapper(NodeProgramMapper.class);
		int result = 0;
		
		try {
			result = mapper.deleteByPrimaryKey(nodeProgramId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
	}
	
	public Boolean updateNodePrograms(ArrayList<NodeProgram> nodePrograms){
		SqlSession session = getSqlMapper().openSession();
		NodeProgramMapper mapper = session.getMapper(NodeProgramMapper.class);
		Boolean result = false;
		
		try {
			for (NodeProgram nodeProgram : nodePrograms) {
				result = mapper.updateByPrimaryKeySelective(nodeProgram) != 0;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		} finally {
			session.commit();
			session.close();
		}
		

		return result;
	}
	

	public BigDecimal getPreviousStockOnHand(Integer ppId,Integer nodeId,Integer order , String precendence){
		SqlSession session = getSqlMapper().openSession();
		NodeMapper mapper = session.getMapper(NodeMapper.class);
		BigDecimal stockOnHand = null;
		
		try {
			if(precendence.equals(ELMISConstants.BEFORE)){
				stockOnHand = mapper.selectStockOnHandBeforeAction(ppId, nodeId, order);

			} else if (precendence.equals(ELMISConstants.AFTER)){
				stockOnHand = mapper.selectStockOnHandAfterAction(ppId, nodeId, order);
				
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}

		return stockOnHand;
	}
	
	public List<NodeType> getNodeTypes() {
		SqlSession session = getSqlMapper().openSession();
		NodeTypeMapper mapper = session.getMapper(NodeTypeMapper.class);
		List<NodeType> nodeTypes = null;
		
		try {
			nodeTypes = mapper.selectAll();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
	
	
		return nodeTypes;
	}
	
	public Node getNodeByName(String name) {
		SqlSession session = getSqlMapper().openSession();
		NodeMapper mapper = session.getMapper(NodeMapper.class);
		Node node = null;
		
		try {
			node = mapper.selectByName(name);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
	
	
		return node;
	}
	
	public Node getNodeById(Integer id) {
		SqlSession session = getSqlMapper().openSession();
		NodeMapper mapper = session.getMapper(NodeMapper.class);
		Node node = null;
		
		try {
			node = mapper.selectByPrimaryKey(id);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
	
	
		return node;
	}
	
	public String getFacilityCodeNodeById(Integer nodeId) {
		SqlSession session = getSqlMapper().openSession();
		FacilityNodeMapper mapper = session.getMapper(FacilityNodeMapper.class);
		String facilityCode = null;
		
		try {
			facilityCode = mapper.getFacilityCodeByNodeId(nodeId);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}	
		return facilityCode;
	}

	public Node getNodeByFacilityCode(String facilityCode) {
		SqlSession session = getSqlMapper().openSession();
		FacilityNodeMapper mapper = session.getMapper(FacilityNodeMapper.class);
		Node nodeFacility = null;
		
		try {
			nodeFacility = mapper.getNodeByFacilityCode(facilityCode);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}	
		return nodeFacility;
	}

	public Integer saveFacilityNode(FacilityNode facilityNode) {
		SqlSession session = getSqlMapper().openSession();
		FacilityNodeMapper facilityNodeMapper = session.getMapper(FacilityNodeMapper.class);
		Integer result = -1;

		try {
			result = facilityNodeMapper.insert(facilityNode);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
	}
}
