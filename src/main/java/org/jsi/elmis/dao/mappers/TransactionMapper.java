/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.dao.mappers;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jsi.elmis.model.Transaction;
import org.jsi.elmis.model.dto.TransactionHistoryDTO;

/**
 * @author Mesay S. Taye
 *
 */
public interface TransactionMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Transaction record);

    int insertSelective(Transaction record);

    Transaction selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Transaction record);

    int updateByPrimaryKey(Transaction record);
    
    Transaction selectConfirmatoryCounterpart(@Param("screeningId") Integer screeningId);

	List<Transaction> selectTransactionsForReversal(@Param("nodeId")Integer nodeId,
                                                    @Param("limit")Integer limit,
                                                    @Param("offset") Integer offset ,
                                                    @Param("scheduleCode") String scheduleCode,
                                                    @Param("fromTxnDate") Date fromTxnDate, @Param("toTxnDate") Date toTxnDate,
                                                    @Param("programCode") String programCode,
                                                    @Param("productCode") String productCode);

	String selectTransactionDescription(@Param("txnId")Integer txnId);
	
	int deleteAssocitedEntriesWithReversedTxn(@Param("txnId")Integer txnId);

	Integer selectTransactionsForReversalCount(@Param("nodeId")Integer nodeId, @Param("scheduleCode")String scheduleCode, @Param("fromTxnDate") Date fromTxnDate, @Param("toTxnDate") Date toTxnDate,
                                               @Param("programCode") String programCode, @Param("productCode") String productCode);
	
	BigDecimal aggregatedTxnQtyByType(@Param("ppId") Integer ppId,@Param("txnType") String txnType,
			@Param("startDate") Date startDate,@Param("endDate") Date endDate);
	
	BigDecimal selectAggregateAdjustmentType(@Param("ppId") Integer ppId,@Param("adjType") String adjType,
			@Param("startDate") Date startDate,@Param("endDate") Date endDate);
	
	BigDecimal selectAveragePeriodicConsumption(@Param("periodId") Integer periodId,@Param("ppId") Integer ppbId,
			@Param("scheduleCode")String scheduelCode ,@Param("no_of_periods") Integer noOfPeriods, @Param("currentPeriodConsumption") BigDecimal currentPeriodConsumption);

	public Integer getCountOfNonZeroConsumptionBefore(@Param("periodId") Integer periodId,@Param("ppId") Integer ppbId,@Param("scheduleCode")String scheduelCode, @Param("no_of_periods") Integer noOfPeriods);
	
	List<TransactionHistoryDTO> selectTransactionHistory(@Param("nodeId") Integer nodeId,@Param("ppbId") Integer ppbId,@Param("from")Date from);
	
	List<TransactionHistoryDTO> selectTransactionHistoryInRange(@Param("nodeId") Integer nodeId,@Param("ppbId") Integer ppbId,@Param("from")Date from , @Param("to")Date to);
	
	Integer selectMaxActionOrderOnADate(@Param("date") Date date);

	Integer selectMaxNonReversalActionOrderOnADate(@Param("date") Date date);

    Integer refreshView4RnR();
    
    Integer refreshActionTimeline();

	List<BigDecimal> selectOrderedTxnQtyList(@Param("nppId") Integer nppId, @Param("date")Date date);

    List<BigDecimal> selectOrderedTxnQtyBatchList(@Param("nppbId") Integer nppId, @Param("date")Date date);

	Integer selectActionOrderForTxn(@Param("txnId")Integer txnId);
}