/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.dao.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jsi.elmis.model.ProcessingPeriod;
import org.jsi.elmis.model.Requisition;

public interface RequisitionMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Requisition record);

    int insertSelective(Requisition record);

    Requisition selectByPrimaryKey(Integer id);

    List<Requisition> selectByCombo(@Param("facilityId") Integer facilityId,
                                    @Param("programId") Integer programId,
                                    @Param("periodId") Integer periodId,
                                    @Param("emergency") Boolean emergency);

    int updateByPrimaryKeySelective(Requisition record);

    int updateByPrimaryKey(Requisition record);

    List<Requisition> selectRequisitionsByPeriodId(Integer periodId);

    List<Requisition> selectRequisitionsByStatus(String status);

    List<Requisition> selectRequisitionsByProgramAndFacility(
            @Param("programId") Integer programId,
            @Param("facilityId") Integer facilityId,
             @Param("emergency") Boolean emergency
    );

    List<ProcessingPeriod> loadFacilityCommingRequisitionPeriods(@Param("programId") Integer programId,
                                                                 @Param("facilityId") Integer facilityId
    );

}