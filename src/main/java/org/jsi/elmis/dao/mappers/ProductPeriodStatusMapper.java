/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.dao.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jsi.elmis.model.ProductPeriodStatus;

public interface ProductPeriodStatusMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ProductPeriodStatus record);

    int insertSelective(ProductPeriodStatus record);

    ProductPeriodStatus selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ProductPeriodStatus record);

    int updateByPrimaryKey(ProductPeriodStatus record);
    
  	ProductPeriodStatus selectByProductAndPeriod(@Param("periodId")Integer periodId,
			@Param("productId")Integer productId);
  	
  	List<ProductPeriodStatus> selectPreviousConsumptions(@Param("productId")Integer productId , @Param("scheduleCode")String scheduleCode);
  	
  	//assumes that physical counts will be equal to stock-on-hands after 
  	//facilitiy-wide physical count is completed
  	int updateFacilityProgramProductPhysicalCounts(@Param("programCode") String programCode, @Param("periodId") Integer periodId);
}