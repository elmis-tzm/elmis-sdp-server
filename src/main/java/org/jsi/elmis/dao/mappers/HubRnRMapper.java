/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.dao.mappers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jsi.elmis.model.Facility;
import org.jsi.elmis.model.ProcessingPeriod;
import org.jsi.elmis.model.User;
import org.jsi.elmis.model.hub.rnr.HubRnR;

public interface HubRnRMapper {
	
	Integer insertRnR(HubRnR hubRnR);
	
	ArrayList<HubRnR> selectMontlyRnRsForPeriod(@Param("startDate")Date startDate , 
			@Param("endDate")Date endDate);
	
	ArrayList<HubRnR> selectAllRnRs(@Param("programCode")String programCode , 
			@Param("facilityCode") String facilityCode, @Param("type") String type);
	
	Integer updateRnR(HubRnR hubRnR);
	
	ArrayList<ProcessingPeriod> selectPeriodsToSubmitRnRFor();
	
	User selectUser(@Param("userName") String userName);
	
	ArrayList<User> selectAllUsers();
	
	Integer insertUser(User user);
	
	Integer updateUser(User user);
	
	Integer findScheduleIdByPeriodId(Integer periodId);
	
	Integer insertWeeklyProcessingPeriod(ProcessingPeriod weeklyProcessingPeriod);
	
	ArrayList<ProcessingPeriod> selectWeeklyPeriods();
	
	ProcessingPeriod selectPerocessingPeriod(@Param("id") Integer id);
	
	List<Facility> selectFacilities();

	ProcessingPeriod selectPerocessingPeriodByRange(@Param("startDate")Date startDate, @Param("endDate")Date endDate);

	ArrayList<ProcessingPeriod> selectAllPeriods();

	public List<HubRnR> selectMontlyRnRsForPeriodByPeriodId(@Param("periodId") Integer periodId);
	
	int updateRnRStatus(HubRnR rnr);
	
	List<HubRnR> selectRnRByStatus(@Param("status") String status);

	Integer deleteById(@Param("id")Integer id);
}

