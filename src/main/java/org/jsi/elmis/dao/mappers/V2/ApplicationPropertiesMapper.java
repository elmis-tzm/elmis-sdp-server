/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.dao.mappers.V2;

import org.apache.ibatis.annotations.*;
import org.jsi.elmis.Application;
import org.jsi.elmis.model.ApplicationProperty;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ApplicationPropertiesMapper {


    @Select("select *  from application_properties")
    @Results(
            @Result(column = "data_type", property = "dataType")
    )
    List<ApplicationProperty> getAllApplicationProperties();

    @Select("select * from application_properties where key = #{key}")
    @Results(
            @Result(column = "data_type", property = "dataType")
    )
    ApplicationProperty getApplicationPropertiesById(String key);

    @Delete("delete from application_properties where id = #{id}")
    void deleteApplicationProperties(String key);

    @Update("update application_properties set value= #{value} where key=#{key}")
    void updateApplicationProperties(ApplicationProperty obj);

    @Insert("insert into application_properties (key, value, data_type, groupname, displayorder, valueoptions, isconfigurable) values (#{key} , #{value} , #{data_type} , #{groupname} , #{displayorder} , #{valueoptions} , #{isconfigurable})")
    void insertApplicationProperties(ApplicationProperty obj);

    @Select("select * from application_properties where key = #{key}")
    @Results(
            @Result(column = "data_type", property = "dataType")
    )
    ApplicationProperty getByKey(String key);

}
