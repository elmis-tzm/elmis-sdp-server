/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.jsi.elmis.dao.mappers.DosageFrequencyMapper;
import org.jsi.elmis.dao.mappers.DosageUnitMapper;
import org.jsi.elmis.dao.mappers.ProgramMapper;
import org.jsi.elmis.dao.mappers.RegimenCombinationProductMapper;
import org.jsi.elmis.dao.mappers.RegimenLineMapper;
import org.jsi.elmis.dao.mappers.RegimenMapper;
import org.jsi.elmis.dao.mappers.RegimenProductCombinationMapper;
import org.jsi.elmis.dao.mappers.RegimenProductDosageMapper;
import org.jsi.elmis.model.DosageFrequency;
import org.jsi.elmis.model.DosageUnit;
import org.jsi.elmis.model.Program;
import org.jsi.elmis.model.Regimen;
import org.jsi.elmis.model.RegimenCombinationProduct;
import org.jsi.elmis.model.RegimenLine;
import org.jsi.elmis.model.RegimenProductCombination;
import org.jsi.elmis.model.RegimenProductDosage;
import org.jsi.elmis.model.dto.RegimenCombinationProductDTO;
import org.springframework.stereotype.Component;

/**
 * @author Mesay S. Taye
 *
 */
@Component
public class RegimenDAO extends DAO{
	
	public List<Program> getAllPrograms( ){
		
		SqlSession session = getSqlMapper().openSession();
		ProgramMapper mapper = session.getMapper(ProgramMapper.class);
		List<Program> programs = null;
		
		try {
			programs = mapper.selectAll();

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return programs;
	}
	
	public List<Regimen> getAllRegimens(){
		SqlSession session = getSqlMapper().openSession();
		RegimenMapper mapper = session.getMapper(RegimenMapper.class);
		List<Regimen> regimens = null;
		
		try {
			regimens = mapper.selectAll();

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return regimens;
	}
	
	public Regimen getRegimenByART(String art){

		SqlSession session = getSqlMapper().openSession();
		RegimenMapper mapper = session
				.getMapper(RegimenMapper.class);

		Regimen regimen = null;
		try {

			regimen = mapper.selectByARTNo(art);
			// System.out.println(prodList);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return regimen;
	}

	public Regimen getRegimenByCode(String regimenCode){

		SqlSession session = getSqlMapper().openSession();
		RegimenMapper mapper = session
				.getMapper(RegimenMapper.class);

		Regimen regimen = null;
		try {

			regimen = mapper.selectByCode(regimenCode);
			// System.out.println(prodList);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return regimen;
	}
	
	public List<DosageFrequency> getDosageFrequencies(){

		SqlSession session = getSqlMapper().openSession();
		DosageFrequencyMapper mapper = session
				.getMapper(DosageFrequencyMapper.class);

		List<DosageFrequency> dosageFrequencies = null;
		try {

			dosageFrequencies = mapper.selectAll();
			// System.out.println(prodList);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return dosageFrequencies;
	}
	
	public List<RegimenCombinationProductDTO> getRegimenComboProducts(String artNo, Integer nodeId , Integer comboId){

		SqlSession session = getSqlMapper().openSession();
		RegimenCombinationProductMapper mapper = session
				.getMapper(RegimenCombinationProductMapper.class);

		List<RegimenCombinationProductDTO> combos = null;
		try {

			combos = mapper.selectProductsByCombinationId(artNo, nodeId, comboId);
			// System.out.println(prodList);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return combos;
	}
	
	public List<DosageUnit> getDosageUnits(){

		SqlSession session = getSqlMapper().openSession();
		DosageUnitMapper mapper = session
				.getMapper(DosageUnitMapper.class);

		List<DosageUnit> dosageUnits = null;
		try {

			dosageUnits = mapper.selectAll();
			// System.out.println(prodList);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return dosageUnits;
	}
	
	public List<RegimenProductCombination> getRegimenProductCombos(Integer regimenId){

		SqlSession session = getSqlMapper().openSession();
		RegimenProductCombinationMapper mapper = session
				.getMapper(RegimenProductCombinationMapper.class);

		List<RegimenProductCombination> combos = null;
		try {

			combos = mapper.selectByRegimenId(regimenId);
			// System.out.println(prodList);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return combos;
	}

	public void upsertRegimenLines(ArrayList<RegimenLine> regimenLines) {
		SqlSession session = getSqlMapper().openSession();
		RegimenLineMapper mapper = session.getMapper(RegimenLineMapper.class);
		try {
			for (int i = 0; i < regimenLines.size(); i++) {
				System.out.println(regimenLines.get(i).getCode());
				mapper.upsertByCode(regimenLines.get(i));

			}
			session.commit();
		} finally {
			//session.close();
		}
		
	}
	
	public void upsertRegimens(ArrayList<Regimen> regimens) {
		SqlSession session = getSqlMapper().openSession();
		RegimenMapper mapper = session.getMapper(RegimenMapper.class);
		try {
			for (int i = 0; i < regimens.size(); i++) {
				System.out.println(regimens.get(i).getCode());
				mapper.upsertByCode(regimens.get(i));

			}
			session.commit();
		} finally {
			//session.close();
		}
		
	}
	
	public void upsertRegimenProductCombinations(
			ArrayList<RegimenProductCombination> regimenProductCombinations) {
		SqlSession session = getSqlMapper().openSession();
		RegimenProductCombinationMapper mapper = session.getMapper(RegimenProductCombinationMapper.class);
		try {
			for (int i = 0; i < regimenProductCombinations.size(); i++) {
				System.out.print(regimenProductCombinations.get(i));
				mapper.upsertByName(regimenProductCombinations.get(i));

			}
			session.commit();
		} finally {
			//session.close();
		}
		
	}
	
	public void upsertRegimenCombinationProducts(
			ArrayList<RegimenCombinationProduct> regimenCombinationProducts) {
		SqlSession session = getSqlMapper().openSession();
		RegimenCombinationProductMapper mapper = session.getMapper(RegimenCombinationProductMapper.class);
		try {
			for (int i = 0; i < regimenCombinationProducts.size(); i++) {
				System.out.println(regimenCombinationProducts.get(i).getId());
				mapper.upsertByComboAndProduct(regimenCombinationProducts.get(i));

			}
			session.commit();
		} finally {
			//session.close();
		}
		
	}
	

	public void upsertRegimenProductDosages(
			ArrayList<RegimenProductDosage> regimenProductDosages) {
		SqlSession session = getSqlMapper().openSession();
		RegimenProductDosageMapper mapper = session.getMapper(RegimenProductDosageMapper.class);
		try {
			for (int i = 0; i < regimenProductDosages.size(); i++) {
				System.out.println(regimenProductDosages.get(i).getId());
				mapper.upsertByCombinationProduct(regimenProductDosages.get(i));

			}
			session.commit();
		} finally {
			//session.close();
		}	
	}
	
	public void upsertDosageUnits(ArrayList<DosageUnit> dosageUnits) {
		SqlSession session = getSqlMapper().openSession();
		DosageUnitMapper mapper = session.getMapper(DosageUnitMapper.class);
		try {
			for (int i = 0; i < dosageUnits.size(); i++) {
				System.out.println(dosageUnits.get(i).getName());
				mapper.upsertByName(dosageUnits.get(i));

			}
			session.commit();
		} finally {
			//session.close();
		}
		
	}
	
	public void upsertDosageFrequencies(ArrayList<DosageFrequency> dosageFrequencies) {
		SqlSession session = getSqlMapper().openSession();
		DosageFrequencyMapper mapper = session.getMapper(DosageFrequencyMapper.class);
		try {
			for (int i = 0; i < dosageFrequencies.size(); i++) {
				System.out.println(dosageFrequencies.get(i).getName());
				mapper.upsertByName(dosageFrequencies.get(i));

			}
			session.commit();
		} finally {
			//session.close();
		}
		
	}



}
