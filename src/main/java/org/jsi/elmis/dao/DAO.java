/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.dao;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.Properties;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.log4j.Logger;
import org.jsi.elmis.common.util.Crypto;
import org.springframework.beans.factory.annotation.Value;

/**
 * @author Mesay S. Taye
 *
 */
public abstract class DAO {

    protected DAO(){
		setSystemProperties();
		setSqlMapper();
	}
	
	private SqlSessionFactory sqlMapper = null;
	
	
	protected SqlSessionFactory getSqlMapper(){
		return this.sqlMapper;
	}
	
	protected SqlSessionFactory setSqlMapper(){
		
		
		String resource = "SqlMapConfig.xml";
		Reader reader = null;

		try {
			reader = Resources.getResourceAsReader(resource);
			sqlMapper = new SqlSessionFactoryBuilder().build(reader,
					System.getProperties());
		} catch (IOException e) {
			e.printStackTrace();
		}
/*		sqlMapper.getConfiguration().setLazyLoadingEnabled(true);
		sqlMapper.getConfiguration().setAggressiveLazyLoading(false);*/
		return sqlMapper;
	}
	
	protected static void setSystemProperties(){
		Properties prop = new Properties(System.getProperties());
		FileInputStream fis = null;
		Reader reader = null;
		try {
            fis = new FileInputStream("db.properties");
            prop.load(fis);

			System.setProperties(prop);

			fis.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

		String dbpassword =  Crypto.decrypt(prop.getProperty("dbpassword"));
		System.setProperty("dbpassword", dbpassword);
	}


}
