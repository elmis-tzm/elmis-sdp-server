/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.dao;

import org.apache.ibatis.session.SqlSession;
import org.jsi.elmis.dao.mappers.*;
import org.jsi.elmis.model.*;
import org.jsi.elmis.model.dto.ARVActivityRegisterItem;
import org.jsi.elmis.model.dto.DispensationLineItem;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;


/**
 * @author Mesay S. Taye
 *
 */
@Component
public class ARVDispensingDAO extends DAO {
	
	public int saveDispensation(Dispensation dispensation){
		SqlSession session = getSqlMapper().openSession();
		ARVDispensationMapper mapper = session.getMapper(ARVDispensationMapper.class);

		try {
			mapper.insert(dispensation);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return dispensation.getId();
	}
	
	public int updateFacilityClientRegimen(FacilityClient facilityClient){
		SqlSession session = getSqlMapper().openSession();
		FacilityClientMapper mapper = session.getMapper(FacilityClientMapper.class);
		try {
			 mapper.updateByPrimaryKeySelective(facilityClient);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return facilityClient.getId();
	}
	
	public int saveDispensationItem(ARVDispensationLineItem arvDispensationItem){
		SqlSession session = getSqlMapper().openSession();
		ARVDispensationLineItemMapper mapper = session.getMapper(ARVDispensationLineItemMapper.class);
		try {
			 mapper.insert(arvDispensationItem);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return arvDispensationItem.getId();
	}
	
	public FacilityClient getARVClientByART(String artNo){
		SqlSession session = getSqlMapper().openSession();
		FacilityClientMapper mapper = session.getMapper(FacilityClientMapper.class);
		FacilityClient arvClient = null;
		try {
			arvClient = mapper.getFacilityClientByART(artNo);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return arvClient;
	}

	public FacilityClient getFacilityClientByUUID(String uuid){
		SqlSession session = getSqlMapper().openSession();
		FacilityClientMapper mapper = session.getMapper(FacilityClientMapper.class);
		FacilityClient arvClient = null;
		try {
			arvClient = mapper.getFacilityClientByUUID(uuid);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return arvClient;
	}

	public FacilityClient selectClientForTransacition(Integer txnId){
		SqlSession session = getSqlMapper().openSession();
		FacilityClientMapper mapper = session.getMapper(FacilityClientMapper.class);
		FacilityClient arvClient = null;
		try {
			 arvClient = mapper.selectClientForTransacition(txnId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return arvClient;
	}
	
	public FacilityClient getARVClientByNRC(String nrcNo){
		SqlSession session = getSqlMapper().openSession();
		FacilityClientMapper mapper = session.getMapper(FacilityClientMapper.class);
		FacilityClient arvClient = null;
		try {
			 arvClient = mapper.getFacilityClientByNRC(nrcNo);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return arvClient;
	}
	
	public List<FacilityClient>  getARVClientExpectedToday(){
		SqlSession session = getSqlMapper().openSession();
		FacilityClientMapper mapper = session.getMapper(FacilityClientMapper.class);
		List<FacilityClient> arvClients = null;
		try {
			 arvClients = mapper.selectClientsExpectedToday();

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return arvClients;
	}
	
	public Integer saveFacilityClient(FacilityClient facilityClient){
		SqlSession session = getSqlMapper().openSession();
		FacilityClientMapper mapper = session.getMapper(FacilityClientMapper.class);
		Integer clientId = null;
		
		if (facilityClient.getUuid() == null){
			facilityClient.setUuid(UUID.randomUUID().toString());
		}
		try {
			 clientId = mapper.insert(facilityClient);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return facilityClient.getId();
	}
	
	public Integer updateFacilityClient(FacilityClient facilityClient){
		SqlSession session = getSqlMapper().openSession();
		FacilityClientMapper mapper = session.getMapper(FacilityClientMapper.class);
		Integer clientId = null;
		try {
			 clientId = mapper.updateByPrimaryKeySelective(facilityClient);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return clientId;
	}
	
	public RegimenProductCombination getLastDispensedComboForClient(String artNo){
		SqlSession session = getSqlMapper().openSession();
		RegimenProductCombinationMapper mapper = session.getMapper(RegimenProductCombinationMapper.class);
		RegimenProductCombination comboId = null;
		try {
			 comboId = mapper.selectLastProductComboForClient(artNo);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return comboId;
	}
	
	
	
	public Dispensation selectPreviousDispensationForCombination(Integer comboId , String artNo){
		SqlSession session = getSqlMapper().openSession();
		ARVDispensationMapper mapper = session.getMapper(ARVDispensationMapper.class);
		Dispensation disp = null;
		try {
			disp = mapper.selectPreviousDispensationForCombination(artNo, comboId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return disp;
	}
	
	public List<FacilityClient> getARVClientByPropertyCombinations(Date from , Date to , String firstName , String lastName , String sex,
                                                                   String artNumber, String nrcNumber, String nupin, String searchId){
		SqlSession session = getSqlMapper().openSession();
		FacilityClientMapper mapper = session.getMapper(FacilityClientMapper.class);
		List<FacilityClient> arvClient = null;
		try {
			 arvClient = mapper.selectByPropertyCombinations(from, to,
                     firstName, lastName, sex, artNumber, nrcNumber, nupin, searchId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return arvClient;
	}

	public List<Regimen> getAllRegimens() {
		SqlSession session = getSqlMapper().openSession();
		RegimenMapper mapper = session.getMapper(RegimenMapper.class);
		List<Regimen> regimens = null;
		try {
			 regimens = mapper.selectAll();

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return regimens;
	}
	
	public List<Regimen> getAllRegimensByLineID(Integer lineId) {
		SqlSession session = getSqlMapper().openSession();
		RegimenMapper mapper = session.getMapper(RegimenMapper.class);
		List<Regimen> regimens = null;
		try {
			 regimens = mapper.selectRegimensByLineId(lineId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return regimens;
	}

	public List<RegimenLine> getAllRegimenLines() {
		SqlSession session = getSqlMapper().openSession();
		RegimenLineMapper mapper = session.getMapper(RegimenLineMapper.class);
		List<RegimenLine> regLines = null;
		try {
			 regLines = mapper.selectAll();

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return regLines;
	}
	
	public List<ARVActivityRegisterItem> getActivityRegisterLineItems(Integer nodeId , Date from , Date to) {
		SqlSession session = getSqlMapper().openSession();
		ARVDispensationMapper mapper = session.getMapper(ARVDispensationMapper.class);
		List<ARVActivityRegisterItem> activityRegisterLineItems = null;
		try {
			activityRegisterLineItems = mapper.selectActivityRegisterItems(nodeId , from, to);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return activityRegisterLineItems;
	}
	
	public List<DispensationLineItem> getAllActivityRegisterLineItems( Date from , Date to, Integer afterTxnId) {
		SqlSession session = getSqlMapper().openSession();
		ARVDispensationMapper mapper = session.getMapper(ARVDispensationMapper.class);
		List<DispensationLineItem> activityRegisterLineItems = null;
		try {
			activityRegisterLineItems = mapper.selectAllARVConsumption( from, to, afterTxnId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return activityRegisterLineItems;
	}

    public List<DispensationLineItem> getAllReversedActivityRegisterLineItems( Date from , Date to) {
        SqlSession session = getSqlMapper().openSession();
        ARVDispensationMapper mapper = session.getMapper(ARVDispensationMapper.class);
        List<DispensationLineItem> activityRegisterLineItems = null;
        try {
            activityRegisterLineItems = mapper.selectAllReversedARVConsumption( from, to);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.close();
        }
        return activityRegisterLineItems;
    }
	
	
	public Integer resetClientNextVisitDate(Integer clientId){
		SqlSession session = getSqlMapper().openSession();
		ARVDispensationMapper mapper = session.getMapper(ARVDispensationMapper.class);
		Integer result = null;
		try {
			result = mapper.resetClientNextVisitDate(clientId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
	}
	public List<Integer> getAllReversedARVTxnIds( Integer txnId) {
		SqlSession session = getSqlMapper().openSession();
		ARVDispensationMapper mapper = session.getMapper(ARVDispensationMapper.class);
		List<Integer> txnIds = null;
		try {
			txnIds = mapper.selectAllReversedARVTxnIds(txnId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return txnIds;
	}

	public List<FacilityClient> selectClientsExpectedByDate(Date date) {

		SqlSession session = getSqlMapper().openSession();
		FacilityClientMapper mapper = session.getMapper(FacilityClientMapper.class);
		List<FacilityClient> arvClients = null;
		try {
			 arvClients = mapper.selectClientsExpectedByDate(date);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return arvClients;
	}

	public Integer upsertARVClients(List<FacilityClient> arvClients) {
		SqlSession session = getSqlMapper().openSession();
		FacilityClientMapper mapper = session.getMapper(FacilityClientMapper.class);
		try {
			for (FacilityClient facilityClient : arvClients) {
				try{
					mapper.upsert(facilityClient);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		} finally {
			session.commit();
			session.close();
		}
		return 0;
	}

	public FacilityClient upsertARVClient(FacilityClient arvClient) {
		SqlSession session = getSqlMapper().openSession();
		FacilityClientMapper mapper = session.getMapper(FacilityClientMapper.class);
		try {

			try{
				mapper.upsert(arvClient);
			}catch(Exception e){
				System.out.println("Patient with ART_NUMBER = " + arvClient.getArtNumber() + " already exists!");
			}

		} finally {
			session.commit();
			session.close();
		}
		return arvClient;
	}

	public FacilityClient updateARVClientByARTNumber(FacilityClient arvClient) {
		SqlSession session = getSqlMapper().openSession();
		FacilityClientMapper mapper = session.getMapper(FacilityClientMapper.class);
		try {

			try{
				mapper.updateByARTNumber(arvClient);
			}catch(Exception e){
				e.printStackTrace();
			}

		} finally {
			session.commit();
			session.close();
		}
		return arvClient;
	}

    public Integer mergeDuplicates(FacilityClient arvClient) {
        SqlSession session = getSqlMapper().openSession();
        FacilityClientMapper mapper = session.getMapper(FacilityClientMapper.class);
        Integer result = null;
        try {

            try{
                result = mapper.mergeDuplicates(arvClient);
            }catch(Exception e){
                e.printStackTrace();
            }

        } finally {
            session.commit();
            session.close();
        }
        return result;
    }

	public Integer upsertFacilityClientVitals(FacilityClientVital facilityClientVital){
		SqlSession session = getSqlMapper().openSession();
		FacilityClientVitalMapper mapper = session.getMapper(FacilityClientVitalMapper.class);
		Integer result = -1;
		try {
			result = mapper.upsert(facilityClientVital);
		} finally {
			session.commit();
			session.close();
		}
		return result;
	}

	public Integer upsertFacilityClientCD4(FacilityClientCD4Count facilityClientCD4Count){
		SqlSession session = getSqlMapper().openSession();
		FacilityClientCD4CountMapper mapper = session.getMapper(FacilityClientCD4CountMapper.class);
		Integer result = -1;
		try {
			result = mapper.upsert(facilityClientCD4Count);
		} finally {
			session.commit();
			session.close();
		}
		return result;
	}

	public Integer upsertFacilityClientViralLoad(FacilityClientViralLoad facilityClientViralLoad){
		SqlSession session = getSqlMapper().openSession();
		FacilityClientViralLoadMapper mapper = session.getMapper(FacilityClientViralLoadMapper.class);
		Integer result = -1;
		try {
			result = mapper.upsert(facilityClientViralLoad);
		} finally {
			session.commit();
			session.close();
		}
		return result;
	}

	public FacilityClientViralLoad getFacilityClientViralLoad(Integer facilityClientId) {
		SqlSession session = getSqlMapper().openSession();
		FacilityClientViralLoadMapper mapper = session.getMapper(FacilityClientViralLoadMapper.class);
		FacilityClientViralLoad result;
		try {
			result = mapper.selectLatestViralLoadByFacilityClientId(facilityClientId);
		} finally {
			session.commit();
			session.close();
		}
		return result;
	}

	public FacilityClientCD4Count getFacilityClientCD4Count(Integer facilityClientId) {
		SqlSession session = getSqlMapper().openSession();
		FacilityClientCD4CountMapper mapper = session.getMapper(FacilityClientCD4CountMapper.class);
		FacilityClientCD4Count result;
		try {
			result = mapper.selectLatestCD4ByFacilityClientId(facilityClientId);
		} finally {
			session.commit();
			session.close();
		}
		return result;
	}

	public FacilityClientVital getFacilityClientVitals(Integer facilityClientId) {
		SqlSession session = getSqlMapper().openSession();
		FacilityClientVitalMapper mapper = session.getMapper(FacilityClientVitalMapper.class);
		FacilityClientVital result;
		try {
			result = mapper.selectLatestVitalsByFacilityClientId(facilityClientId);
		} finally {
			session.commit();
			session.close();
		}
		return result;
	}

}
