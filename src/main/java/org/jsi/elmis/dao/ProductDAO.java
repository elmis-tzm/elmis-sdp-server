/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.jsi.elmis.dao.mappers.*;
import org.jsi.elmis.model.*;
import org.springframework.stereotype.Component;


/**
 * @author Mesay S. Taye
 *
 */
@Component
public class ProductDAO extends DAO {
	
	public Product selectProductByCode(String code){
		
		SqlSession session = getSqlMapper().openSession();
		ProductMapper mapper = session.getMapper(ProductMapper.class);
		Product product = null;
		
		try {
			product = mapper.selectByCode(code);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return product;
		
	}
	
	public Product selectProductById(Integer id){
		
		SqlSession session = getSqlMapper().openSession();
		ProductMapper mapper = session.getMapper(ProductMapper.class);
		Product product = null;
		
		try {
			product = mapper.selectByPrimaryKey(id);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return product;
		
	}
	
	public List<FacilityApprovedProduct> getFacilityApprovedProducts(String facilityCode){
		
		SqlSession session = getSqlMapper().openSession();
		FacilityApprovedProductMapper mapper = session.getMapper(FacilityApprovedProductMapper.class);
		List<FacilityApprovedProduct>  facilityApprovedProducts = null;
		
		try {
			facilityApprovedProducts = mapper.selectAll(facilityCode);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return facilityApprovedProducts;
		
	}
/*	
	public List<FacilityApprovedProduct> getFacilityApprovedProducts(String programCode){
		
		SqlSession session = getSqlMapper().openSession();
		FacilityApprovedProductMapper mapper = session.getMapper(FacilityApprovedProductMapper.class);
		List<FacilityApprovedProduct>  facilityApprovedProducts = null;
		
		try {
			facilityApprovedProducts = mapper.selectByProgram(programCode);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return facilityApprovedProducts;
		
	}
	*/
	public List<ProgramProduct> getProgramProduct(){
		
		SqlSession session = getSqlMapper().openSession();
		ProgramProductMapper mapper = session.getMapper(ProgramProductMapper.class);
		List<ProgramProduct>  programProducts = null;
		
		try {
			programProducts = mapper.selectAll();

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return programProducts;
	}
	
	public List<ProgramProduct> getProductsInProgram(String programCode){
		
		SqlSession session = getSqlMapper().openSession();
		ProgramProductMapper mapper = session.getMapper(ProgramProductMapper.class);
		List<ProgramProduct>  programProducts = null;
		
		try {
			programProducts = mapper.selectProductsInAProgram(programCode);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return programProducts;
	}
	
	public List<ProgramProduct> getAllProductsInProgram(String programCode){
		
		SqlSession session = getSqlMapper().openSession();
		ProgramProductMapper mapper = session.getMapper(ProgramProductMapper.class);
		List<ProgramProduct>  programProducts = null;
		
		try {
			programProducts = mapper.selectAllProductsInAProgram(programCode);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return programProducts;
	}
	
	public List<ProgramProduct> getFacilityApprovedProductsInProgram(String programCode,String facilityCode){
		
		SqlSession session = getSqlMapper().openSession();
		ProgramProductMapper mapper = session.getMapper(ProgramProductMapper.class);
		List<ProgramProduct>  programProducts = null;
		
		try {
			programProducts = mapper.selectFacilityApprovedProductsInAProgram(programCode,facilityCode);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return programProducts;
	}
	
	public List<FacilityApprovedProduct> getFacilityApprovedProducts(String programCode,String facilityCode){
		
		SqlSession session = getSqlMapper().openSession();
		FacilityApprovedProductMapper mapper = session.getMapper(FacilityApprovedProductMapper.class);
		List<FacilityApprovedProduct>  products = null;
		
		try {
			products = mapper.selectFacilityApprovedProductsInAProgram(programCode,facilityCode);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return products;
	}
	
	
	public List<Program> getProductPrograms(Integer productId){
		
		SqlSession session = getSqlMapper().openSession();
		ProgramMapper mapper = session.getMapper(ProgramMapper.class);
		List<Program>  programs = null;
		
		try {
			programs = mapper.selectProductPrograms(productId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return programs;
	}
	
	public void upsertProductCategories(ArrayList<ProductCategory> categories){
		SqlSession session = getSqlMapper().openSession();
		ProductCategoryMapper mapper = session
				.getMapper(ProductCategoryMapper.class);
		try {
			for (int i = 0; i < categories.size(); i++) {
				System.out.println(categories.get(i).getCode());
				// mapper.updateByPrimaryKey(products.get(i));
				mapper.upsertByCode(categories.get(i));

			}
			session.commit();
		} finally {
			//session.close();
		}
		
	}

	public void upsertProducts(ArrayList<Product> products) {
		SqlSession session = getSqlMapper().openSession();
		ProductMapper mapper = session
				.getMapper(ProductMapper.class);
		try {
			for (int i = 0; i < products.size(); i++) {
				System.out.println(products.get(i).getCode());
				// mapper.updateByPrimaryKey(products.get(i));
				mapper.upsertByCode(products.get(i));

			}
			session.commit();
		} finally {
			//session.close();
		}	
		
	}
	
	public List<ProductCategory> getProductCategories(){
		
		SqlSession session = getSqlMapper().openSession();
		ProductCategoryMapper mapper = session.getMapper(ProductCategoryMapper.class);
		List<ProductCategory>  productCategories = null;
		
		try {
			productCategories = mapper.selectAll();

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return productCategories;
	}
	
	public int  update(Product product){
		int result = -1;
		SqlSession session = getSqlMapper().openSession();
		ProductMapper mapper = session.getMapper(ProductMapper.class);
		
		try {
			result = mapper.updateByPrimaryKeySelective(product);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		
		return result;
		
	}

	public Integer updateProducts(List<Product> products) {
		SqlSession session = getSqlMapper().openSession();
		ProductMapper mapper = session.getMapper(ProductMapper.class);
		
		try {
			for(Product product : products)
			{
				mapper.updateByPrimaryKeySelective(product);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return 0;
	}

	public List<ProductSubstitute> findProductSubstitutes(Integer productId){
		SqlSession session = getSqlMapper().openSession();
		ProductSubstituteMapper mapper = session.getMapper(ProductSubstituteMapper.class);
		List<ProductSubstitute> substitutes = null;

		try {
			substitutes = mapper.selectByProductId(productId);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return substitutes;
	}



	public List<ProductBatch> getProductBatches(String productCode) {
		SqlSession session = getSqlMapper().openSession();
		ProductBatchMapper mapper = session.getMapper(ProductBatchMapper.class);
		List<ProductBatch> productBatches = null;

		try {
			productBatches = mapper.selectByProductCode(productCode);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return  productBatches;
	}

	}
