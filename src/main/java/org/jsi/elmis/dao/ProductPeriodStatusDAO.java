/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.dao;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.jsi.elmis.dao.mappers.ProductPeriodStatusAdjustmentMapper;
import org.jsi.elmis.dao.mappers.ProductPeriodStatusMapper;
import org.jsi.elmis.dao.mappers.ProductPeriodStockedOutPeriodMapper;
import org.jsi.elmis.model.ProductPeriodStatus;
import org.jsi.elmis.model.ProductPeriodStatusAdjustment;
import org.jsi.elmis.model.ProductPeriodStockedOutPeriod;
import org.springframework.stereotype.Component;

/**
 * @author Mesay S. Taye
 *
 */
@Component
public class ProductPeriodStatusDAO extends DAO {
	
	public ProductPeriodStatus getProductPeriodStatus(Integer periodId, Integer productId){
		
		SqlSession session = getSqlMapper().openSession();
		ProductPeriodStatusMapper mapper = session.getMapper(ProductPeriodStatusMapper.class);
		ProductPeriodStatus  productPeriodStatus = null;
		
		try {
			productPeriodStatus = mapper.selectByProductAndPeriod(periodId, productId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return productPeriodStatus;
	}
	
	public List<ProductPeriodStatus> getPreviousConsumptions(Integer productId, String scheduleCode){
		
		SqlSession session = getSqlMapper().openSession();
		ProductPeriodStatusMapper mapper = session.getMapper(ProductPeriodStatusMapper.class);
		List<ProductPeriodStatus>  productPeriodStatuses = null;
		
		try {
			productPeriodStatuses = mapper.selectPreviousConsumptions(productId , scheduleCode);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return productPeriodStatuses;
	}
	
	public Integer insertProductPeriodStatus(ProductPeriodStatus  productPeriodStatus){
		
		SqlSession session = getSqlMapper().openSession();
		ProductPeriodStatusMapper mapper = session.getMapper(ProductPeriodStatusMapper.class);
		
		try {
			mapper.insert(productPeriodStatus);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return productPeriodStatus.getId();
	}
	
	public Integer updateProductPeriodStatus(ProductPeriodStatus  productPeriodStatus){
		
		SqlSession session = getSqlMapper().openSession();
		ProductPeriodStatusMapper mapper = session.getMapper(ProductPeriodStatusMapper.class);
		
		try {
			mapper.updateByPrimaryKeySelective(productPeriodStatus);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return productPeriodStatus.getId();
	}
	
	public Integer insertProductPeriodStatusAdjustment(ProductPeriodStatusAdjustment periodAdjustment){
		
		SqlSession session = getSqlMapper().openSession();
		ProductPeriodStatusAdjustmentMapper mapper = session.getMapper(ProductPeriodStatusAdjustmentMapper.class);
		
		try {
			mapper.insert(periodAdjustment);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return periodAdjustment.getId();
	}
	
	public Integer updateProductPeriodStatusAdjustment(ProductPeriodStatusAdjustment periodAdjustment){
		
		SqlSession session = getSqlMapper().openSession();
		ProductPeriodStatusAdjustmentMapper mapper = session.getMapper(ProductPeriodStatusAdjustmentMapper.class);
		
		try {
			mapper.updateByPrimaryKeySelective(periodAdjustment);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return periodAdjustment.getId();
	}
	
public Integer updateProductPeriodStatusPhysicalCounts(String programCode , Integer periodId){
		
		SqlSession session = getSqlMapper().openSession();
		ProductPeriodStatusMapper mapper = session.getMapper(ProductPeriodStatusMapper.class);
		Integer result = 0;
		try {
			result = mapper.updateFacilityProgramProductPhysicalCounts(programCode, periodId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
	}
	
	public ProductPeriodStatusAdjustment getProductAdjustmentForPeriod(Integer prodPeriodStatusId , String adjustmentType){
		
		SqlSession session = getSqlMapper().openSession();
		ProductPeriodStatusAdjustmentMapper mapper = session.getMapper(ProductPeriodStatusAdjustmentMapper.class);
		ProductPeriodStatusAdjustment periodAdjustment = null;
		try {
			periodAdjustment = mapper.selectByPeriodStatusId(prodPeriodStatusId, adjustmentType);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return periodAdjustment;
	}
	
	public List<ProductPeriodStatusAdjustment> getProductAdjustmentsForPeriod(Integer prodPeriodStatusId){
		
		SqlSession session = getSqlMapper().openSession();
		ProductPeriodStatusAdjustmentMapper mapper = session.getMapper(ProductPeriodStatusAdjustmentMapper.class);
		List<ProductPeriodStatusAdjustment> periodAdjustments = null;
		try {
			periodAdjustments = mapper.selectAdjustmentsByPeriodStatusId(prodPeriodStatusId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return periodAdjustments;
	}
	
	public List<ProductPeriodStatusAdjustment> getFacilityStockAdjustments(Integer ppbId, Date startDate , Date endDate){
		
		SqlSession session = getSqlMapper().openSession();
		ProductPeriodStatusAdjustmentMapper mapper = session.getMapper(ProductPeriodStatusAdjustmentMapper.class);
		List<ProductPeriodStatusAdjustment> periodAdjustments = null;
		try {
			periodAdjustments = mapper.selectFacilityStockAdjustments(startDate, endDate, ppbId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return periodAdjustments;
	}
	
	public Integer insertProductPeriodStockOut(ProductPeriodStockedOutPeriod stockOutPeriod ){
		
		SqlSession session = getSqlMapper().openSession();
		ProductPeriodStockedOutPeriodMapper mapper = session.getMapper(ProductPeriodStockedOutPeriodMapper.class);
		Integer result = 0;
		try {
			result = mapper.insert(stockOutPeriod);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
	}

	public Integer updateByProductPeriodStockOutByID(ProductPeriodStockedOutPeriod stockOutPeriod ){
	
		SqlSession session = getSqlMapper().openSession();
		ProductPeriodStockedOutPeriodMapper mapper = session.getMapper(ProductPeriodStockedOutPeriodMapper.class);
		Integer result = 0;
		try {
			result = mapper.updateByPrimaryKeySelective(stockOutPeriod);
		} catch (Exception ex) {
		ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
	}
}
