package org.jsi.elmis.dao;/*
 * This program was produced for the U.S. Agency for International Development. It was prepared by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed under MPL v2 or later.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the Mozilla Public License as published by the Mozilla Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 *
 * You should have received a copy of the Mozilla Public License along with this program. If not, see http://www.mozilla.org/MPL/
 */

import org.apache.ibatis.session.SqlSession;
import org.jsi.elmis.dao.mappers.StockRequestApprovalItemMapper;
import org.jsi.elmis.dao.mappers.StockRequisitionApprovalStatusMapper;
import org.jsi.elmis.model.StockRequestApprovalItem;
import org.jsi.elmis.model.StockRequisitionApprovalStatus;
import org.jsi.elmis.model.dto.StockRequistionDTO;
import org.jsi.elmis.model.dto.transaction.details.StockRequisitionItemDetails;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class StockRequisitionApprovalStatusDAO extends  DAO {

    public Integer insert(StockRequisitionApprovalStatus requisitionApprovalStatus){
        SqlSession session = getSqlMapper().openSession();
        StockRequisitionApprovalStatusMapper mapper = session.getMapper(StockRequisitionApprovalStatusMapper.class);
        int result = 0;

        try {
            mapper.insert(requisitionApprovalStatus);
            result = requisitionApprovalStatus.getId();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }
    public List<StockRequistionDTO> loadRequestsForUserApproval(Integer user){
        SqlSession session = getSqlMapper().openSession();
        StockRequisitionApprovalStatusMapper mapper = session.getMapper(StockRequisitionApprovalStatusMapper.class);
        List<StockRequistionDTO> result = null;

        try {
            result=  mapper.loadRequestsForUserApproval(user);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }
    public List<StockRequistionDTO> loadApprovedRequistionList(Integer user){
        SqlSession session = getSqlMapper().openSession();
        StockRequisitionApprovalStatusMapper mapper = session.getMapper(StockRequisitionApprovalStatusMapper.class);
        List<StockRequistionDTO> result = null;

        try {
            result=  mapper.loadApprovedRequistionList(user);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }

    public List<StockRequisitionItemDetails> loadStockRequisitionItemList(Integer requisition) {
        SqlSession session = getSqlMapper().openSession();
        StockRequisitionApprovalStatusMapper mapper = session.getMapper(StockRequisitionApprovalStatusMapper.class);
        List<StockRequisitionItemDetails> result = null;

        try {
            result=  mapper.loadRequestedProducts(requisition);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }

    public Integer updateRequistionStatustoApproved() {
        SqlSession session = getSqlMapper().openSession();
        StockRequisitionApprovalStatusMapper mapper = session.getMapper(StockRequisitionApprovalStatusMapper.class);
        int result = 0;

        try {
            mapper.updateRequistionStatustoApproved();

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }
    
    public Integer saveApprovalLineItems(StockRequisitionApprovalStatus approval) {
        SqlSession session = getSqlMapper().openSession();
        StockRequestApprovalItemMapper mapper = session.getMapper(StockRequestApprovalItemMapper.class);
        int result = 0;

        try {
            for (StockRequestApprovalItem stockRequestApprovalItem : approval.getApprovalItems()) {
            	stockRequestApprovalItem.setApprovalId(approval.getId());
				mapper.insert(stockRequestApprovalItem);
			}

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }
}
