/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.dao;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.jsi.elmis.dao.mappers.NodeProductMapper;
import org.jsi.elmis.dao.mappers.PhysicalCountAdjustmentMapper;
import org.jsi.elmis.dao.mappers.PhysicalCountMapper;
import org.jsi.elmis.model.NodeProduct;
import org.jsi.elmis.model.PhysicalCount;
import org.jsi.elmis.model.PhysicalCountAdjustment;
import org.springframework.stereotype.Component;


/**
 * @author Mesay S. Taye
 *
 */
@Component
public class PhysicalCountDAO extends DAO {

	public int savePhysicalCount(PhysicalCount pc){
		SqlSession session = getSqlMapper().openSession();
		PhysicalCountMapper mapper = session.getMapper(PhysicalCountMapper.class);
		
		try {
			mapper.insert(pc);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return pc.getId();
	}
	
	public int deletePhysicalCountsAfterDate(Date date,Integer  nodeId,Integer  productId){
		SqlSession session = getSqlMapper().openSession();
		PhysicalCountMapper mapper = session.getMapper(PhysicalCountMapper.class);
		int result = 0;
		try{
			result  = mapper.deletePhysicalCountsAfterDate(date, nodeId, productId);
		} catch (Exception ex){
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
	}
	public int deletePhysicalCountsAfterTimeStamp(Date date,Integer  nodeId,Integer  programProductId){
		SqlSession session = getSqlMapper().openSession();
		PhysicalCountMapper mapper = session.getMapper(PhysicalCountMapper.class);
		int result = 0;
		try{
			result  = mapper.deletePhysicalCountsAfterDate(date, nodeId, programProductId);
		} catch (Exception ex){
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
	}
	
	
	public int insertNodeProduct(NodeProduct nodeProduct){
		
		SqlSession session = getSqlMapper().openSession();
		NodeProductMapper mapper = session.getMapper(NodeProductMapper.class);
		int result = 0;
		
		try {
			result = mapper.insert(nodeProduct);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
		
	}
	
	public int updateNodeProduct(NodeProduct nodeProduct){
		
		SqlSession session = getSqlMapper().openSession();
		NodeProductMapper mapper = session.getMapper(NodeProductMapper.class);
		int result = 0;
		
		try {
			result = mapper.updateByPrimaryKeySelective(nodeProduct);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
		
	}
	
	public NodeProduct selectNodeProductByNodeandProduct(Integer nodeId, Integer productId){
		
		SqlSession session = getSqlMapper().openSession();
		NodeProductMapper mapper = session.getMapper(NodeProductMapper.class);
		NodeProduct nodeProduct = null;
		
		try {
			nodeProduct = mapper.selectByProductIdandNodeID(nodeId, productId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return nodeProduct;
		
	}

	public Boolean deletePhysicalCount(Integer physicalCountId) {
		SqlSession session = getSqlMapper().openSession();
		PhysicalCountMapper mapper = session.getMapper(PhysicalCountMapper.class);
		Boolean result = false;
		
		try {
			result = mapper.deleteByPrimaryKey(physicalCountId) != 0;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		} finally {
			session.commit();
			session.close();
		}
		

		return result;
	}
	
	public List<PhysicalCountAdjustment> getPhysicalCountAdjustments(
			Integer physicalCountId) {
		SqlSession session = getSqlMapper().openSession();
		PhysicalCountAdjustmentMapper mapper = session.getMapper(PhysicalCountAdjustmentMapper.class);
		List<PhysicalCountAdjustment> result = null;
		
		try {
			result = mapper.selectAllByPhysicalCountId(physicalCountId);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		

		return result;
	}

	public Boolean deletePhysicalCountAdjustment(
			Integer id) {
		SqlSession session = getSqlMapper().openSession();
		PhysicalCountAdjustmentMapper mapper = session.getMapper(PhysicalCountAdjustmentMapper.class);
		Boolean result = false;
		
		try {
			result = mapper.deleteByPrimaryKey(id) != 0;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		} finally {
			session.commit();
			session.close();
		}
		

		return result;
	}
	
	public Boolean savePhysicalCountAdjustment(
			PhysicalCountAdjustment physicalCountAdjustment) {
		SqlSession session = getSqlMapper().openSession();
		PhysicalCountAdjustmentMapper mapper = session.getMapper(PhysicalCountAdjustmentMapper.class);
		Boolean result = false;
		
		try {
			result = mapper.insert(physicalCountAdjustment) != 0;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		} finally {
			session.commit();
			session.close();
		}
		

		return result;
	}
	

}
