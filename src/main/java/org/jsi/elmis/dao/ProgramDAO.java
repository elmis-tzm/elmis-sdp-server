/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.jsi.elmis.dao.mappers.ProgramMapper;
import org.jsi.elmis.model.Program;
import org.springframework.stereotype.Component;


/**
 * @author Mesay S. Taye
 *
 */
@Component
public class ProgramDAO extends DAO {

	public void upsertPrograms(ArrayList<Program> programs) {
		SqlSession session = getSqlMapper().openSession();
		ProgramMapper mapper = session.getMapper(ProgramMapper.class);
		try {
			for (int i = 0; i < programs.size(); i++) {
				System.out.println(programs.get(i).getCode());
				mapper.upsertByCode(programs.get(i));

			}
			session.commit();
		} finally {
			//session.close();
		}
	}
	
	public Program selectByCode(String code) {
		SqlSession session = getSqlMapper().openSession();
		ProgramMapper mapper = session.getMapper(ProgramMapper.class);
		Program program = null;
		try {
			program = mapper.selectByCode(code);
		} finally {
			//session.close();
		}
		return program;
	}

    public List<Program> selectAll() {
        SqlSession session = getSqlMapper().openSession();
        ProgramMapper mapper = session.getMapper(ProgramMapper.class);
        List<Program> programs = null;
        try {
            programs = mapper.selectAll();
        } finally {
            session.commit();
            session.close();
        }
        return programs;
    }
    //TODO: workaround to get programs which the facility is managing
    public List<Program> selectFacilityPrograms() {
        SqlSession session = getSqlMapper().openSession();
        ProgramMapper mapper = session.getMapper(ProgramMapper.class);
        List<Program> programs = null;
        try {
            programs = mapper.selectFacilityPrograms();
        } finally {
            session.commit();
            session.close();
        }
        return programs;
    }
}
