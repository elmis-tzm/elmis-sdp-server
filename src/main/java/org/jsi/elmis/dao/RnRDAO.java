/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.jsi.elmis.common.constants.ELMISConstants;
import org.jsi.elmis.dao.mappers.*;
import org.jsi.elmis.model.*;
import org.springframework.stereotype.Component;


/**
 * @author Mesay S. Taye
 *
 */
@Component
public class RnRDAO extends DAO {

	public int saveRnR(ReportAndRequisition rnr){
		SqlSession session = getSqlMapper().openSession();
		ReportAndRequisitionMapper mapper = session.getMapper(ReportAndRequisitionMapper.class);
		
		try {
			 mapper.insert(rnr);

		} catch (Exception ex) {
			ex.printStackTrace(); 
		} finally {
			session.commit();
			session.close();
		}
		return (rnr != null)?rnr.getId():0;
	}
	
	public List<Requisition> selectRequisitionsByPeriodId(Integer periodId){
		List<Requisition> requisitions = null;
		SqlSession session = getSqlMapper().openSession();
		RequisitionMapper mapper = session.getMapper(RequisitionMapper.class);
		try {
			requisitions = mapper.selectRequisitionsByPeriodId(periodId);			 
		} catch (Exception ex) {
			ex.printStackTrace(); 
		} finally {
			session.commit();
			session.close();
		}
		return requisitions;
	}
	
	public List<Requisition> selectRequisitionsByStatus(String status){
		List<Requisition> requisitions = null;
		SqlSession session = getSqlMapper().openSession();
		RequisitionMapper mapper = session.getMapper(RequisitionMapper.class);
		try {
			requisitions = mapper.selectRequisitionsByStatus(status);			 
		} catch (Exception ex) {
			ex.printStackTrace(); 
		} finally {
			session.commit();
			session.close();
		}
		return requisitions;
	}
	
	public int saveRnR(Requisition rqn){
		SqlSession session = getSqlMapper().openSession();
		RequisitionMapper mapper = session.getMapper(RequisitionMapper.class);
		RequisitionLineItemMapper rliMapper = session.getMapper(RequisitionLineItemMapper.class);
		RequisitionLineItemLossesAndAdjustmentMapper rliLnAMapper = session.getMapper(RequisitionLineItemLossesAndAdjustmentMapper.class);
		
		try {
			if(rqn.getStatus()==null){
				rqn.setStatus("SUBMITTED");
				rqn.setRequisitionType("REGULAR_REQUISITION");
			}
			 mapper.insert(rqn);
			 
			 for (RequisitionLineItem rlItem : rqn.getRequisitionLineItems()) {
				rlItem.setRequisitionId(rqn.getId());
				if(rlItem.getReasonForRequestedQuantity() == null){
                    rlItem.setReasonForRequestedQuantity("SDP Submission");
                }
				rliMapper.insert(rlItem);
				
				for (RequisitionLineItemLossesAndAdjustment rliLnA : rlItem.getRliLossesNAdjustments()) {
					rliLnA.setReqLineItemId(rlItem.getId());
					rliLnAMapper.insert(rliLnA);
				}
			}

		} catch (Exception ex) {
			ex.printStackTrace(); 
		} finally {
			session.commit();
			session.close();
		}
		return (rqn != null)?rqn.getId():0;
	}
	
	public int updateRnR(Requisition rqn){
		SqlSession session = getSqlMapper().openSession();
		RequisitionMapper mapper = session.getMapper(RequisitionMapper.class);
		RequisitionLineItemMapper rliMapper = session.getMapper(RequisitionLineItemMapper.class);
		RequisitionLineItemLossesAndAdjustmentMapper rliLnAMapper = session.getMapper(RequisitionLineItemLossesAndAdjustmentMapper.class);
		
		try {
			 mapper.updateByPrimaryKey(rqn);
			 
			 for (RequisitionLineItem rlItem : rqn.getRequisitionLineItems()) {
                 if(rlItem.getReasonForRequestedQuantity() == null || rlItem.getReasonForRequestedQuantity().isEmpty()){
                     rlItem.setReasonForRequestedQuantity("SDP Submission");
                 }
				rliMapper.updateByPrimaryKey(rlItem);

				for (RequisitionLineItemLossesAndAdjustment rliLnA : rlItem.getRliLossesNAdjustments()) {
                    rliLnA.setReqLineItemId(rlItem.getId());
					if(rliLnA.getId() == null){
						rliLnA.setReqLineItemId(rlItem.getId());
						rliLnAMapper.insert(rliLnA);
					}else{
						rliLnAMapper.updateByPrimaryKey(rliLnA);
					}					
					
				}
			}

		} catch (Exception ex) {
			ex.printStackTrace(); 
		} finally {
			session.commit();
			session.close();
		}
		return (rqn != null)?rqn.getId():0;
	}
	
	public int delete(Integer requisitionId){
		SqlSession session = getSqlMapper().openSession();
		RequisitionMapper mapper = session.getMapper(RequisitionMapper.class);
		Integer result;
		try {
			result = mapper.deleteByPrimaryKey(requisitionId);
		} catch (Exception ex) {
			ex.printStackTrace();
			result = null;
		} finally {
			session.commit();
			session.close();
		}
		return result;
	}

	public Integer saveOfflineRnRStatus(OfflineRnRStatus offlineRnRStatus){
		SqlSession session = getSqlMapper().openSession();
		OfflineRnRStatusMapper mapper = session.getMapper(OfflineRnRStatusMapper.class);
		Integer result = -1;
		try {
			result = mapper.upsert(offlineRnRStatus);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
	}

	public OfflineRnRStatus getOfflineRnRStatusByRequisitionId(Integer requisitionId){
		SqlSession session = getSqlMapper().openSession();
		OfflineRnRStatusMapper mapper = session.getMapper(OfflineRnRStatusMapper.class);
		OfflineRnRStatus result = null;
		try {
			result = mapper.selectByRequisitionId(requisitionId);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
	}

    public List<Requisition> getRequisitionsByCombo(Integer facilityId, Integer programId, Integer periodId, Boolean emergency){
        SqlSession session = getSqlMapper().openSession();
        RequisitionMapper mapper = session.getMapper(RequisitionMapper.class);
        List<Requisition> requisitions = null;
        try {
            requisitions = mapper.selectByCombo(facilityId, programId, periodId, emergency);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return requisitions;
    }
	public List<Requisition> selectRequisitionsByFacilityProgram(Integer program,Integer facilityId,Boolean emergency){
		List<Requisition> requisitions = null;
		SqlSession session = getSqlMapper().openSession();
		RequisitionMapper mapper = session.getMapper(RequisitionMapper.class);
		try {
			requisitions = mapper.selectRequisitionsByProgramAndFacility(program,facilityId,emergency);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return requisitions;
	}
	public List<ProcessingPeriod> loadFacilityProgramPeriods(Integer program,Integer facilityId){
		List<ProcessingPeriod> processingPeriods = null;
		SqlSession session = getSqlMapper().openSession();
		RequisitionMapper mapper = session.getMapper(RequisitionMapper.class);
		try {
			processingPeriods = mapper.loadFacilityCommingRequisitionPeriods(program,facilityId);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return processingPeriods;
	}
}
