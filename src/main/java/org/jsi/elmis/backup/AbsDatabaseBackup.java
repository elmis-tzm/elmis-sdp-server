package org.jsi.elmis.backup;/*
 * This program was produced for the U.S. Agency for International Development. It was prepared by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed under MPL v2 or later.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the Mozilla Public License as published by the Mozilla Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 *
 * You should have received a copy of the Mozilla Public License along with this program. If not, see http://www.mozilla.org/MPL/
 */

import org.jsi.elmis.dao.DAO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;

import java.io.*;
public abstract class AbsDatabaseBackup extends DAO implements IDatabaseBackup {
    protected static final int BUFFER = 10485760;
    @Value("${dbuser}")
    protected String user;
    @Value("${dbport}")
    protected String port;
    @Value("${dbhost}")
    protected String host;
    @Value("${dbpassword}")
    protected String password;
    @Value("${dbname}")
    protected String db;
    @Value("${database.backup.database.home.location}")
    protected String POSTGRES_HOME;
    @Value("${database.backup.home}")
    protected String BACKUP_HOME;
    @Value("${database.backup.database.backupName}")
    protected String backupName;

    @Override
    public InputStream readDumpFile(String fileName) throws FileNotFoundException {
        File dumpFile = new File(fileName);
        InputStream inputStream = new FileInputStream(dumpFile);
        return inputStream;
    }

    @Override
    public InputStream backup() throws FileNotFoundException {
        Runtime r = Runtime.getRuntime();
        Process p = null;
        ProcessBuilder pb;
        r = Runtime.getRuntime();
        pb = this.constructBackupCommand();
        pb.redirectErrorStream(true);
        try {
            p = pb.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
        InputStream is = this.readDumpFile(this.BACKUP_HOME + "java31_rawDatabase.backup");
        return is;
    }

    @Override
    public String getDataBaseBackup() throws Exception {

        String executedCommand = "";

        executedCommand = this.constructSqlStringBackupCommand();
        Process run = Runtime.getRuntime().exec(executedCommand);
        InputStream in = this.readDumpFile(this.BACKUP_HOME + backupName + ".sql ");
        InputStreamReader isr = new InputStreamReader(in);
        BufferedReader br = new BufferedReader(isr);
        StringBuffer content = new StringBuffer();

        int count = 0;
        String line = "";
        char[] cbuf = new char[BUFFER];

        while (br.read(cbuf, 0, BUFFER) != -1) {

            content.append(cbuf,count,BUFFER);
            count += BUFFER;
        }
        br.close();
        in.close();
        return content.toString();

    }
}
