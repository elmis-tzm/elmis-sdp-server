/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.service.interfaces;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.jsi.elmis.common.constants.Constant;
import org.jsi.elmis.exceptions.InsufficientNodeProductException;
import org.jsi.elmis.exceptions.UnavailableHIVTestProductException;
import org.jsi.elmis.exceptions.UnavailableNodeProductException;
import org.jsi.elmis.exceptions.UnavailablePhysicalCountException;
import org.jsi.elmis.model.ActionOrder;
import org.jsi.elmis.model.HIVTestProduct;
import org.jsi.elmis.model.Node;
import org.jsi.elmis.model.dto.HIVProductAvailabilityDTO;
import org.jsi.elmis.model.dto.HIVTestDARItem;
import org.jsi.elmis.model.dto.HIVTestDTO;
import org.jsi.elmis.model.report.StockControlCard;
import org.jsi.elmis.rest.request.HIVTestKitChangeRequest;
import org.jsi.elmis.rest.result.HIVTestDARResult;

/**
 * @author Mesay S. Taye
 *
 */
public interface HIVTestingService {

	public List<Constant> getHIVTestPurposes();

	public List<Constant> getHIVTestResults();

	public List<Constant> getHIVTestTypes();
	
	public Integer doHIVTest(HIVTestDTO hivTest, Node site, Date txnDate,
			Integer userId , ActionOrder actionOrder, Boolean batchAware) throws UnavailableNodeProductException, InsufficientNodeProductException;

	public HIVProductAvailabilityDTO checkAvailabilityOfProduct(String hivTestType,
			Integer nodeId);

	public List<HIVTestDARItem> getHIVDARItems(Date from, Date to, Integer nodeId);

	public HIVProductAvailabilityDTO getCurrentBeginningBalance(String hivTestType,
			Integer nodeId, Integer numberOfDays) throws UnavailableHIVTestProductException, UnavailablePhysicalCountException, UnavailableNodeProductException;

	public HIVTestProduct getHIVProduct(String hivTestType);

	public HIVTestDARResult generateHIVDAR(Date from, Date to, Integer nodeId) throws UnavailableHIVTestProductException, UnavailableNodeProductException, UnavailablePhysicalCountException;

	public BigDecimal selectMostRecentPC(List<BigDecimal> sccList);

	public String getNextClientNumber(Date date);

    public Boolean saveHIVTestKits(HIVTestKitChangeRequest request);

}
