/*
\ * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.service.interfaces;

import java.sql.SQLException;
import java.util.List;

import org.jsi.elmis.model.Facility;
import org.jsi.elmis.model.GeographicLevel;
import org.jsi.elmis.model.GeographicZone;
import org.jsi.elmis.model.ProcessingPeriod;
import org.jsi.elmis.model.Program;
import org.jsi.elmis.model.TransactionType;
import org.jsi.elmis.rest.result.FacilityDistrictAndProvinceResult;

/**
 * @author Mesay S. Taye
 *
 */
public interface MiscellaneousService {
	
	public List<Program> getAllPrograms();
	
	public List<Facility> getAllFacilities(Integer geoZoneId);
	
	public FacilityDistrictAndProvinceResult selectMyDistrictAndProvince();
	
	public List<ProcessingPeriod> getAllProcessingPeriods(String scheduleCode);
	
	public List<GeographicLevel> getAllGeographicLeves();

	public List<GeographicZone> getAllGeographicZones();

	public Facility getFacilityById(Integer facilityId);
	
	public Facility getFacilityByCode(String code);

	public ProcessingPeriod getCurrentPeriod(String scheduleCode);

	public ProcessingPeriod getPreviousPeriod(String scheduleCode);
	
	public ProcessingPeriod getProcessingPeriod(Integer periodId);
	
	public ProcessingPeriod getPrecedingProcessingPeriod(Integer periodId);
	
	public List<TransactionType> getTransactionTypes();
	
	public Boolean reorderActions() throws SQLException;

    public Program getProgramById(Integer id);
}
