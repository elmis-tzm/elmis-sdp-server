/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.service.interfaces;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.jsi.elmis.exceptions.InsufficientNodeProductException;
import org.jsi.elmis.exceptions.UnavailableNodeProductException;
import org.jsi.elmis.model.Action;
import org.jsi.elmis.model.ActionOrder;
import org.jsi.elmis.model.Node;
import org.jsi.elmis.model.NodeProductDateBalance;
import org.jsi.elmis.model.NodeTransaction;
import org.jsi.elmis.model.Transaction;
import org.jsi.elmis.model.TransactionHistoryItem;
import org.jsi.elmis.model.TransactionProduct;
import org.jsi.elmis.model.TransactionType;
import org.jsi.elmis.model.dto.TransactionHistoryDTO;
import org.jsi.elmis.model.dto.transaction.details.TransactionDetails;

/**
 * @author Mesay S. Taye
 *
 */
public interface TransactionService {
	
	public Integer performTransaction(Node supplier, Node recipient,
									  List<TransactionProduct> txnProducts, String txnTypeName,
									  Date txnDate, Integer userId , ActionOrder actionOrder, Integer reversedTxnI, String remarks, Boolean batchAware) throws UnavailableNodeProductException, InsufficientNodeProductException;
	
	public TransactionType getTransactionTypeByName(String name);

	public List<TransactionHistoryItem> getTransactionsToReverse(Integer nodeId, Integer limit, Integer offset, Date fromTxnDate, Date toTxnDate, String programCode, String productCode, String scheduleCode);

	public String getTransactionDescription(Integer txnId);

	public Integer getTransactionsToReverseCount(Integer nodeId, Date fromTxnDate, Date toTxnDate, String programCode, String productCode, String scheduleCode);

	public String getScheduleCodeForReversal();
	
	public BigDecimal aggregatedTxnQtyByType(Integer productId, String txnType,
			Integer periodId);

	public BigDecimal getAveragePeriodicConsumption(Integer productId,
			Integer periodId, String scheduleCode, Integer noOfPeriods, BigDecimal currentPeriodConsumption);

	public List<TransactionHistoryDTO> selectTransactionHistory(Integer nodeId,
			Integer productId, Date from);

	public Action getAction(Integer id);
	
	public Integer getTransactionCountOnNodeProductAfterDate(Integer nodeId, Integer productId , Date txnDate);

	public Integer getMaxActionOrderInADay(Date date);

	public NodeProductDateBalance getBalanceOnDate(Date date, Integer nppId);

	public Integer reverseTransaction(Transaction transaction,
			TransactionType txnType,List<NodeTransaction> nodeTxns, Integer userId)
			throws UnavailableNodeProductException,
			InsufficientNodeProductException;

	public Integer reverseTransaction(Integer transactionId, Integer userId)
			throws UnavailableNodeProductException,
			InsufficientNodeProductException;

	public Integer selectMaxNonReversalActionOrderOnADate(Date date);

	public TransactionDetails getTransactionDetails(Integer txnId);

	public Map<String, NodeTransaction> getNodeTxnWithDirections(
			List<NodeTransaction> nodeTxns);

	public Map<String, NodeProductDateBalance> getNodeProductBalancesOnDate(
			Date date, Integer nodeId, Integer programId);

	public List<TransactionHistoryDTO> selectTransactionHistory(Integer nodeId,
			Integer productId, Date from, Date to);

	public Integer registerBulkConsumption(Node node,
			List<TransactionProduct> txnProducts, String txnType, Date txnDate,
			Integer userId, ActionOrder actionOrder, Boolean batchAware)
			throws UnavailableNodeProductException,
			InsufficientNodeProductException;

	public void refreshView4RnR();

	public void refreshActionTimeline();

	public Integer reverseStockTransferTransaction(Integer stockTransferId, Integer userId) throws InsufficientNodeProductException, UnavailableNodeProductException;


}
