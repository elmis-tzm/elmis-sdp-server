/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.service.interfaces;

import java.util.Date;
import java.util.List;

import org.jsi.elmis.exceptions.InsufficientNodeProductException;
import org.jsi.elmis.exceptions.UnavailableNodeProductException;
import org.jsi.elmis.model.*;
import org.jsi.elmis.model.dto.*;
import org.jsi.elmis.rest.client.model.SmartCarePatientRegistration;

/**
 * @author Mesay S. Taye
 *
 */
public interface ARVDispensingService {
	
	public Regimen getRegimenByARTNo(String artNo);

	public Regimen getRegimenByCode(String regimenCode);
	
	public List<RegimenProductCombination> getRegimenProductCombos(Integer regimenId); 
	
	public List<RegimenCombinationProductDTO> getRegimenComboProducts(String artNo, Integer nodeId , Integer comboId);

	public Integer dispenseARV(Node node,List<ARVDispensationLineItemDTO> arvDispensationItems,Date dispensationDate, Integer userId, Integer dispenserId, Integer productComboId ,
			Integer clientId , ActionOrder actionOrder, Boolean batchAware) throws UnavailableNodeProductException, InsufficientNodeProductException;

    public Integer dispenseEM(Node node, Integer programId,
                              List<DispensationLineItemDTO> dispensationItems,
                              Date dispensationDate, Integer userId, Integer dispenserId, Integer productComboId,
                              Integer clientId, ActionOrder actionOrder, Boolean batchAware)
            throws UnavailableNodeProductException,
            InsufficientNodeProductException;

	public FacilityClient getARVClinetByARTNOByART(String artNo);
	
	public FacilityClient getARVClinetByARTNOByNRC(String nrcNo);

	public List<FacilityClient> getARVClinetByARTNOByPropertyCombinations(Date from, Date to, String firstName, String lastName, String sex, String artNumber, String nrcNumber, String nupin, String searchId);

	public List<Regimen> getAllRegimens();
	
	public List<RegimenLine> getAllRegimenLines();

	public List<Regimen> getRegimensByLineId(Integer lineId);

	public RegimenProductCombination getLastDispensedComboForClient(String artNo);

	public Integer changeRegimen(FacilityClient client);

	public List<DosageUnit> getDosageUnits();

	public List<DosageFrequency> getDosageFrequencies();

	public Integer registerARVClient(FacilityClient arvClient);

	public Integer upsertARVClients(List<FacilityClient> facilityClients);

	public Integer upsertARVClientsFromSmartCare(List<SmartCarePatientRegistration> smartCarePatientRegistrations);

	public FacilityClient updateRegimenFromSmartCare(SmartCarePatientRegistration smartCarePatientRegistration);

	public Integer updatePatientUUIForARVClientsFromSmartCare();

	public Integer updateARVClient(FacilityClient arvClient);

	public Dispensation getPreviousDispensationForCombination(Integer comboId,
                                                              String artNo);
	public ARVActivityRegister generateARVActivityRegister(Integer nodeId , Date from, Date to);

	public List<FacilityClient> getARVClientsExpectedToday();
	
	public List<FacilityClient> getARVClientsExpectedByDate(Date date);

	public Integer resetClientNextVisitDate(Integer clientId);

	public DispensationInfo getAllARTDispensations(Date from, Date to, Integer afterTxnId);

    public DispensationInfo getAllReversedARTDispensations(Date from, Date to);

	public List<Integer> getReversedTransctionsAfterTxn(Integer txnId);

	public FacilityClientViralLoad getFacilityClientViralLoad(Integer facilityClientId);

	public FacilityClientCD4Count getFacilityClientCD4Count(Integer facilityClientId);

	public FacilityClientVital getFacilityClientVitals(Integer facilityClientId);
	
}
