/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.service.factory;

import org.jsi.elmis.service.implementation.DMRnRIntializerService;
import org.jsi.elmis.service.implementation.FacilityRnRIntializerService;
import org.jsi.elmis.service.interfaces.PropertiesService;
import org.jsi.elmis.service.interfaces.RnRIntializerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
public class RnRServiceConfiguration {
    @Autowired
    ApplicationContext ctx;
    @Autowired
    PropertiesService propertiesService;

    public RnRIntializerService getIntializerService() {
        RnRIntializerService rnRIntializerService = null;
        Boolean dmInstallation = propertiesService.getProperty("installation.type", false).getValue().equalsIgnoreCase("DM") ? true : false;
        if (dmInstallation) {
            rnRIntializerService = (RnRIntializerService) ctx.getBean("dMRnRIntializerService");
        } else {
            rnRIntializerService = (RnRIntializerService) ctx.getBean("facilityRnRIntializerService");
        }
        return rnRIntializerService;
    }
}
