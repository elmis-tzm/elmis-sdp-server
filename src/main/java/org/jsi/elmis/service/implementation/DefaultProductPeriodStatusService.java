/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.service.implementation;

import java.math.BigDecimal;

import org.jsi.elmis.dao.ProductPeriodStatusDAO;
import org.jsi.elmis.model.ProcessingPeriod;
import org.jsi.elmis.model.ProductPeriodStatus;
import org.jsi.elmis.model.TransactionType;
import org.jsi.elmis.service.interfaces.MiscellaneousService;
import org.jsi.elmis.service.interfaces.ProductPeriodStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Mesay S. Taye
 *
 */
@Component
public class DefaultProductPeriodStatusService implements ProductPeriodStatusService{

	@Autowired
	ProductPeriodStatusDAO productPeriodStatusDAO;
	
	@Autowired
	MiscellaneousService miscService;
	
	@Override
	public BigDecimal getPreviousPeriodPhysicalCount(Integer productId , String scheduleCode){
		ProcessingPeriod period = miscService.getPreviousPeriod(scheduleCode);
		if(period == null){
			return null;
		}
		ProductPeriodStatus prodPeriodStatus = productPeriodStatusDAO.getProductPeriodStatus(period.getId(), productId);
		if(prodPeriodStatus != null){
			return (prodPeriodStatus.getLatestPhysicalCount().compareTo(BigDecimal.ZERO) == 0 )? prodPeriodStatus.getStockOnHand():prodPeriodStatus.getLatestPhysicalCount();
		}
		return null;
	}
	
	@Override
	public Integer saveProductPeriodStatus(Integer periodId,Integer productId, 
			BigDecimal prodTxnQuantity , TransactionType txnType , String  scheduleCode){
		ProductPeriodStatus ppStatus =productPeriodStatusDAO.getProductPeriodStatus(periodId, productId);
		if(ppStatus == null){
			ppStatus = new ProductPeriodStatus();
			ppStatus.setPeriodId(periodId);
			ppStatus.setProductId(productId);
			//TODO: there's this assumption that there will be no possibility that there is no physical count for previous period
			BigDecimal prevPhysicalCount = getPreviousPeriodPhysicalCount(productId , scheduleCode); //TODO: scheduleCode should be the same as getProcessingPeriod(periodId).getCode()
			prevPhysicalCount = (prevPhysicalCount != null)?prevPhysicalCount:BigDecimal.ZERO;
			ppStatus.setBeginningBalance(prevPhysicalCount);
			ppStatus.setLatestPhysicalCount(prevPhysicalCount);
			
/*			if(ppStatus.getBeginningBalance().compareTo(BigDecimal.ZERO) == 0  && prodTxnQuantity.compareTo(BigDecimal.ZERO) > 0 && txnType.isPositive()){
				//TODO: terminate current stockout period
			}*/
			
			if(txnType.getName().equalsIgnoreCase(TransactionType.ARV_DISPENSING)){
				ppStatus.setQuantityDispensed(prodTxnQuantity);
				ppStatus.setQuantityReceived(BigDecimal.ZERO);
				ppStatus.setStockOnHand(ppStatus.getBeginningBalance().subtract(prodTxnQuantity));
			} else if(txnType.getName().equalsIgnoreCase(TransactionType.ISSUE_OUT)){
				ppStatus.setQuantityDispensed(prodTxnQuantity);
				ppStatus.setQuantityReceived(BigDecimal.ZERO);
				ppStatus.setStockOnHand(ppStatus.getBeginningBalance().subtract(prodTxnQuantity));
			}else if (txnType.getName().equalsIgnoreCase(TransactionType.ISSUING)){
				ppStatus.setQuantityDispensed(prodTxnQuantity);
				ppStatus.setQuantityReceived(BigDecimal.ZERO);
				ppStatus.setStockOnHand(ppStatus.getBeginningBalance().subtract(prodTxnQuantity));
			} else if (txnType.getName().equalsIgnoreCase(TransactionType.SCREENING_TEST)){
				ppStatus.setQuantityDispensed(prodTxnQuantity);
				ppStatus.setQuantityReceived(BigDecimal.ZERO);
				ppStatus.setStockOnHand(ppStatus.getBeginningBalance().subtract(prodTxnQuantity));
			} else if (txnType.getName().equalsIgnoreCase(TransactionType.CONFIRMATORY_TEST)){
				ppStatus.setQuantityDispensed(prodTxnQuantity);
				ppStatus.setQuantityReceived(BigDecimal.ZERO);
				ppStatus.setStockOnHand(ppStatus.getBeginningBalance().subtract(prodTxnQuantity));
			} else if (txnType.getName().equalsIgnoreCase(TransactionType.POSITIVE_ADJUSTMENT)){
				ppStatus.setQuantityDispensed(BigDecimal.ZERO);
				ppStatus.setQuantityReceived(BigDecimal.ZERO);
				ppStatus.setStockOnHand(ppStatus.getBeginningBalance().add(prodTxnQuantity));
			} else if (txnType.getName().equalsIgnoreCase(TransactionType.NEGATIVE_ADJUSTMENT)){
				ppStatus.setQuantityDispensed(BigDecimal.ZERO);
				ppStatus.setQuantityReceived(BigDecimal.ZERO);
				ppStatus.setStockOnHand(ppStatus.getBeginningBalance().subtract(prodTxnQuantity));
			} else if (txnType.getName().equalsIgnoreCase(TransactionType.RECEIVING)){
				ppStatus.setQuantityDispensed(BigDecimal.ZERO);
				ppStatus.setQuantityReceived(prodTxnQuantity);
				ppStatus.setStockOnHand(ppStatus.getBeginningBalance().add(prodTxnQuantity));
			} 
			return productPeriodStatusDAO.insertProductPeriodStatus(ppStatus);
				
		} else {
/*			
			if(ppStatus.getStockOnHand().compareTo(BigDecimal.ZERO) == 0  && 
					prodTxnQuantity.compareTo(BigDecimal.ZERO) > 0 && 
					txnType.isPositive()){
				//TODO: end current stockout period
			} else if (ppStatus.getStockOnHand().compareTo(BigDecimal.ZERO) > 0 && 
					ppStatus.getStockOnHand().compareTo(prodTxnQuantity) == 0 && 
					!txnType.isPositive()){
				//productPeriodStatusDAO.insertProductPeriodStockOut(new ProductPeriodStockedOutPeriod(null, ppStatus.getId(), startDate, endDate, numberOfDays))
				
			}*/
			
			if(txnType.getName().equalsIgnoreCase(TransactionType.ARV_DISPENSING)){
				ppStatus.setQuantityDispensed(ppStatus.getQuantityDispensed().add(prodTxnQuantity));
				ppStatus.setStockOnHand(ppStatus.getStockOnHand().subtract(prodTxnQuantity));
			}else if(txnType.getName().equalsIgnoreCase(TransactionType.ISSUE_OUT)){
				ppStatus.setQuantityDispensed(ppStatus.getQuantityDispensed().add(prodTxnQuantity));
				ppStatus.setStockOnHand(ppStatus.getStockOnHand().subtract(prodTxnQuantity));
			}else if (txnType.getName().equalsIgnoreCase(TransactionType.ISSUING)){
				ppStatus.setQuantityDispensed(ppStatus.getQuantityDispensed().add(prodTxnQuantity));
				ppStatus.setStockOnHand(ppStatus.getStockOnHand().subtract(prodTxnQuantity));
			} else if (txnType.getName().equalsIgnoreCase(TransactionType.SCREENING_TEST)){
				ppStatus.setQuantityDispensed(ppStatus.getQuantityDispensed().add(prodTxnQuantity));
				ppStatus.setStockOnHand(ppStatus.getStockOnHand().subtract(prodTxnQuantity));
			} else if (txnType.getName().equalsIgnoreCase(TransactionType.CONFIRMATORY_TEST)){
				ppStatus.setQuantityDispensed(ppStatus.getQuantityDispensed().add(prodTxnQuantity));
				ppStatus.setStockOnHand(ppStatus.getStockOnHand().subtract(prodTxnQuantity));
			} else if (txnType.getName().equalsIgnoreCase(TransactionType.POSITIVE_ADJUSTMENT)){
				ppStatus.setStockOnHand(ppStatus.getStockOnHand().add(prodTxnQuantity));
			} else if (txnType.getName().equalsIgnoreCase(TransactionType.NEGATIVE_ADJUSTMENT)){
				ppStatus.setStockOnHand(ppStatus.getStockOnHand().subtract(prodTxnQuantity));
			} else if (txnType.getName().equalsIgnoreCase(TransactionType.RECEIVING)){
				ppStatus.setStockOnHand(ppStatus.getStockOnHand().add(prodTxnQuantity));
				ppStatus.setQuantityReceived(ppStatus.getQuantityReceived().add(prodTxnQuantity));
			} else if(txnType.getName().equalsIgnoreCase(TransactionType.DISPENSING_REVERSAL)){
				ppStatus.setQuantityDispensed(ppStatus.getQuantityDispensed().subtract(prodTxnQuantity));
				ppStatus.setStockOnHand(ppStatus.getStockOnHand().add(prodTxnQuantity));
			}else if (txnType.getName().equalsIgnoreCase(TransactionType.RECEIVING_REVERSAL)){
				ppStatus.setStockOnHand(ppStatus.getStockOnHand().subtract(prodTxnQuantity));
				ppStatus.setQuantityReceived(ppStatus.getQuantityReceived().subtract(prodTxnQuantity));
			} 
			
			return productPeriodStatusDAO.updateProductPeriodStatus(ppStatus);
		}
	}
}