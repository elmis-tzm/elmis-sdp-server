/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.service.implementation;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.jsi.elmis.dao.SmartCareIntegrationDAO;
import org.jsi.elmis.rest.client.WSClientSmartCare;
import org.jsi.elmis.rest.client.model.SmartCarePatientRegistration;
import org.jsi.elmis.service.interfaces.ARVDispensingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;


/**
 * @author Adugna Worku
 * 
 *  This service takes care of any integration services that is related with
 *  SmartCare integration
 *  
 **/

@Service
public class SmartCareIntegrationService {

	@Autowired
	private SmartCareIntegrationDAO smartCareIntegrationDAO;
	@Autowired
	private WSClientSmartCare wsClientSmartCare;

	@Autowired
	private ARVDispensingService  arvDispensingService;

	@Value("${smartcare.integration.numberOfDaysToPullRegistrationsFor}")
	private String numberOfDaysToPullRegistrationsFor;
	
	private Integer WORKING_WEEK_DAYS_OFFSET = 2;
	
	public List<Map<String, Object>> getDispensationDataByDate(Date startDate, Date endDate){
		return smartCareIntegrationDAO.getDispensation(startDate, endDate);
	}
	
	
	public void importPatientRegFromSmartCare(){
		
		Date endDate = new Date();
		Date startDate = (Date) endDate.clone();
		Calendar cal = Calendar.getInstance();
		cal.setTime(startDate);
		Integer dayOfYear = cal.get(Calendar.DAY_OF_YEAR);
		cal.set(Calendar.DAY_OF_YEAR, (dayOfYear - Integer.valueOf(numberOfDaysToPullRegistrationsFor)));
		startDate = cal.getTime();
		
		
		//TODO : subject to change when SmartCare implementation removes explicit parameters 0 and 1
		ArrayList<SmartCarePatientRegistration> newRegistrations = wsClientSmartCare.pullClientRegistrationFromSmartCare(0, startDate, endDate);
		ArrayList<SmartCarePatientRegistration> updatedRegistrations = wsClientSmartCare.pullClientRegistrationFromSmartCare(1, startDate, endDate);

/*		Gson smartCareJSONMarshaller = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSS").create();
		String regJSON = "[\n" +
				"  {\n" +
				"    \"RegistrationDate\": \"2017-06-28T14:25:54.0000+02:00\",\n" +
				"    \"DateOfBirth\": \"1985-07-14T00:00:00.0000+02:00\",\n" +
				"    \"ArtNumber\": \"5040-150-785222-6\",\n" +
				"    \"NrcNumber\": \"\",\n" +
				"    \"Vitals\": {\n" +
				"      \"DateCollected\": \"2017-06-28T14:25:54.0000+02:00\",\n" +
				"      \"Height\": \"146.00000\",\n" +
				"      \"Weight\": \"57.00000\",\n" +
				"      \"BMI\": \"null\",\n" +
				"      \"BloodPressure\": \"114/80\"\n" +
				"    },\n" +
				"    \"CD4\": {\n" +
				"      \"LastCD4Count\": \"500.00\",\n" +
				"      \"LastCD4CountDate\": \"2017-06-28T14:25:54.0000+02:00\"\n" +
				"    },\n" +
				"    \"ViralLoad\": {\n" +
				"      \"LastViralLoad\": \"1050\",\n" +
				"      \"LastViralLoadDate\": \"2017-06-28T14:25:54.0000+02:00\"\n" +
				"    },\n" +
				"    \"RegimenId\": null,\n" +
				"    \"RegimenCode\": \"ABC + 3TC +EFV (ADFL)\",\n" +
				"    \"FirstName\": \"TESTER\",\n" +
				"    \"LastName\": \"TEST\",\n" +
				"    \"PatientGUID\": \"63b1dc761e7f4d82aa768a83bcb0ee05\",\n" +
				"    \"PatientID\": \"5040-150-00002-2\",\n" +
				"    \"Sex\": \"M\",\n" +
				"    \"BirthDay\": \"14\",\n" +
				"    \"BirthMonth\": \"7\",\n" +
				"    \"BirthYear\": \"1985\",\n" +
				"    \"Sequence\": -1\n" +
				"  }\n" +
				"]";

		System.out.println(regJSON);

		ArrayList<SmartCarePatientRegistration> newRegistrations = smartCareJSONMarshaller.fromJson(regJSON,
				new TypeToken<ArrayList<SmartCarePatientRegistration>>() {
				}.getType());*/

		if(newRegistrations != null){
			arvDispensingService.upsertARVClientsFromSmartCare(newRegistrations);
		}
		if(updatedRegistrations != null){
			arvDispensingService.upsertARVClientsFromSmartCare(updatedRegistrations);
		}
	}
	
	public Integer getMaxSequenceNumber(){
		return smartCareIntegrationDAO.getMaxSequenceNumber();
	}
	
}
