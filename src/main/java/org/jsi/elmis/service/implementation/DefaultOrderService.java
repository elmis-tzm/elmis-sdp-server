/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.service.implementation;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.jsi.elmis.common.constants.ELMISConstants;
import org.jsi.elmis.dao.CommonDAO;
import org.jsi.elmis.dao.OrderDAO;
import org.jsi.elmis.dao.ProductDAO;
import org.jsi.elmis.model.Facility;
import org.jsi.elmis.model.Order;
import org.jsi.elmis.model.ProcessingPeriod;
import org.jsi.elmis.model.ProcessingSchedule;
import org.jsi.elmis.model.Product;
import org.jsi.elmis.model.Program;
import org.jsi.elmis.model.ProgramProduct;
import org.jsi.elmis.service.interfaces.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

@Component
public class DefaultOrderService implements OrderService {
	@Value(value = "${orders.directory}")
	String ordersDirectoryPath;
	@Value(value = "${shipment.directory}")
	String shipmentDirectoryPath;
	@Autowired
	OrderDAO orderDAO;
	@Autowired
	CommonDAO commonDAO;
	@Autowired
	ProductDAO productDAO;
	private final Gson jSONMarshaller = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSS").create();
	SimpleDateFormat sdf = new SimpleDateFormat("MMyyyy");
	@Override
	public Integer saveOrders(List<Order> orders) {
		
		Integer result = orderDAO.saveOrders(orders);
		Map<String, Object> orderMap = null;
		List<String[]> shipmentCSVContent = new ArrayList<>();
		File csvShipmentFile;
		String csvFilePath;
		Facility facility;
		Program program;
		ProcessingPeriod period;
		Product product;
		if(result != null){
			CSVWriter csvWriter = null;
			for(Order order : orders){
				
				if(order != null){
					
					orderMap = jSONMarshaller.fromJson(order.getOrderJson(), Map.class);
					
					facility = new Gson().fromJson(orderMap.get("facility").toString(), Facility.class);
					program = new Gson().fromJson(orderMap.get("program").toString(), Program.class);
					period = new Gson().fromJson(orderMap.get("processingPeriod").toString(), ProcessingPeriod.class);
					
					List<String> csvValues;
					for(Map<String, Object> orderItem : (List<Map<String, Object>>) orderMap.get("orderItems")){
						
						product = jSONMarshaller.fromJson(orderItem.get("product").toString(), Product.class);
						
						csvValues = new ArrayList<>();
						
						csvValues.add( orderMap.get("orderNumber").toString() );
						csvValues.add( facility.getCode() );
						csvValues.add( product.getCode());
						csvValues.add( orderItem.get("quantity").toString() );
						csvValues.add( orderItem.get("approvedQuantity").toString() );
						csvValues.add( sdf.format(period.getStartdate()) );
						/**
						 * TODO : the below code will have to be re-written to allow 
						 * product substitutions
						 */
						csvValues.add( "" );
						csvValues.add( "" );
						csvValues.add( "0" );
						csvValues.add( "0" );
						
						shipmentCSVContent.add(csvValues.toArray(new String[0]));
					}
				}
				
				try {
					csvFilePath = shipmentDirectoryPath.replace("{{file-name}}", orderMap.get("orderNumber").toString()) + ".csv";
					csvShipmentFile = new File(csvFilePath);

					if(!csvShipmentFile.exists()){
						csvShipmentFile.createNewFile();
					}
					csvWriter = new CSVWriter(new FileWriter(csvShipmentFile),CSVWriter.DEFAULT_SEPARATOR,CSVWriter.NO_QUOTE_CHARACTER);
					csvWriter.writeAll(shipmentCSVContent);
				} catch (IOException e) {
					e.printStackTrace();
				}finally {
					try {
						csvWriter.close();
					} catch (Exception e2) {
					}
				}
			}	 
		}
		return result;
	}

	@Override
	public Integer updateOrders(List<Order> orders) {
		return orderDAO.updateOrders(orders);
	}
	
	@Override
	public Order getOrderByRequisitionId(Integer requisitionId) {
		return orderDAO.getOrderByRequisitionId(requisitionId);
	}

	@Override
	public List<Map<String, Object>> getAllOrders() {
		CSVReader reader = null;
		List<ProcessingPeriod> periods = commonDAO.getAllProcessingPeriods(ProcessingSchedule.MONTHLY);
		List<Program> programs = commonDAO.getAllPrograms();
		File orderFolder = new File(ordersDirectoryPath);
		List<Map<String, Object>> orderList = new ArrayList<>();
		Map<String, Object> orderMap;
		Map<String, Object> orderItemMap;
		List<Map<String, Object>> orderProductsMapList;
		List<ProgramProduct> programProducts = productDAO.getProgramProduct();
		try {
			
			for (File orderFile : orderFolder.listFiles()) {
				reader = new CSVReader(new FileReader(orderFile));
				List<String[]> csvContent = reader.readAll();
				if(csvContent==null || csvContent.isEmpty()){
					continue;
				}
				
				Optional<ProcessingPeriod> periodResult = periods.stream().filter(pp-> sdf.format(pp.getStartdate()).equalsIgnoreCase(csvContent.get(0)[4]) ).findFirst();
				Optional<Program> programResult = programs.stream().filter(p->p.getCode().equalsIgnoreCase(extractProgramCode(csvContent.get(0)[0]))).findFirst();
				Facility facility = commonDAO.getFacilityByCode(csvContent.get(0)[1]);
				if(facility==null || !programResult.isPresent() || !periodResult.isPresent()){
					continue;
				}
				
				orderMap = new HashMap<>();
				
				Integer requisitionId =  Integer.valueOf(orderFile.getName().substring(1, orderFile.getName().indexOf(".")));
				
				
				Order order = getOrderByRequisitionId(requisitionId);
				if(order!=null && order.getStatus().equals(ELMISConstants.COMPLETED.getValue())){
					continue;
				}
				
				
				orderMap.put("requisitionId", requisitionId);
				
				orderProductsMapList = new ArrayList<>();
				
				for(String[] values : csvContent){
					orderItemMap = new HashMap<>();
					Optional<ProgramProduct> programProductResult = programProducts.stream().filter(programProduct -> programProduct.getProduct().getCode().equalsIgnoreCase(values[2])).findFirst();
					if(!programProductResult.isPresent()){
						continue;
					}
					Map<String, Object> product = new HashMap<>();
					product.put("code", programProductResult.get().getProduct().getCode());
					product.put("primaryname", programProductResult.get().getProduct().getPrimaryname());
					product.put("id", programProductResult.get().getProduct().getId());
					product.put("strength", programProductResult.get().getProduct().getStrength());
					orderItemMap.put("product", new Gson().toJson(product));
					orderItemMap.put("quantity", values[3]);
					orderItemMap.put("approvedQuantity", values[3]);
					orderItemMap.put("balance", 0d);//TODO : change with actual balance
					orderProductsMapList.add(orderItemMap);
				}
				
				orderMap.put("dateSubmitted", csvContent.get(0)[5]);
				orderMap.put("facility", new Gson().toJson(facility));
				orderMap.put("processingPeriod", new Gson().toJson(periodResult.get()));
				orderMap.put("program", new Gson().toJson(programResult.get()));
				
				orderMap.put("orderNumber", csvContent.get(0)[0]);
				orderMap.put("orderItems", orderProductsMapList);
				orderMap.put("orderDate",csvContent.get(0)[5]);
				orderList.add(orderMap);
			}
			System.out.println(new Gson().toJson(orderList));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				reader.close();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return orderList;
	}
	
	String extractProgramCode(String orderName){
		if(orderName==null || orderName.isEmpty()){
			return null;
		}
		StringBuilder sbProgramCode = new StringBuilder();
		for(int i=0; i<orderName.length(); i++){
			String temp = String.valueOf(orderName.charAt(i));
			try{
				Integer.parseInt(temp);
				break;
			}catch(NumberFormatException nfe){
				sbProgramCode.append(temp);
			}
		}
		return sbProgramCode.toString();
	}

}
