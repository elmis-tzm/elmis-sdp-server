/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.service.implementation;

import org.jsi.elmis.model.Requisition;
import org.jsi.elmis.rest.client.RnRInterfacing;
import org.jsi.elmis.rest.client.model.Rnr;
import org.jsi.elmis.service.interfaces.RnRIntializerService;
import org.jsi.elmis.service.interfaces.RnRService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component("dMRnRIntializerService")
public class DMRnRIntializerService extends FacilityRnRIntializerServiceAbs {
    @Autowired
    RnRService rnRService;
    @Autowired
    RnRInterfacing rnRInterfacing;

    @Override
    public Requisition intializeRequisition(Long facilityId, Long programId, Long periodId, String sourceApplication, Boolean emergency) {
        Requisition requisition = null;
        Requisition  synchRequisiton=null;
        rnRService.downloadAndSaveRnrs(facilityId.intValue(), periodId.intValue(), programId.intValue(), emergency);
        Rnr rnr = rnRInterfacing.initiateRnr(facilityId, programId, periodId, sourceApplication, emergency, "eLMIS_DM");
        if (rnr != null) {
            requisition = rnRService.saveRnr(rnr);
            requisition.setSynced(true);
            requisition.setDmInstance(true);
            synchRequisiton=synchConsumptionPoints(rnr,requisition);
        }else{
            synchRequisiton = new Requisition();
            synchRequisiton.setSynced(false);
            synchRequisiton.setDmInstance(true);
        }
        return synchRequisiton;
    }

    @Override
    public Requisition syncronize(Requisition requisition) {

        Requisition intiatedRequisition = null;
        intiatedRequisition = this.intializeRequisitionFromCentral(Long.valueOf(requisition.getFacilityId()), Long.valueOf(requisition.getProgramId()),
                Long.valueOf(requisition.getPeriodId()), null, requisition.getEmergency());
        if (intiatedRequisition != null) {
            requisition = this.synchBalanceValue(requisition, intiatedRequisition);
            requisition.setSynced(true);
        } else {
            requisition.setSynced(false);
        }
        return requisition;
    }

}
