/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.service.implementation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.jsi.elmis.common.constants.ELMISConstants;
import org.jsi.elmis.dao.AdjustmentDAO;
import org.jsi.elmis.dao.NodeDAO;
import org.jsi.elmis.dao.NodeProductDAO;
import org.jsi.elmis.dao.ProductPeriodStatusDAO;
import org.jsi.elmis.exceptions.InsufficientNodeProductException;
import org.jsi.elmis.exceptions.UnavailableNodeProductException;
import org.jsi.elmis.model.*;
import org.jsi.elmis.model.report.AdjustmentProgramProductBatch;
import org.jsi.elmis.service.interfaces.AdjustmentService;
import org.jsi.elmis.service.interfaces.MiscellaneousService;
import org.jsi.elmis.service.interfaces.NodeService;
import org.jsi.elmis.service.interfaces.ProductPeriodStatusService;
import org.jsi.elmis.service.interfaces.ProductService;
import org.jsi.elmis.service.interfaces.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


/**
 * @author Mesay S. Taye
 *
 */
@Component
public class DefaultAdjustmentService implements AdjustmentService{

    @Autowired
    TransactionService txnService;
    @Autowired
    ProductService prodService;
    @Autowired
    NodeService nodeService;
    @Autowired
    ProductPeriodStatusService productPeriodStatusService;
    @Autowired
    MiscellaneousService miscService;
    @Autowired
    ProductPeriodStatusDAO productPeriodStatusDAO;
    @Autowired
    AdjustmentDAO adjDAO;
    @Autowired
    NodeProductDAO npDAO;
    @Autowired
    NodeDAO nodeDAO;


    @Value("${rnr.schedule.weekly}")
    Boolean rnrSubmittedWeekly;
    @Value("${rnr.schedule.monthly}")
    Boolean rnrSubmittedMonthly;

    @Override
    public Boolean doAdjustment(Node node, List<AdjustmentProgramProductBatch> adjustmentProgramProductBatches, Date date, Integer userId ,
                                ActionOrder actionOrder, String remarks, Boolean batchAware) throws UnavailableNodeProductException, InsufficientNodeProductException {

        List<LossAdjustmentType> laTypes = new ArrayList<>();

        for (AdjustmentProgramProductBatch adjProduct: adjustmentProgramProductBatches) {
            LossAdjustmentType laType = adjProduct.getLaType();

            if (!laTypes.contains(laType)){
                laTypes.add(laType);
            }
        }

        for (LossAdjustmentType laType:
             laTypes) {
            //extract adjustment TxnProducts for laType
            //perform txn for laType
        }


/*        for (AdjustmentProgramProductBatch adjProd : adjustmentProgramProductBatches) {
            doAdjustment(node, adjProd.getPpId(),
                    *//* adjProd.getLaType()*//* null, date, adjProd.getQuantity() , userId , actionOrder , adjProd.getRemarks(), batchAware);
            if(actionOrder != null){
                if(actionOrder.getPrecedence().equals(ActionOrder.BEFORE)){
                    actionOrder.setPrecedence(ActionOrder.AFTER);
                    actionOrder.setNeignboringOrder(actionOrder.getNeignboringOrder());
                } else {
                    actionOrder.setNeignboringOrder(actionOrder.getNeignboringOrder() + 1);
                }
            }
        }*/
        return true;
    }

    private List<TransactionProduct> makeTransactionProducts( List<AdjustmentProgramProductBatch> adjustmentProgramProductBatches, LossAdjustmentType laType){
        List<TransactionProduct> txnProducts = new ArrayList<>();


        for (AdjustmentProgramProductBatch adjPPB:
                adjustmentProgramProductBatches) {

            if (adjPPB.getLaType().getName().equalsIgnoreCase(laType.getName())){
                TransactionProduct txnProd = findTxnProductForPP(adjPPB.getProgramProductId(), txnProducts);

                if(txnProd == null){
                    txnProd = new TransactionProduct(null, adjPPB.getProgramProductId(), null , null);
                } else {

                }

                if (txnProd != null){
                    TransactionProgramProductBatch tppb = findTxnProgramProductBatch(adjPPB.getProductBatchId(), txnProd.getBatchQuantities());
                    if(tppb == null){
                        tppb = new TransactionProgramProductBatch(null, adjPPB.getProgramProductId(), adjPPB.getProductBatchId(), adjPPB.getQuantity(), null);
                        txnProd.getBatchQuantities().add(tppb);
                    }
                }

            }
        }
        return txnProducts;
    }

    private TransactionProduct findTxnProductForPP(Integer ppId, List<TransactionProduct> txnProducts){
        for (TransactionProduct txnProduct:
             txnProducts) {
            if(txnProduct.getProgramProductId() == ppId) {
                return txnProduct;
            }
        }
        return null;
    }

    private TransactionProgramProductBatch findTxnProgramProductBatch(Integer pbId, List<TransactionProgramProductBatch> txnPPBatches ){
        for (TransactionProgramProductBatch tppb: txnPPBatches) {
            if(tppb.getProgramProductBatchId() == pbId){
                return tppb;
            }
        }
        return null;
    }


    @Override
    public Integer doAdjustment(Node node, ProgramProduct programProduct,
                                LossAdjustmentType type, Date date, Double quantity , Integer userId , ActionOrder actionOrder , String remarks, Boolean batchAware) throws UnavailableNodeProductException, InsufficientNodeProductException {

        List<TransactionProduct> txnProducts = new ArrayList<>();

        TransactionProduct txnProduct = new TransactionProduct();
        txnProduct.setProgramProductId(programProduct.getId());
        txnProduct.setQuantity(BigDecimal.valueOf(quantity));

        //add txn program product batches here

        txnProducts.add(txnProduct);

        String txnTypeName = resolveTransactionTypeForAdjustment(type);
        Integer txnId = null;

        Node adjNode = nodeDAO.getNodeByName(Node.ADJUSTMENT);

        if(txnTypeName.equals(TransactionType.POSITIVE_ADJUSTMENT)){

            if (actionOrder == null){
                Integer order = txnService.getMaxActionOrderInADay(date);
                ActionOrder ao = new ActionOrder(order , ActionOrder.AFTER);
                //supplier, recipient, txnProducts, txnTypeName, txnDate, userId , actionOrder, reversedTxnId, remarks
                txnId = txnService.performTransaction(adjNode, node, txnProducts, txnTypeName, date , userId , ao ,  null, remarks, batchAware);
            } else {
                txnId = txnService.performTransaction(adjNode, node, txnProducts, txnTypeName, date , userId , actionOrder , null, remarks, batchAware);
            }

        } else {

            if (actionOrder == null){
                Integer order = txnService.getMaxActionOrderInADay(date);
                ActionOrder ao = new ActionOrder(order , ActionOrder.AFTER);
                //supplier, recipient, txnProducts, txnTypeName, txnDate, userId , actionOrder, reversedTxnId, remarks
                txnId = txnService.performTransaction(node, adjNode, txnProducts, txnTypeName, date , userId , ao ,  null, remarks, batchAware);
            } else {
                txnId = txnService.performTransaction(node, adjNode, txnProducts, txnTypeName, date , userId , actionOrder , null, remarks, batchAware);
            }

        }


        Adjustment adjustment = new Adjustment();
        adjustment.setTransactionId(txnId);
        adjustment.setAdjustmentType(type.getName());

        NodeProduct nodeProd = nodeService.findProductByNodeAndProduct(node.getId(), programProduct.getId());
        adjustment.setNodeProductId(nodeProd.getId());
        adjDAO.insertAdjustment(adjustment);

        saveDailyAdjustmentAdjustmentTotals(date, nodeProd.getId(), type, BigDecimal.valueOf(quantity));

        return adjustment.getId();
    }


    private void saveDailyAdjustmentAdjustmentTotals(Date date, Integer nodeProgProdId, LossAdjustmentType laType, BigDecimal adjQty){

        NodeProgramProductDailyAdjustmentSummary nppDailyAdjummary = npDAO.getNPPDailyAdjSummary(date, laType , nodeProgProdId);

        BigDecimal signedAdjQty = (laType.getAdditive())?adjQty.abs():adjQty.negate();

        if(nppDailyAdjummary == null){

            nppDailyAdjummary = new NodeProgramProductDailyAdjustmentSummary();

            nppDailyAdjummary.setDate(date);
            nppDailyAdjummary.setNodeProgProdId(nodeProgProdId);
            nppDailyAdjummary.setQuantity(signedAdjQty);
            nppDailyAdjummary.setAdjustmentType(laType.getName());

            npDAO.insertNPPDailyAdjustmentSummary(nppDailyAdjummary);

        } else {
            nppDailyAdjummary.setQuantity(nppDailyAdjummary.getQuantity().add(signedAdjQty));

            npDAO.updatePPDailyAdjustmentSummary(nppDailyAdjummary);
        }
    }

    public Integer saveAdjustment(Adjustment adjustment){
        return adjDAO.insertAdjustment(adjustment);
    }

    private String resolveTransactionTypeForAdjustment(LossAdjustmentType laType){

        if(laType.getAdditive()){
            return TransactionType.POSITIVE_ADJUSTMENT;
        } else {
            return TransactionType.NEGATIVE_ADJUSTMENT;
        }
    }

    @Override
    public List<LossAdjustmentType> getAllAdjustments() {
        return adjDAO.getAllAdjustmets();
    }

    @Override
    public List<LossAdjustmentType> getLossAdjustmentTypes() {
        return adjDAO.getAllAdjustmets().stream().filter(laType -> !laType.getName().equalsIgnoreCase(ELMISConstants.COMPUTER_GENERATED_POSITIVE.getValue())
                && !laType.getName().equalsIgnoreCase(ELMISConstants.COMPUTER_GENERATED_NEGATIVE.getValue()))
                .collect(Collectors.toList());
    }

    public Integer saveProductPeriodStatusAdjustment(Integer productId , String adjustmentType , BigDecimal quantity , String scheduleCode){
        ProcessingPeriod period = miscService.getCurrentPeriod(scheduleCode);
        ProductPeriodStatus ppStatus =productPeriodStatusDAO.getProductPeriodStatus(period.getId(), productId);
        //TODO: could ppStatus be null?
        ProductPeriodStatusAdjustment ppStatusAdjustment = productPeriodStatusDAO.getProductAdjustmentForPeriod(ppStatus.getId(), adjustmentType);
        if(ppStatusAdjustment == null){
            ppStatusAdjustment = new ProductPeriodStatusAdjustment();
            ppStatusAdjustment.setAdjustmentType(adjustmentType);
            ppStatusAdjustment.setProductPeriodStatusId(ppStatus.getId());
            ppStatusAdjustment.setQuantity(quantity);
            productPeriodStatusDAO.insertProductPeriodStatusAdjustment(ppStatusAdjustment);
        } else {
            ppStatusAdjustment.setQuantity(ppStatusAdjustment.getQuantity().add(quantity));
            productPeriodStatusDAO.updateProductPeriodStatusAdjustment(ppStatusAdjustment);
        }
        return ppStatusAdjustment.getId();
    }

}
