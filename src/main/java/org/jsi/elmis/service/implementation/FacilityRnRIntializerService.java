/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.service.implementation;


import org.jsi.elmis.dao.mappers.ProgramMapper;
import org.jsi.elmis.model.Program;
import org.jsi.elmis.model.Requisition;
import org.jsi.elmis.model.adapters.RequisitionAdapter;
import org.jsi.elmis.model.adapters.RequisitionUtil;
import org.jsi.elmis.rest.client.RnRInterfacing;
import org.jsi.elmis.rest.client.model.Rnr;
import org.jsi.elmis.rnr.RnR;
import org.jsi.elmis.service.interfaces.MiscellaneousService;
import org.jsi.elmis.service.interfaces.RnRIntializerService;
import org.jsi.elmis.service.interfaces.RnRService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.stream.Collectors;

@Component("facilityRnRIntializerService")
public class FacilityRnRIntializerService extends FacilityRnRIntializerServiceAbs {
    @Autowired
    RnRService rnRService;
    @Autowired
    RnRInterfacing rnRInterfacing;
    @Autowired
    MiscellaneousService miscellaneousService;
    @Override
    public Requisition intializeRequisition(Long facilityId, Long programId, Long periodId, String sourceApplication, Boolean emergency) {

        RnR generatedRnR=null;
        Requisition requisition=null;
        Requisition intiatedRequisition=null;
        Requisition generatedRequistion=null;
        Program program =miscellaneousService.getProgramById(programId.intValue());
        try {
            intiatedRequisition=this.intializeRequisitionFromCentral( facilityId,  programId,  periodId,  sourceApplication,  emergency);
            generatedRnR=  rnRService.generateRnROnTheFly(periodId.intValue(), program.getCode(), emergency , "MONTHLY");
            generatedRequistion=adapter.convertRnR(generatedRnR);

        } catch (Exception e) {
            e.printStackTrace();
        }
        if(intiatedRequisition!=null){
            generatedRequistion=this.intiateAmcValue(generatedRequistion,intiatedRequisition);
            generatedRequistion.setSynced(true);
        }else{
            generatedRequistion.setSynced(false);
        }

        generatedRequistion.setDmInstance(false);
        return generatedRequistion;
    }
    @Override
    public Requisition syncronize(Requisition requisition) {
        Requisition intiatedRequisition=null;
        intiatedRequisition=this.intializeRequisitionFromCentral( Long.valueOf(requisition.getFacilityId()),  Long.valueOf(requisition.getProgramId()),
                Long.valueOf(requisition.getPeriodId()),  null, requisition.getEmergency() );
        if (intiatedRequisition != null) {
            requisition= this.intiateAmcValue(requisition,intiatedRequisition);
            requisition.setSynced(true);
        }else{
            requisition.setSynced(false);
        }
        return requisition;
    }

    private Requisition intiateAmcValue(Requisition generated, Requisition intiated){

        if(generated!=null &&intiated!=null ){
            generated.getRequisitionLineItems().forEach(generatedLineItem ->
                intiated.getRequisitionLineItems().stream() .filter(iLineItem->iLineItem.getProductCode()
                                .equals(generatedLineItem.getProductCode())).map(li -> {
                            generatedLineItem.setPreviousNormalizedConsumptions(li.getPreviousNormalizedConsumptions());
                                    generatedLineItem.setAmc(calculateAmc(generatedLineItem.getQuantityDispensed(),li.getAmc())); return li;
                                }
                                ).collect(Collectors.toList())
            );
        }

        return  generated;

    }
private BigDecimal calculateAmc(BigDecimal quantityDispensed,BigDecimal intiatedAmc){
    intiatedAmc= intiatedAmc!=null?intiatedAmc:new BigDecimal(0);
    BigDecimal denominator= new BigDecimal(3);
        BigDecimal calcAmc=quantityDispensed!=null?(quantityDispensed.divide(denominator,RoundingMode.HALF_UP)).add(intiatedAmc):intiatedAmc;
        return  calcAmc;
}

}
