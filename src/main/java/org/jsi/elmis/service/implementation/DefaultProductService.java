/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.service.implementation;

import java.util.List;

import org.jsi.elmis.dao.ProductDAO;
import org.jsi.elmis.dao.ProgramProductDAO;
import org.jsi.elmis.model.*;
import org.jsi.elmis.service.interfaces.ProductService;
import org.jsi.elmis.service.interfaces.PropertiesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author Mesay S. Taye
 *
 */
@Component
public class DefaultProductService implements ProductService{
	
	@Autowired
	ProductDAO pDAO;
	
	@Autowired
	ProgramProductDAO ppDAO;


	@Autowired
	PropertiesService propServ;

	private String facilityCode;

	@Override
	public Product getProductByCode(String productCode) {
		
		return pDAO.selectProductByCode(productCode);
	}
	

	@Override
	public List<FacilityApprovedProduct> getFacilityApprovedProducts() {
		facilityCode = propServ.getProperty("facility.code",false).getValue();
		return pDAO.getFacilityApprovedProducts(facilityCode);
	}


	/* (non-Javadoc)
	 * @see org.jsi.elmis.service.interfaces.ProductService#getProductById(java.lang.String)
	 */
	@Override
	public Product getProductById(Integer productId) {
		return pDAO.selectProductById(productId);
	}
	
	@Override
	public List<Program> getProductPrograms(Integer productId){
		return pDAO.getProductPrograms(productId);
	}


	@Override
	public List<ProductCategory> getProductCategories() {
		return pDAO.getProductCategories();
	}


	@Override
	public List<ProgramProduct> getProgramProductsByProgramCode(
			String programCode) {
		return pDAO.getProductsInProgram(programCode);
	}

	@Override
	public List<ProgramProduct> getAllProgramProductsByProgramCode(
			String programCode) {
		return pDAO.getAllProductsInProgram(programCode);
	}

	@Override
	public Integer update(Product product) {
		return pDAO.update(product);
	}
	
	@Override
	public Integer updateProducts(List<Product> products) {
		return pDAO.updateProducts(products);
	}


	@Override
	public Integer updateProgramProduct(ProgramProduct programProduct) {
		return ppDAO.updateProgramProduct(programProduct);
	}


	@Override
	public Integer updateProgramProducts(List<ProgramProduct> programProducts) {
		return ppDAO.updateProgramProducts(programProducts);
	}


	@Override
	public List<FacilityApprovedProduct> getFacilityApprovedProducts(String facilityCode) {
		return pDAO.getFacilityApprovedProducts(facilityCode);
	}

	@Override
	public List<ProductSubstitute> getProductSubstitutes(Integer productId) {
		return pDAO.findProductSubstitutes(productId);
	}

	@Override
	public List<ProductBatch> getProductBatches(String productCode) {
		return pDAO.getProductBatches(productCode);
	}


}
