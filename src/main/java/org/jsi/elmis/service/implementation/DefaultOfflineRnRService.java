/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.service.implementation;

import org.jsi.elmis.common.constants.ELMISConstants;
import org.jsi.elmis.dao.RnRDAO;
import org.jsi.elmis.model.OfflineRnRStatus;
import org.jsi.elmis.model.ProcessingPeriod;
import org.jsi.elmis.model.Requisition;
import org.jsi.elmis.model.adapters.RequisitionUtil;
import org.jsi.elmis.rest.client.WSClientInterfacing;
import org.jsi.elmis.rest.client.model.Rnr;
import org.jsi.elmis.rest.request.RequisitionSearchRequest;
import org.jsi.elmis.service.interfaces.OfflineRnRService;
import org.jsi.elmis.service.interfaces.PropertiesService;
import org.jsi.elmis.service.interfaces.RnRService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class DefaultOfflineRnRService implements OfflineRnRService {

    @Autowired
    RnRDAO requisitionDAO;
    @Autowired
    private RnRService mainRnRService;
    @Autowired
    RnRService rnRService;

    @Autowired
    WSClientInterfacing wsClientInterfacing;
    @Autowired
    PropertiesService propertiesService;
    @Override
    public Integer saveRnR(Requisition rnr) {
        return requisitionDAO.saveRnR(rnr);
    }

    @Override
    public Integer updateRnR(Requisition rnr) {
        return requisitionDAO.updateRnR(rnr);
    }

    @Override
    public List<Requisition> getOfflineRequisitionsByPeriodId(Integer periodId) {
        return requisitionDAO.selectRequisitionsByPeriodId(periodId);
    }

    @Override
    public Integer deleteRnR(Integer id) {
        return requisitionDAO.delete(id);
    }

    @Override
    public Integer saveOfflineRnRStatus(OfflineRnRStatus offlineRnRStatus) {
        return requisitionDAO.saveOfflineRnRStatus(offlineRnRStatus);
    }

    @Override
    public OfflineRnRStatus getOfflineRnRStatusByRequisitionId(Integer requisitionId) {
        return requisitionDAO.getOfflineRnRStatusByRequisitionId(requisitionId);
    }

    @Override
    public List<Requisition> getOfflineRequisitionsByStatus(String status) {
        return requisitionDAO.selectRequisitionsByStatus(status);
    }

    @Override
    public Map<String, List> getOfflineRequisitionsByFacilityProgram(Integer program, Integer facility, Boolean emergency) {
        //        this is synced from the databae
        Boolean dmInstallation = propertiesService.getProperty("installation.type", false).getValue().equalsIgnoreCase("DM") ? true : false;
        Map<String, List> rnrsPeriodsMap = null;
        List<ProcessingPeriod> processingPeriods=null;
        Map<String, List> value = new HashMap<>();
        List<Boolean> booleans = new ArrayList<>();
        List<Boolean> dmInstallationBooleans = new ArrayList<>();
        List<Requisition> requistionMapped = null;
        List<Requisition> centralRequisitionList = null;
        rnrsPeriodsMap = mainRnRService.downloadAndSaveLogesticPeriods(Long.valueOf(facility), Long.valueOf(program), emergency);
        List<Requisition> requisitions = requisitionDAO.selectRequisitionsByFacilityProgram(program, facility, emergency);
        dmInstallationBooleans.add(dmInstallation);
        if (rnrsPeriodsMap != null) {
            processingPeriods = rnrsPeriodsMap.get("periods");
            centralRequisitionList = rnrsPeriodsMap.get("requisitions");
        }
        if (processingPeriods == null || processingPeriods.isEmpty()) {
            booleans.add(false);
            processingPeriods = this.loadFacilityProgramPeriods(program, facility);
            value.put("fromServer", booleans);
        } else {
            booleans.add(true);
            value.put("fromServer", booleans);
            requistionMapped = RequisitionUtil.mapRequistionIntiation(processingPeriods, centralRequisitionList, requisitions,dmInstallation);
        }
        value.put("mappedRequistions", requistionMapped);
        value.put("requisitions", requisitions);
        value.put("periods", processingPeriods);
        value.put("dmInstallation", dmInstallationBooleans);
        return value;
    }

    @Override
    public List<ProcessingPeriod> loadFacilityProgramPeriods(Integer program, Integer facility) {
        return requisitionDAO.loadFacilityProgramPeriods(program, facility);
    }


}
