/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.service.implementation;

import java.sql.SQLException;
import java.util.List;

import org.jsi.elmis.dao.CommonDAO;
import org.jsi.elmis.dao.ELMISJDBC;
import org.jsi.elmis.dao.GeographicLevelDAO;
import org.jsi.elmis.dao.GeographicZoneDAO;
import org.jsi.elmis.dao.ProcessingPeriodDAO;
import org.jsi.elmis.model.Facility;
import org.jsi.elmis.model.GeographicLevel;
import org.jsi.elmis.model.GeographicZone;
import org.jsi.elmis.model.ProcessingPeriod;
import org.jsi.elmis.model.Program;
import org.jsi.elmis.model.TransactionType;
import org.jsi.elmis.rest.result.FacilityDistrictAndProvinceResult;
import org.jsi.elmis.service.interfaces.MiscellaneousService;
import org.jsi.elmis.service.interfaces.PropertiesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author Mesay S. Taye
 *
 */
@Component
public class DefaultMiscellaneousService implements MiscellaneousService {
	
	@Autowired
	CommonDAO cmnDAO;
	
	@Autowired
	GeographicLevelDAO geoLevelDAO;
	
	@Autowired
	GeographicZoneDAO geoZoneDAO;
	
	@Autowired
	ProcessingPeriodDAO periodDAO;

    @Autowired
    PropertiesService propertiesService;
    
    @Autowired
    ELMISJDBC elmisJDBC;
	

	@Override
	public List<Program> getAllPrograms() {
		return cmnDAO.getAllPrograms();
	}

	@Override
	public List<Facility> getAllFacilities(Integer geoZoneId) {
		return cmnDAO.getAllFacilities(geoZoneId);
	}
	

	
	@Override 
	public ProcessingPeriod getCurrentPeriod(String scheduleCode){
		return cmnDAO.getCurrentPeriod(scheduleCode);
	}
	

	
	@Override 
	public ProcessingPeriod getPreviousPeriod(String scheduleCode){
		return cmnDAO.getPreviousPeriod(scheduleCode);
	}

	@Override
	public FacilityDistrictAndProvinceResult selectMyDistrictAndProvince() {
		return cmnDAO.selectDistrictAndProvince(propertiesService.getProperty("facility.code",false).getValue());
	}

	@Override
	public List<ProcessingPeriod> getAllProcessingPeriods(String scheduleCode) {
		return cmnDAO.getAllProcessingPeriods(scheduleCode);
	}

	@Override
	public List<GeographicLevel> getAllGeographicLeves() {
		return geoLevelDAO.getAllGeographicLeves();
	}

	@Override
	public List<GeographicZone> getAllGeographicZones() {
		return geoZoneDAO.getAllGeographicZones();
	}

	@Override
	public Facility getFacilityById(Integer facilityId) {
		return cmnDAO.getFacilityById(facilityId);
	}
	
	@Override
	public Facility getFacilityByCode(String code) {
		return cmnDAO.getFacilityByCode(code);
	}

	@Override
	public ProcessingPeriod getProcessingPeriod(Integer periodId) {
		return periodDAO.getProcessingPeriod(periodId);
	}

	@Override
	public List<TransactionType> getTransactionTypes() {
		return cmnDAO.getTransactionTypes();
	}

	@Override
	public ProcessingPeriod getPrecedingProcessingPeriod(Integer periodId) {
		return periodDAO.getPrecedingProcessingPeriod(periodId);
	}

	@Override
	public Boolean reorderActions() throws SQLException {
		return elmisJDBC.reorderActions();
	}

    @Override
    public Program getProgramById(Integer id) {
        return cmnDAO.getProgramById(id);
    }
}
