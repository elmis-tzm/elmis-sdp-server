/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.service.implementation;

import org.jsi.elmis.model.Program;
import org.jsi.elmis.model.Requisition;
import org.jsi.elmis.model.adapters.RequisitionAdapter;
import org.jsi.elmis.rest.client.RnRInterfacing;
import org.jsi.elmis.rest.client.model.Rnr;
import org.jsi.elmis.rnr.RnR;
import org.jsi.elmis.service.interfaces.MiscellaneousService;
import org.jsi.elmis.service.interfaces.RnRIntializerService;
import org.jsi.elmis.service.interfaces.RnRService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.stream.Collectors;

/**
 * Created by abebe on 4/17/2018.
 */
public abstract class FacilityRnRIntializerServiceAbs implements RnRIntializerService {


    @Autowired
    RnRInterfacing rnRInterfacing;
    @Autowired
    RequisitionAdapter adapter;
    @Override
    public Requisition intializeRequisitionFromCentral(Long facilityId, Long programId, Long periodId, String sourceApplication, Boolean emergency){
        Requisition intiatedRequisition=null;
        Rnr initiatedRnr=null;
        initiatedRnr= rnRInterfacing.initiateRnr(facilityId,programId,periodId,sourceApplication,emergency,"eLMIS_FE");
        if(initiatedRnr!=null) {
            intiatedRequisition = adapter.convertRnr(initiatedRnr);
            intiatedRequisition=this.synchConsumptionPoints(initiatedRnr,intiatedRequisition);
        }
        return  intiatedRequisition;
    }

    protected Requisition synchBalanceValue(Requisition generated, Requisition intiated) {

        if (generated != null && intiated != null) {
            generated.getRequisitionLineItems().forEach(generatedLineItem ->
                    intiated.getRequisitionLineItems().stream().filter(iLineItem -> iLineItem.getProductCode()
                            .equals(generatedLineItem.getProductCode())).map(li -> {
                                generatedLineItem.setPreviousNormalizedConsumptions(li.getPreviousNormalizedConsumptions());
                                generatedLineItem.setBeginningBalance(li.getBeginningBalance());
                                return li;
                            }
                    ).collect(Collectors.toList())
            );
        }
        return generated;
    }

    protected Requisition synchConsumptionPoints(Rnr rnr, Requisition requisition) {
        if (requisition != null && rnr != null) {
            requisition.getRequisitionLineItems().forEach(generatedLineItem ->
                    rnr.getAllLineItems().stream().filter(iLineItem -> iLineItem.getProductCode()
                            .equals(generatedLineItem.getProductCode())).map(li -> {
                                generatedLineItem.setPreviousNormalizedConsumptions(li.getPreviousNormalizedConsumptions());
                                return li;
                            }
                    ).collect(Collectors.toList())
            );
        }
        return requisition;
    }
}
