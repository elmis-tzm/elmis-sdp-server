/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.service.implementation;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;

import com.google.common.hash.Hashing;
import com.google.gson.Gson;
import org.jsi.elmis.common.util.Crypto;
import org.jsi.elmis.dao.RightDAO;
import org.jsi.elmis.dao.RoleDAO;
import org.jsi.elmis.dao.RoleRightDAO;
import org.jsi.elmis.dao.UserDAO;
import org.jsi.elmis.dao.UserNodeRoleDAO;
import org.jsi.elmis.dao.mappers.V2.RoleMapper;
import org.jsi.elmis.dao.mappers.V2.UserAccountMapper;
import org.jsi.elmis.interfacing.openlmis.BaseFactory;
import org.jsi.elmis.model.*;
import org.jsi.elmis.rest.result.UserNodeRoleRightResult;
import org.jsi.elmis.service.interfaces.PropertiesService;
import org.jsi.elmis.service.interfaces.UserManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DefaultUserManagementService implements UserManagementService{
	
	@Autowired
	UserNodeRoleDAO userNodeRoleDao;
	@Autowired
	RoleDAO roleDao;
	@Autowired
	UserDAO userDao;
	@Autowired
	RoleRightDAO roleRightDao;
	@Autowired
	RightDAO rightDao;

	@Autowired
	PropertiesService propertiesService;

	@Autowired
	UserAccountMapper userAccountMapper;

	@Autowired
	RoleMapper roleMapper;

	@Override
	public Integer saveUserInfo(User user) {

		user.setPassword(user.getPassword());
		int userId =  userDao.saveUser(user);

		insertUserRoles(user);

		return userId;
	}

	@Override
	public User selectUserById(int id) {
		return userDao.selectUserById(id);
	}

	@Override
	public Integer updateUser(User user) {

		//first reset the user role to re-insert
		userAccountMapper.resetUserRole(user.getId());

		insertUserRoles(user);

		if(user.getPassword() != null && !user.getPassword().isEmpty())
			user.setPassword(getSHA256Hash(user.getPassword()));


		return userDao.updateByPrimaryKeySelective(user);
	}

	private String getSHA256Hash(String password) {
		return password == null ? null : Hashing.sha256()
                        .hashString(password, StandardCharsets.UTF_8)
                        .toString();
	}

	private void insertUserRoles(User user){
		user.getUserNodes()
				.stream().forEach(node -> node.getRoles()
				.stream().forEach(role -> userAccountMapper.insertUserRole(role.getId(), node.getId(), user.getId()) ));
	}

	@Override
	public Integer updateUserPassword(User user) {
		return userDao.updateUserPassword(user);
	}

	@Override
	public Integer saveRole(Role role) {

		if(role.getId() == null){
			roleMapper.insertRole(role);

			role.getRights().stream().forEach(right -> roleMapper.insertRoleRights(role.getId(), right.getName()));

		}
		else {
			roleMapper.resetRolesRightByRoleId(role.getId());

			role.getRights().stream().forEach(right -> roleMapper.insertRoleRights(role.getId(), right.getName()));

			roleMapper.updateRole(role);
		}

		return role.getId();

	}

	@Override
	public Integer assignUserNodeRoles(User user, Node node,
			List<Role> nodeRoles) {
		UserNodeRole userNodeRole = new UserNodeRole();
		userNodeRole.setUserId(user.getId());
		userNodeRole.setNodeId(node.getId());		
		return userNodeRoleDao.saveUserNodeRole(userNodeRole, nodeRoles);
	}
	@Override
	public Integer saveRoleRight(RoleRight roleRight) {
		
		return roleRightDao.saveRoleRight(roleRight);
	}
	@Override
	public List<Role> getRoles() {
		return roleMapper.getAllRoles();
	}
	@Override
	public List<RoleRight> getRoleRights() {
		return roleRightDao.getRoleRights();
	}
	@Override
	public Integer updateRole(Role role) {
		
		return roleDao.updateRole(role);
	}
	@Override
	public Integer updateRoleRights(RoleRight roleRight) {
		
		return roleRightDao.updateRoleRights(roleRight);
	}
	@Override
	public Integer updateUserNodeRoles(User user, Node node,
			List<Role> nodeRoles, int userNodeRoleId) {
		UserNodeRole userNodeRole = new UserNodeRole();
		userNodeRole.setId(userNodeRoleId);
		userNodeRole.setUserId(user.getId());
		userNodeRole.setNodeId(node.getId());
		
		return userNodeRoleDao.updateUserNodeRole(userNodeRole, nodeRoles);
	}
	@Override
	public List<User> getUsers() {
		return userDao.getUsers();
	}
	@Override
	public List<Right> getRights() {
		return rightDao.getRights();
	}
	
	@Override
	public Integer saveRoleRights(List<RoleRight> roleRights) {
		for (RoleRight roleRight : roleRights) {
			roleRightDao.saveRoleRight(roleRight);
		}	
		return 1;
	}
	
	@Override
	public Integer deleteUser(int userId) {
		return userDao.deleteUser(userId);
	}
	@Override
	public Boolean insertUserNodeRoles(List<UserNodeRole> userNodeRoles) {
		for (UserNodeRole userNodeRole : userNodeRoles) {
			try{
				userNodeRoleDao.insertUserNodeRole(userNodeRole);
			}catch(Exception ex){
				//something has broken
				return false;
			}
		}
		return true;
	}
	@Override
	public ArrayList<UserNodeRoleRightResult> authenticate(User user) throws Exception {
		ApplicationProperty unifiedUserAuthentication = propertiesService.getProperty("unified.user.authentication", true);
		String plainPassword = user.getPassword();
		ArrayList<UserNodeRoleRightResult> result;
		if(unifiedUserAuthentication != null && Boolean.valueOf(unifiedUserAuthentication.getValue())) {
			user.setPassword(Crypto.hash(user.getPassword()));
			result = userDao.authenticate(user);
			if(result.isEmpty() ){
				//try authenticating on central eLMIS
				user.setPassword(plainPassword);
				if(isUserAuthenticatedOnCentralELMIS(user)) {
					user.setPassword(Crypto.hash(user.getPassword()));
					updateUserPassword(user);
					result = userDao.authenticate(user);
				}
			}
		}else{
			result = userDao.authenticate(user);
		}
		return result;
	}
	@Override
	public ArrayList<UserNodeRoleRightResult> getUserNodeRoleRightsByUserId(
			Integer userId) {
		return userDao.getUserNodeRoleRights(userId);
	}
	@Override
	public Boolean revokeRolesFromUser(ArrayList<UserNodeRole> userNodeRoleList) {
		return userDao.revokeRolesFromUser(userNodeRoleList);
	}
	@Override
	public Boolean revokeNodesFromUser(ArrayList<UserNodeRole> userNodeRoleList) {
		return userDao.revokeNodesFromUser(userNodeRoleList);
	}
	@Override
	public Boolean revokeRightsFromRole(Integer roleId,
			ArrayList<String> rightList) {
		return userDao.revokeRightsFromRole(roleId, rightList);
	}
	@Override
	public ArrayList<UserNodeRoleRightResult> getUserNodeRoleRightsByUserAndNode(
			UserNodeRole unr) {
		return userDao.getUserNodeRoleRightsByUserAndNode(unr);
	}
	@Override
	public List<RoleRight> getRoleRightsByRoleId(Integer roleId) {
		return roleRightDao.getRoleRightsByRoleId(roleId);
	}
	@Override
	public Integer deleteRole(int roleId) {
		return roleDao.deleteRole(roleId);
	}
	@Override
	public User selectUserByEmail(String email) {
		return userDao.selectUserByEmail(email);
	}

	@Autowired
	BaseFactory baseFactory;

	@Override
	public Boolean isUserAuthenticatedOnCentralELMIS(User user) throws Exception {
		String userJSON = new Gson().toJson(user);
		userJSON = userJSON.replace("userName", "username");//TODO: replace hack job
		return  baseFactory.isUserAuthenticated("login", userJSON);
	}
 
	public List<User> getUsersV2(){
		return userAccountMapper.getAllUsers();
	}

	@Override
	public User getUserV2(Long userId) {
		return  userAccountMapper.getUserById(userId);
	}

	@Override
	public Role getRoleById(Integer roleId) {
		return roleMapper.getRoleById(roleId);
	}

	public List<Role> getRolesV2(){ return roleMapper.getAllRoles(); }


}
