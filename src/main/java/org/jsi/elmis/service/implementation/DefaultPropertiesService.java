/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.service.implementation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.PostConstruct;

import org.jsi.elmis.dao.ApplicationPropertiesDAO;
import org.jsi.elmis.model.ApplicationProperty;
import org.jsi.elmis.service.interfaces.PropertiesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
@Component
public class DefaultPropertiesService implements PropertiesService{
	
	@Autowired
	ApplicationPropertiesDAO aPropDAO;
	
	private Map<String, ApplicationProperty> properties = null;
	
	@Override
	public List<ApplicationProperty> getApplicationProperties(String key){
		return aPropDAO.getProperties(key);
	}
	
	@Override 
	public void saveApplicationProperties(List<ApplicationProperty> applicationProperties){
		aPropDAO.saveApplicationProperties(applicationProperties);
		this.setProperties(loadApplicationPoperties());
//		updatePropertiesFile(applicationProperties);
	}
	
	@Override
	public ApplicationProperty getProperty(String key, boolean loadFirst){
		if(loadFirst){
			this.setProperties(loadApplicationPoperties());
		}
		if (this.getProperties().isEmpty() || this.getProperties() == null){
			return null;
		}
		return this.getProperties().get(key);
	}
	
	@Override 
	public void setProperty(ApplicationProperty property){
		List<ApplicationProperty> properties = new ArrayList<>();
		properties.add(property);
		saveApplicationProperties(properties);
	}

	@Override
	public String getARTDefaultPrefix(){
		return getProperty("art.number.default.prefix",false).getValue();
	}
	
	@Override
	public String getFacilityCode(){
		return getProperty("facility.code",false).getValue();
	}
	
	@Override
	public Boolean getSendRnRToHub(){
		return Boolean.valueOf(getProperty("facility.code",false).getValue());
	}

	public Map<String, ApplicationProperty> getProperties() {
		return properties;
	}

	public void setProperties(Map<String, ApplicationProperty> properties) {
		this.properties = properties;
	}
	
	@PostConstruct
	public Map<String, ApplicationProperty> loadApplicationPoperties(){
		List<ApplicationProperty> properties = getApplicationProperties(null);
		Map<String,ApplicationProperty> propertyMap = new HashMap<String, ApplicationProperty>();
		if(properties != null)
		for (ApplicationProperty applicationProperty : properties) {
			try{ //TDDO: Change this. Don't catch a null  pointer exception
				propertyMap.put(applicationProperty.getKey(), applicationProperty);
			} catch (Exception ex){
				System.out.println(ex.getMessage());
			}
		}
		this.setProperties(propertyMap);
		return propertyMap;
	}
	//
	@Override
	public void updatePropertiesFile(List<ApplicationProperty> applicationProperties){
    	String propertyFileName = "application.properties";
    	File propertiesFile = new File(propertyFileName);
		FileWriter fileWriter = null;
		try {											
			Properties applicationProps = new Properties();
	    	applicationProps.load(new BufferedReader(new FileReader(propertiesFile)));
			for(ApplicationProperty prop : applicationProperties){
				applicationProps.setProperty(prop.getKey(), prop.getValue());
			}
			
			fileWriter = new FileWriter(propertiesFile);
			
			applicationProps.store(fileWriter, "eLMIS Facility Edition Configuration");
		
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(fileWriter!=null){
				try {
					fileWriter.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
}
