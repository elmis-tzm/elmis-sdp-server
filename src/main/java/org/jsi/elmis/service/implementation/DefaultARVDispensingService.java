/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.service.implementation;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jsi.elmis.common.util.DateCustomUtil;
import org.jsi.elmis.dao.ARVDispensingDAO;
import org.jsi.elmis.dao.NodeDAO;
import org.jsi.elmis.dao.NodeProductDAO;
import org.jsi.elmis.dao.RegimenDAO;
import org.jsi.elmis.exceptions.InsufficientNodeProductException;
import org.jsi.elmis.exceptions.UnavailableNodeProductException;
import org.jsi.elmis.model.*;
import org.jsi.elmis.model.dto.*;
import org.jsi.elmis.org.jsi.elmis.interfacing.smartcare.facade.ARVDispensationFacade;
import org.jsi.elmis.rest.client.WSClientSmartCare;
import org.jsi.elmis.rest.client.model.SmartCarePatientRegistration;
import org.jsi.elmis.service.interfaces.ARVDispensingService;
import org.jsi.elmis.service.interfaces.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Mesay S. Taye
 *
 */
@Component
public class DefaultARVDispensingService implements ARVDispensingService {

	@Autowired
	TransactionService txnService;
	@Autowired
	ARVDispensingDAO arvDispDAO;
	@Autowired
	RegimenDAO regDAO;
	@Autowired
	NodeProductDAO npDAO;
	
	@Autowired
	NodeDAO nodeDAO;

	@Autowired
	WSClientSmartCare wsClientSmartCare;


	@Autowired
	private ARVDispensationFacade  arvDispensationFacade;

	@Override
	public Regimen getRegimenByARTNo(String artNo) {
		return regDAO.getRegimenByART(artNo);
	}

	@Override
	public Regimen getRegimenByCode(String regimenCode) {
		return regDAO.getRegimenByCode(regimenCode);
	}

	@Override
	public Integer registerARVClient(FacilityClient arvClient) {
		return arvDispDAO.saveFacilityClient(arvClient);
	}

	@Override
	public Integer updateARVClient(FacilityClient arvClient) {
		return arvDispDAO.updateFacilityClient(arvClient);
	}

	@Override
	public List<RegimenProductCombination> getRegimenProductCombos(
			Integer regimenId) {
		return regDAO.getRegimenProductCombos(regimenId);
	}

	@Override
	public List<RegimenCombinationProductDTO> getRegimenComboProducts(
			String artNo, Integer nodeId, Integer comboId) {
		return regDAO.getRegimenComboProducts(artNo, nodeId, comboId);
	}

	@Override
	public Dispensation getPreviousDispensationForCombination(
			Integer comboId, String artNo) {
		return arvDispDAO.selectPreviousDispensationForCombination(comboId,
				artNo);
	}

	@Override
	public RegimenProductCombination getLastDispensedComboForClient(String artNo) {
		return arvDispDAO.getLastDispensedComboForClient(artNo);
	}

	@Override
	public Integer dispenseEM(Node node, Integer programId,
							   List<DispensationLineItemDTO> dispensationItems,
							   Date dispensationDate, Integer userId, Integer dispenserId, Integer productComboId,
							   Integer clientId, ActionOrder actionOrder, Boolean batchAware)
			throws UnavailableNodeProductException,
			InsufficientNodeProductException {

		List<TransactionProduct> txnProducts = new ArrayList<>();
//		List<DispensationLineItem> emLineItems = new ArrayList<>();
		for (DispensationLineItemDTO item : dispensationItems) {
			txnProducts.add(item.getTxnProduct());
//			arvLineItems.add(item.getArvDispensationLineItem());
		}

		Integer txnId = null;
		
		Node arvConsumer = nodeDAO.getNodeByName(Node.EM_CONSUMER);
		
		
		if (actionOrder == null) {
			Integer order = txnService
					.getMaxActionOrderInADay(dispensationDate);
			actionOrder = new ActionOrder(order, ActionOrder.AFTER);
			txnId = txnService.performTransaction(node, arvConsumer, txnProducts,
					TransactionType.EM_DISPENSING, dispensationDate,
					userId, actionOrder , null, null, batchAware);

		} else {
			txnId = txnService.performTransaction(node, arvConsumer, txnProducts, TransactionType.EM_DISPENSING,
					dispensationDate, userId , actionOrder, null, null, batchAware);
			
		}

        Dispensation dispensation = new Dispensation();
//		Date nextVisitDate = null;
//		arvDispensation.setNextVisitDate(nextVisitDate);
//		arvDispensation.setProductComboId(productComboId);
        dispensation.setTransactionId(txnId);
        dispensation.setClientId(clientId);
        dispensation.setDispenserId(dispenserId);

		Integer dispId = arvDispDAO.saveDispensation(dispensation);

//		for (int i = 0; i < arvLineItems.size(); i++) {
//			arvLineItems.get(i).setTransactionProductId(
//					txnProducts.get(i).getId());
//			arvLineItems.get(i).setArvDispensationId(dispId);
//			arvDispDAO.saveDispensationItem(arvLineItems.get(i));
//		}
		return dispId;
	}

    public Integer dispenseARV(Node node,
                               List<ARVDispensationLineItemDTO> arvDispensationItems,
                               Date dispensationDate, Integer userId, Integer dispenserId, Integer productComboId,
                               Integer clientId, ActionOrder actionOrder, Boolean batchAware)
            throws UnavailableNodeProductException,
            InsufficientNodeProductException {

        List<TransactionProduct> txnProducts = new ArrayList<TransactionProduct>();
        List<ARVDispensationLineItem> arvLineItems = new ArrayList<ARVDispensationLineItem>();
        for (ARVDispensationLineItemDTO item : arvDispensationItems) {
            txnProducts.add(item.getTxnProduct());
            arvLineItems.add(item.getArvDispensationLineItem());
        }

        Integer txnId = null;

        Node arvConsumer = nodeDAO.getNodeByName(Node.ARV_CONSUMER);


        if (actionOrder == null) {
            Integer order = txnService
                    .getMaxActionOrderInADay(dispensationDate);
            actionOrder = new ActionOrder(order, ActionOrder.AFTER);
            txnId = txnService.performTransaction(node, arvConsumer, txnProducts,
                    TransactionType.ARV_DISPENSING, dispensationDate,
                    userId, actionOrder , null, null, batchAware);

        } else {
            txnId = txnService.performTransaction(node, arvConsumer, txnProducts, TransactionType.ARV_DISPENSING,
                    dispensationDate, userId , actionOrder, null, null, batchAware);

        }
        Dispensation arvDispensation = new Dispensation();
        Date nextVisitDate = calculateNextAppointmentDate(dispensationDate,
                arvLineItems);
        arvDispensation.setNextVisitDate(nextVisitDate);
        arvDispensation.setProductComboId(productComboId);
        arvDispensation.setTransactionId(txnId);
        arvDispensation.setClientId(clientId);
        arvDispensation.setDispenserId(dispenserId);

        Integer dispId = arvDispDAO.saveDispensation(arvDispensation);

        for (int i = 0; i < arvLineItems.size(); i++) {
            arvLineItems.get(i).setTransactionProductId(
                    txnProducts.get(i).getId());
            arvLineItems.get(i).setArvDispensationId(dispId);
            arvDispDAO.saveDispensationItem(arvLineItems.get(i));
        }
        return dispId;
    }

	private Date calculateNextAppointmentDate(Date currentDispensationDate,
			List<ARVDispensationLineItem> arvDispensationItems) {
		Integer minNoOfDays = null;
		// TODO: proof of correctness of logic
		if (arvDispensationItems.size() > 0)
			minNoOfDays = arvDispensationItems.get(0).getNumberOfDays()
					+ arvDispensationItems.get(0).getRemainingNoOfDays();
		for (ARVDispensationLineItem item : arvDispensationItems) {
			if (item.getNumberOfDays() < minNoOfDays) {
				minNoOfDays = item.getNumberOfDays()
						+ item.getRemainingNoOfDays();
			}
		}
		return DateCustomUtil.addDays(currentDispensationDate, minNoOfDays);
	}

	@Override
	public Integer changeRegimen(FacilityClient client) {
		return arvDispDAO.updateFacilityClientRegimen(client);
	}

	@Override
	public FacilityClient getARVClinetByARTNOByART(String artNo) {
		return arvDispDAO.getARVClientByART(artNo);
	}

	@Override
	public FacilityClient getARVClinetByARTNOByNRC(String nrcNo) {
		return arvDispDAO.getARVClientByNRC(nrcNo);
	}

	@Override
	public List<FacilityClient> getARVClientsExpectedToday() {
		return arvDispDAO.getARVClientExpectedToday();
	}

	@Override
	public List<FacilityClient> getARVClinetByARTNOByPropertyCombinations(
			Date from, Date to, String firstName, String lastName, String sex, String artNumber, String nrcNumber,
            String nupin, String searchId) {

        searchId = searchId.trim();
	    String[] namesFromSearchId = searchId.trim().split(" ");

	    if(namesFromSearchId.length > 1){
	        if(firstName==null || firstName.isEmpty()){
	            firstName = namesFromSearchId[0];
            }
            if(lastName==null || lastName.isEmpty()){
	            lastName = namesFromSearchId[1];
            }


            firstName = "%"+ firstName + "%";
            lastName = "%" + lastName + "%";

            searchId = null;

        }else{
	        firstName = null;
	        lastName = null;

            searchId = "%" + searchId + "%";
        }

		return arvDispDAO.getARVClientByPropertyCombinations(from, to,
				firstName, lastName, sex, artNumber, nrcNumber, nupin, searchId);
	}

	@Override
	public List<Regimen> getAllRegimens() {
		return arvDispDAO.getAllRegimens();
	}

	@Override
	public List<Regimen> getRegimensByLineId(Integer lineId) {
		return arvDispDAO.getAllRegimensByLineID(lineId);
	}

	@Override
	public List<RegimenLine> getAllRegimenLines() {
		return arvDispDAO.getAllRegimenLines();
	}

	@Override
	public List<DosageUnit> getDosageUnits() {
		return regDAO.getDosageUnits();
	}

	@Override
	public List<DosageFrequency> getDosageFrequencies() {
		return regDAO.getDosageFrequencies();
	}

	@Override
	public ARVActivityRegister generateARVActivityRegister(Integer nodeId,
			Date from, Date to) {
		List<ARVActivityRegisterItem> arvActivityRegisterItems = arvDispDAO
				.getActivityRegisterLineItems(nodeId, from, to);
		ARVActivityRegister activityRegister = new ARVActivityRegister();
		activityRegister.setFrom(from);
		activityRegister.setTo(to);
		activityRegister.setActivityRegisterItems(arvActivityRegisterItems);
		return activityRegister;
	}
	@Override
	public DispensationInfo getAllARTDispensations(
			Date from, Date to, Integer afterTxnId) {
		List<DispensationLineItem> dispLineItems = arvDispDAO
				.getAllActivityRegisterLineItems(from, to, afterTxnId);
		DispensationInfo dInfo = new DispensationInfo();
		dInfo.setFrom(from);
		dInfo.setTo(to);
		dInfo.setDispensationLineItems(dispLineItems);
		return dInfo;
	}

    @Override
    public DispensationInfo getAllReversedARTDispensations(Date from, Date to) {
        List<DispensationLineItem> dispLineItems = arvDispDAO
                .getAllReversedActivityRegisterLineItems(from, to);
        DispensationInfo dInfo = new DispensationInfo();
        dInfo.setFrom(from);
        dInfo.setTo(to);
        dInfo.setDispensationLineItems(dispLineItems);
        return dInfo;
    }
    
    @Override
    public List<Integer> getReversedTransctionsAfterTxn(Integer txnId){
    	return arvDispDAO.getAllReversedARVTxnIds(txnId);
    }

	@Override
	public FacilityClientViralLoad getFacilityClientViralLoad(Integer facilityClientId) {
		return arvDispDAO.getFacilityClientViralLoad(facilityClientId);
	}

	@Override
	public FacilityClientCD4Count getFacilityClientCD4Count(Integer facilityClientId) {
		return arvDispDAO.getFacilityClientCD4Count(facilityClientId);
	}

	@Override
	public FacilityClientVital getFacilityClientVitals(Integer facilityClientId) {
		return arvDispDAO.getFacilityClientVitals(facilityClientId);
	}

	@Override
	public Integer resetClientNextVisitDate(Integer clientId) {
		return arvDispDAO.resetClientNextVisitDate(clientId);
	}

	@Override
	public List<FacilityClient> getARVClientsExpectedByDate(Date date) {
		return arvDispDAO.selectClientsExpectedByDate(date);	
	}

	@Override
	public Integer upsertARVClients(List<FacilityClient> facilityClients) {
		return arvDispDAO.upsertARVClients(facilityClients);
	}

	@Override
	public Integer upsertARVClientsFromSmartCare(List<SmartCarePatientRegistration> smartCarePatientRegistrations) {

		for(SmartCarePatientRegistration smartCarePatientRegistration : smartCarePatientRegistrations){
			FacilityClient facilityClient = arvDispensationFacade.toFacilityClient(smartCarePatientRegistration);
			arvDispDAO.upsertARVClient(facilityClient);
			facilityClient = arvDispDAO.getFacilityClientByUUID(facilityClient.getUuid().toString());
			if( facilityClient == null ) {
				continue;
			}
			if(smartCarePatientRegistration.getVitals() != null) {
				smartCarePatientRegistration.getVitals().setFacilityClientId(facilityClient.getId());
				arvDispDAO.upsertFacilityClientVitals(smartCarePatientRegistration.getVitals());
			}

			if(smartCarePatientRegistration.getCd4() != null) {
				smartCarePatientRegistration.getCd4().setFacilityClientId(facilityClient.getId());
				arvDispDAO.upsertFacilityClientCD4(smartCarePatientRegistration.getCd4());
			}

			if(smartCarePatientRegistration.getViralLoad() != null) {
				smartCarePatientRegistration.getViralLoad().setFacilityClientId(facilityClient.getId());
				arvDispDAO.upsertFacilityClientViralLoad(smartCarePatientRegistration.getViralLoad());
			}
		}

		return 0;
	}

	@Override
	public FacilityClient updateRegimenFromSmartCare(SmartCarePatientRegistration smartCarePatientRegistration) {

    	if(smartCarePatientRegistration == null){
    		return null;
		}

		FacilityClient facilityClient = arvDispensationFacade.toFacilityClient(smartCarePatientRegistration);

    	if( facilityClient == null ) {
			return null;
		}

		arvDispDAO.updateARVClientByARTNumber(facilityClient);
		facilityClient = arvDispDAO.getFacilityClientByUUID(facilityClient.getUuid().toString());


		if(smartCarePatientRegistration.getVitals() != null) {
			smartCarePatientRegistration.getVitals().setFacilityClientId(facilityClient.getId());
			arvDispDAO.upsertFacilityClientVitals(smartCarePatientRegistration.getVitals());
		}

		if(smartCarePatientRegistration.getCd4() != null) {
			smartCarePatientRegistration.getCd4().setFacilityClientId(facilityClient.getId());
			arvDispDAO.upsertFacilityClientCD4(smartCarePatientRegistration.getCd4());
		}

		if(smartCarePatientRegistration.getViralLoad() != null) {
			smartCarePatientRegistration.getViralLoad().setFacilityClientId(facilityClient.getId());
			arvDispDAO.upsertFacilityClientViralLoad(smartCarePatientRegistration.getViralLoad());
		}

		return facilityClient;
	}

	@Override
	public Integer updatePatientUUIForARVClientsFromSmartCare() {
		for(SmartCarePatientRegistration smartCarePatientRegistration : wsClientSmartCare.allPatientRegistrations()) {
			FacilityClient facilityClient = arvDispensationFacade.toFacilityClient(smartCarePatientRegistration);
			if(arvDispDAO.saveFacilityClient(facilityClient) == null) {
				arvDispDAO.updateARVClientByARTNumber(facilityClient);
			}
            arvDispDAO.mergeDuplicates(facilityClient);
		}
		return 0;
	}
}
