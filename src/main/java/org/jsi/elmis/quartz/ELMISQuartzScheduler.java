/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.quartz;

import org.apache.log4j.Logger;
import org.jsi.elmis.quartz.config.AutoWiringSpringBeanJobFactory;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.quartz.SpringBeanJobFactory;

import java.io.IOException;


public class ELMISQuartzScheduler {

	private final Logger logger = Logger.getLogger(getClass());

	public static final String SCHEDULER_BEAN_NAME = "simpleQuartzScheduler";

	@Autowired
	private ApplicationContext applicationContext;

	@Bean
	public SpringBeanJobFactory springBeanJobFactory() {
		AutoWiringSpringBeanJobFactory jobFactory = new AutoWiringSpringBeanJobFactory();
		logger.debug("Configuring Job factory");
		jobFactory.setApplicationContext(applicationContext);
		return jobFactory;
	}

	@Bean(name = SCHEDULER_BEAN_NAME)
	public Scheduler myScheduler() throws SchedulerException, IOException {
		StdSchedulerFactory factory = new StdSchedulerFactory();
		logger.debug("Getting a handle to the Scheduler");
		Scheduler scheduler = factory.getScheduler();
		scheduler.setJobFactory(springBeanJobFactory());
		scheduler.start();
		return scheduler;
	}


	protected Scheduler scheduler(){
		return (Scheduler) applicationContext.getBean(SCHEDULER_BEAN_NAME);
	}

}
