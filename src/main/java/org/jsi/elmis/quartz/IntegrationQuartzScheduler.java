/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.quartz;

import org.apache.log4j.Logger;
import org.jsi.elmis.quartz.job.SmartCareIntegrationJob;
import org.jsi.elmis.service.interfaces.PropertiesService;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

@Component
@ConditionalOnProperty(value = { "quartz.integration.enabled" })
public class IntegrationQuartzScheduler extends ELMISQuartzScheduler{

    private final Logger logger = Logger.getLogger(getClass());

    @Autowired
    PropertiesService propertyService;

    @PostConstruct
    public void init() throws SchedulerException, IOException{
        logger.info("starting integration quartz...");

        String smartCareImportPatientRegCronExpression = propertyService.getProperty("smart_care_import_patient.cron", false).getValue();
        JobDetail jbSmartCareImportPatientReg = newJob().ofType(SmartCareIntegrationJob.class).storeDurably()
                .withIdentity(JobKey.jobKey("Qrtz_Job_SmartCareImportPatientReg"))
                .withDescription("Invoke import patient registration from smartcare").build();

        Trigger trgSmartCareImportPatientReg = newTrigger().forJob(JobKey.jobKey("Qrtz_Job_SmartCareImportPatientReg"))
                .withIdentity(TriggerKey.triggerKey("Qrtz_Trigger_SmartCareImportPatientReg")).withDescription(smartCareImportPatientRegCronExpression)
                .withSchedule(cronSchedule(smartCareImportPatientRegCronExpression)).build();

        scheduler().scheduleJob(jbSmartCareImportPatientReg, trgSmartCareImportPatientReg);
    }
}
