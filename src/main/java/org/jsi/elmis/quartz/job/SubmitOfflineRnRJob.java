/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.quartz.job;

import org.jsi.elmis.common.constants.ELMISConstants;
import org.jsi.elmis.interfacing.openlmis.RnRSubmissionResult;
import org.jsi.elmis.model.OfflineRnRStatus;
import org.jsi.elmis.quartz.job.facade.OfflineRequisitionFacade;
import org.jsi.elmis.rnr.OfflineRnRDTO;
import org.jsi.elmis.service.interfaces.OfflineRnRService;
import org.jsi.elmis.service.interfaces.PropertiesService;
import org.jsi.elmis.service.interfaces.RnRService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;

public class SubmitOfflineRnRJob implements Job{
	@Autowired
    PropertiesService propertiesService;
	@Autowired
	RnRService rnrService;
	@Autowired
	OfflineRequisitionFacade offlineRequisitionFacade;
	@Autowired
	OfflineRnRService offlineRnRService;
	
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		
		for(OfflineRnRDTO offlineRnRDTO : offlineRequisitionFacade.getOfflineRequistionsForSubmission()){
            String installationType = propertiesService.getProperty("installation.type",false).getValue();
            String sourceApplication = installationType.equalsIgnoreCase("DM")? "eLMIS_DM":"eLMIS_FE";
			offlineRnRDTO.getRnr().setSourceApplication(sourceApplication);
			RnRSubmissionResult result = rnrService.submitOfflineRnR(offlineRnRDTO.getRnr());
			System.out.println("\n\n\n\n==============================\n\n\n" + new Gson().toJson(result) + "\n\n\n\n==============================\n\n\n");
			OfflineRnRStatus offlineRnRStatus = new OfflineRnRStatus(null, result.isSuccess() ? ELMISConstants.SUCCESSFUL.getValue() :
					ELMISConstants.FAILED.getValue() ,offlineRnRDTO.getOfflineRequisition().getId(), result.getRemark());
			offlineRnRService.saveOfflineRnRStatus(offlineRnRStatus);
			if(result.isSuccess() || result.getRemark().equalsIgnoreCase("error.rnr.already.submitted.for.this.period")){
				offlineRnRDTO.getOfflineRequisition().setStatus(ELMISConstants.SUBMITTED.getValue());
				offlineRnRService.updateRnR(offlineRnRDTO.getOfflineRequisition());
			}			
		}
		
	}

}
