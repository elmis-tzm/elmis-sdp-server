/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.org.jsi.elmis.interfacing.smartcare.facade;

import java.util.ArrayList;
import java.util.List;

import org.jsi.elmis.model.FacilityClient;
import org.jsi.elmis.model.Regimen;
import org.jsi.elmis.model.dto.DispensationLineItem;
import org.jsi.elmis.model.dto.SmartCareDispensationDTO;
import org.jsi.elmis.rest.client.model.SmartCarePatientRegistration;
import org.jsi.elmis.rest.result.ARVClientResult;
import org.jsi.elmis.service.interfaces.ARVDispensingService;
import org.jsi.elmis.service.interfaces.PropertiesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * Created by Mekbib on 9/15/2016.
 */
@Service
@Scope
public class ARVDispensationFacade {
    @Autowired
    PropertiesService propertiesService;

    @Autowired
    ARVDispensingService arvDispensingService;
    



    public List<SmartCareDispensationDTO> toSmartCareDispnesationDTOList(List<DispensationLineItem> dispensationLineItems){
    	String facilityCode = propertiesService.getProperty("facility.code", false).getValue();
        if(dispensationLineItems.isEmpty()){
            return new ArrayList<>();
        }

        List<SmartCareDispensationDTO> smartCareDispensationDTOList = new ArrayList<>();
        SmartCareDispensationDTO smartCareDispensationDTO;
        for(DispensationLineItem dli : dispensationLineItems){

            smartCareDispensationDTO = new SmartCareDispensationDTO();

            smartCareDispensationDTO.setPatientGuid(dli.getUuid());
            smartCareDispensationDTO.setMslDrugId(dli.getProductCode());//TODO : communicate with smart care guys
            smartCareDispensationDTO.setProviderId(dli.getProviderId());//TODO : communicate with smart care guys
            smartCareDispensationDTO.setUserId(dli.getUserId());
            smartCareDispensationDTO.setArtNumber(dli.getArtNumber());
            smartCareDispensationDTO.setHmisCode(facilityCode);
            smartCareDispensationDTO.setNextVisitDate(dli.getNextVisitDate());
            smartCareDispensationDTO.setTransactionTime(dli.getTxnDate());
            smartCareDispensationDTO.setDispensationDate(dli.getDispensationDate());
            smartCareDispensationDTO.setFrequency(dli.getFrequency());
            smartCareDispensationDTO.setQuantityDispensed(dli.getTablets().doubleValue());
            smartCareDispensationDTO.setUnitOfMeasurement(null);//TODO : communicate with smart care guys
            smartCareDispensationDTO.setDoseStrength(null);//TODO : communicate with smart care guys
            smartCareDispensationDTO.setUnitQuantityPerDose(dli.getUnitQuantityPerDose());//TODO : communicate with smart care guys
            smartCareDispensationDTO.setSequence(dli.getSequence());
            smartCareDispensationDTO.setTransactionId(dli.getTransactionId());
            
            smartCareDispensationDTOList.add(smartCareDispensationDTO);
        }

        return smartCareDispensationDTOList;
    }
    
	public List<FacilityClient> toFacilityClientsList(ArrayList<SmartCarePatientRegistration> smartCarePatientRegistrationList) {
		List<FacilityClient> facilityClients = new ArrayList<>();
		FacilityClient newARVClient = null;
		for(SmartCarePatientRegistration scpr : smartCarePatientRegistrationList)
		{
			newARVClient = new FacilityClient();
			
			newARVClient.setArtNumber(scpr.getArtNumber());
			newARVClient.setFirstName(scpr.getFirstName());
			newARVClient.setLastName(scpr.getLastName());
			newARVClient.setSex(scpr.getSex().equalsIgnoreCase("F") ? "Female" : "Male");
			newARVClient.setDateOfBirth(scpr.getDateOfBirth());
			newARVClient.setRegistrationDate(scpr.getRegistrationDate());
			newARVClient.setUuid(scpr.getPatientGUID());
			newARVClient.setSequenceNumber(scpr.getSequence().intValue());
            newARVClient.setRegimenId(arvDispensingService.getRegimenByCode(scpr.getRegimenCode()).getId());
			facilityClients.add(newARVClient);
		}
		 
		return facilityClients;
		
	}

    public FacilityClient toFacilityClient(SmartCarePatientRegistration smartCarePatientRegistration){

        if(smartCarePatientRegistration==null){
            return null;
        }

        FacilityClient facilityClient = new FacilityClient();

        facilityClient.setArtNumber(smartCarePatientRegistration.getArtNumber());
        facilityClient.setFirstName(smartCarePatientRegistration.getFirstName());
        facilityClient.setLastName(smartCarePatientRegistration.getLastName());
        facilityClient.setSex(smartCarePatientRegistration.getSex().equalsIgnoreCase("F") ? "Female" : "Male");
        facilityClient.setDateOfBirth(smartCarePatientRegistration.getDateOfBirth());
        facilityClient.setRegistrationDate(smartCarePatientRegistration.getRegistrationDate());
        facilityClient.setUuid(smartCarePatientRegistration.getPatientGUID());
        facilityClient.setSequenceNumber(smartCarePatientRegistration.getSequence().intValue());

        Regimen reg = arvDispensingService.getRegimenByCode(smartCarePatientRegistration.getRegimenCode());

        facilityClient.setRegimenId( reg != null ? reg.getId() : null);

        return facilityClient;
    }

    public ARVClientResult toARVClientResult(FacilityClient facilityClient, Integer nodeId){
        ARVClientResult result = new ARVClientResult();
        result.setClient(facilityClient);
        result.setFacilityClientCD4Count(arvDispensingService.getFacilityClientCD4Count(facilityClient.getId()));
        result.setFacilityClientViralLoad(arvDispensingService.getFacilityClientViralLoad(facilityClient.getId()));
        result.setFacilityClientVitals(arvDispensingService.getFacilityClientVitals(facilityClient.getId()));
        if(result.getClient().getRegimenId() != null){//if client is initiated on regimen
            result.setRegimenCombos(arvDispensingService.getRegimenProductCombos(facilityClient.getRegimenId()));
            result.setDefaultCombo(arvDispensingService.getLastDispensedComboForClient(facilityClient.getArtNumber()));//previous combination dispensed to user. getLastDispensedComboForClient returns null if client is new or regimen has been changed
            if(result.getDefaultCombo() != null && result.getDefaultCombo().getId() != null){
                result.setAppointmentDate(arvDispensingService.getPreviousDispensationForCombination(result.getDefaultCombo().getId(), facilityClient.getArtNumber()).getNextVisitDate());
                result.setProducts(arvDispensingService.getRegimenComboProducts(facilityClient.getArtNumber(), nodeId, result.getDefaultCombo().getId()));
            }
        }
        return result;
    }
}
