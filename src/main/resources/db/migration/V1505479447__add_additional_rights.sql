insert into rights(name, righttype, description)
select 'CONFIGURE FACILITY INTERACTIONS','','CONFIGURE FACILITY INTERACTIONS'
where not exists (select name from rights where name='CONFIGURE FACILITY INTERACTIONS');

insert into rights(name, righttype, description)
select 'SITE INITIATOR','','SITE INITIATOR'
where not exists (select name from rights where name='SITE INITIATOR');
