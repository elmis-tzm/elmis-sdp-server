ALTER TABLE facility_client_vitals DROP COLUMN date_collected;

ALTER TABLE facility_client_vitals ADD COLUMN height_date_collected TIMESTAMP;

ALTER TABLE facility_client_vitals ADD COLUMN weight_date_collected TIMESTAMP;

ALTER TABLE facility_client_vitals ADD COLUMN blood_pressure_date_collected TIMESTAMP;