CREATE TABLE public.batch_standards
(
    id serial NOT NULL,
    code character varying(10) NOT NULL,
    name character varying(100) NOT NULL,
    PRIMARY KEY (id)
);