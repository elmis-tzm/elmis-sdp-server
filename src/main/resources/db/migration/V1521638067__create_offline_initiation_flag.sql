INSERT INTO application_properties
(
  KEY,
  value
)
SELECT 'allow.offline.rnr.initiation',
       'false' WHERE NOT EXISTS (SELECT *
                                 FROM application_properties
                                 WHERE KEY = 'allow.offline.rnr.initiation');