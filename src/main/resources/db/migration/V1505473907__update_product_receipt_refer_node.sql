ALTER TABLE public.product_receipt
    DROP CONSTRAINT fk_prouduct_source_id;

ALTER TABLE public.product_receipt
    ADD CONSTRAINT fk_prouduct_source_id FOREIGN KEY (source_id)
    REFERENCES public.nodes (id) MATCH SIMPLE
    ON UPDATE CASCADE
    ON DELETE NO ACTION;