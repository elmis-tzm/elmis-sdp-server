CREATE TABLE public.node_program_product_batch
(
    id serial NOT NULL,
    node_program_product_id integer NOT NULL,
    program_product_batch_id integer NOT NULL,
    quantity numeric NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_node_program_product_id FOREIGN KEY (node_program_product_id)
        REFERENCES public.node_products (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT fk_product_batch_id FOREIGN KEY (program_product_batch_id)
        REFERENCES public.product_batch (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.node_program_product_batch
    OWNER to postgres;