CREATE TABLE public.node_program_product_batch_daily_adjustment_totals
(
    id serial NOT NULL,
    adjustment_type character varying(100) COLLATE pg_catalog."default",
    quantity numeric,
    node_prog_prod_batch_id integer,
    date date,
    CONSTRAINT pk_nppb_daily_adj PRIMARY KEY (id),
    CONSTRAINT fk_nppb__daily_adj_total_adju_type FOREIGN KEY (adjustment_type)
        REFERENCES public.losses_adjustments_types (name) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_nppb_daily_adj_total_npp FOREIGN KEY (node_prog_prod_batch_id)
        REFERENCES public.node_program_product_batch (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE TABLE public.node_program_product_batch_daily_status
(
    id serial NOT NULL,
    node_program_product_batch_id integer,
    beginnig_balance numeric,
    final_balance numeric,
    first_action integer,
    last_action integer,
    date date,
    minimum_balance numeric,
    CONSTRAINT pk_nppbds_id PRIMARY KEY (id),
    CONSTRAINT fk_nppb_first_action_id FOREIGN KEY (first_action)
        REFERENCES public.actions (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_nppb_last_action_id FOREIGN KEY (last_action)
        REFERENCES public.actions (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_nppb_node_program_product_id FOREIGN KEY (node_program_product_batch_id)
        REFERENCES public.node_program_product_batch (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE TABLE public.node_program_product_batch_daily_transaction_summary
(
    id serial NOT NULL,
    txn_type_id integer,
    quantity numeric,
    node_program_product_batch_id integer,
    date date,
    CONSTRAINT pk_nppb_daily_txn_summary_id PRIMARY KEY (id),
    CONSTRAINT fk_nppb_node_program_product_id FOREIGN KEY (node_program_product_batch_id)
        REFERENCES public.node_program_product_batch (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_nppb_txn_type_id FOREIGN KEY (txn_type_id)
        REFERENCES public.transaction_types (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);
