ALTER TABLE product_batch DROP COLUMN expirty_date;
ALTER TABLE product_batch ADD COLUMN expiry_date timestamp without time zone;

