ALTER TABLE public.arv_dispensations
    ADD COLUMN program_id integer;

ALTER TABLE public.arv_dispensations
    ADD CONSTRAINT fk_program_id FOREIGN KEY (program_id)
    REFERENCES public.programs (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;

