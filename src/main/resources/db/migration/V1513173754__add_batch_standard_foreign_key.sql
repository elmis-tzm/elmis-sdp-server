ALTER TABLE product_batch ADD COLUMN batch_standard_id INTEGER;

ALTER TABLE product_batch ADD CONSTRAINT fk_batch_standard_id FOREIGN KEY (batch_standard_id) REFERENCES batch_standards (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE NO ACTION;
