ALTER TABLE application_properties ADD COLUMN IF NOT EXISTS groupname character varying(250) NOT NULL DEFAULT 'General'::character varying;
ALTER TABLE application_properties ADD COLUMN IF NOT EXISTS displayorder integer NOT NULL DEFAULT 1;
ALTER TABLE application_properties ADD COLUMN IF NOT EXISTS valueoptions character varying(1000);
ALTER TABLE application_properties ADD COLUMN IF NOT EXISTS isconfigurable boolean DEFAULT true;