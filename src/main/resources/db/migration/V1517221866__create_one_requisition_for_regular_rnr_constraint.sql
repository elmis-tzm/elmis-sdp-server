CREATE UNIQUE INDEX one_requisition_for_regular_rnr
  ON public.requisitions USING btree
  (facility_id, program_id, period_id)
TABLESPACE pg_default    WHERE emergency = false
;