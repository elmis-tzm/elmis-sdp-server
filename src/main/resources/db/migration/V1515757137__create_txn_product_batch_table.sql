CREATE TABLE public.transaction_program_product_batch
(
    id serial NOT NULL,
    transaction_program_product_id integer NOT NULL,
    program_product_batch_id integer NOT NULL,
    quantity numeric NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_txn_program_product_id FOREIGN KEY (transaction_program_product_id)
        REFERENCES public.transaction_products (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT fk_product_batch_id FOREIGN KEY (program_product_batch_id)
        REFERENCES public.product_batch (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.transaction_program_product_batch
    OWNER to postgres;